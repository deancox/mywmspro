<x-wms-layout>
    @section('content')
        <div class="h-auto" x-data="{ showModal: false }">
            @include('inbound.navbar')
            @include('flash-message')
            <div class="flex justify-between justify-items-center">
                <h1 class="flex font-semibold text-2xl px-4 items-center py-2">{{ __('Doors') }}</h1>
            </div>
            <div class="p-4 shadow-lg border-2 border-gray-300 mx-16 bg-white rounded-lg">
                <table class="w-full bg-white text-left" id="dataTable">
                    <thead class="w-full border-b-2 border-black pt-2">
                        <tr class="w-full border-b border-black bg-gray-200">
                            <th class="py-2 pl-2">{{ __('Name') }}</th>
                            <th class="py-2 pl-2">{{ __('Type') }}</th>
                            <th class="py-2 pl-2">{{ __('In Service') }}</th>
                            <th class="py-2 pl-2">{{ __('Max Trucks') }}</th>
                            <th class="py-2 pl-2">{{ __('Current Trucks') }}</th>
                            <th class="py-2 pl-2">{{ __('Current Deliveries') }}</th>
                        </tr>
                    </thead>
                    <tbody class="bg-grey-light" id="tbody">
                        @forelse ($doors as $door)
                            <tr class=" border-b hover:bg-gray-100 text-left text-sm pl-2 sortable draggable">
                                <td class="pl-2 py-2">
                                    <a href="#"
                                        class="hover:underline text-blue-600 hover:text-blue-800">{{ $door->name }}</a>
                                </td>
                                <td class="pl-2">
                                    {{ $door->doorType->description }}
                                </td>
                                <td class="pl-2">
                                    @if ($door->in_service === 1)
                                        <i class="fas fa-check text-green-500 fa-lg in_service"
                                            id="toggle_{{ $door->id }}"></i>
                                    @else
                                        <i class="fas fa-times text-red-500 fa-lg in_service"
                                            id="toggle_{{ $door->id }}"></i>
                                    @endif
                                </td>
                                <td class="pl-2">
                                    {{ $door->max_trucks }}
                                </td>
                                <td class="pl-2">
                                    {{ $door->cur_truck_count }}
                                </td>
                                <td class="pl-2">
                                    {{ $door->cur_delivery_count }}
                                </td>
                            @empty
                                <td>{{ __('No doors Found.') }}</td>
                        @endforelse
                    </tbody>
                    <x-buttons.primary>{{ __('Add New') }}</x-buttons.primary>
                </table>
                {{-- $doors->links() --}}
                <!--new door modal-->
                <form method="POST" action="{{ route('doors.store') }}">
                    @csrf
                    <x-modal>
                        <x-slot name="title">{{ __('Add Door') }}</x-slot>
                        <div class="form-group px-4">
                            <textarea name="name"
                                class="bg-gray-100 rounded border border-gray-400 leading-normal resize-none w-full h-12 px-3 font-medium placeholder-gray-700 focus:outline-none focus:bg-white"
                                placeholder='Name...' required></textarea>
                        </div>
                        <div class="form-group px-4">
                            <label for="max_trucks" class="font-semibold">{{ __('Max Trucks:') }}</label>
                            <h3 class="text-sm italic">{{ _('Note: 0 means unlimited.')}}</h3>
                            <input name="max_trucks" type="number" min="0" value="1"
                                class="bg-gray-100 rounded border border-gray-400 leading-normal resize-none w-full h-12 px-3 font-medium placeholder-gray-700 focus:outline-none focus:bg-white"
                                placeholder='Default 1..' />
                        </div>
                        <div class="form-group px-4 py-2">
                            <label for="in_service" class="font-semibold pr-2">{{ __('In Service:') }}</label>
                                <label data-toggle="checkbox-toggle" data-rounded="rounded-xl" data-handle-size="12"
                                data-handle-color="bg-white" data-off-color="bg-gray-400" data-on-color="bg-blue-500">
                                <input type="checkbox" name="in_service" class="onOff" checked />
                        </div>
                        <div class="form-group px-4">
                            <label for="supplier" class="font-semibold">{{ __('Door Type:') }}</label>
                            <br>
                            <select name="door_type_id" id="door_type_id"
                                class="rounded-lg focus:outline-none focus:ring-0 focus:bg-gray-100 border-gray-400 bg-gray-100 w-full"
                                required placeholder="Select door Type">
                                <option value=""></option>
                                @forelse ($doorTypes as $doorType)
                                    <option value="{{ $doorType->id }}">{{ $doorType->description }}
                                    </option>
                                @empty
                                    <option value="">{{ __('No door Types Found.') }}</option>
                                @endforelse
                            </select>
                        </div>
                    </x-modal>
                </form>
            </div>
        </div>
        <script>
            $(function() {
                $('.in_service').click(function() {

                    var id = $.trim($(this).closest('tr').find('td:eq(3)').text());

                    $.ajax({
                        headers: {
                            'X-HTTP-Method-Override': 'PATCH'
                        },
                        type: "POST",
                        url: 'doors/' + id,
                        data: {
                            "door": id,
                            "_token": "{{ csrf_token() }}"
                        },
                        success: function(response) {
                            $("#toggle_" + id).toggleClass('fa-check text-green-500').toggleClass(
                                'fa-times text-red-400');
                            console.log('updated');
                        },
                        error: function(req, status, error) {
                            console.log(error);
                        }
                    });
                });
            });
            $(function() {
                var fixHelper = function(e, ui) {
                    ui.children().each(function() {
                        $(this).width($(this).width());
                    });
                    return ui;
                };

                $("#tbody").sortable({
                    helper: fixHelper
                }).disableSelection();

            });
            $(document).ready(function() {
                $('td').each(function() {
                    $(this).css('width', $(this).width() + 'px');
                });
                $('th').each(function() {
                    $(this).css('width', $(this).width() + 'px');
                });
            });
        </script>
    @endsection
</x-wms-layout>
