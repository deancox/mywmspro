<div>
    @if ($message = Session::get('success'))
        <div class="w-full alert alert-success pl-5 bg-green-200 py-2">
            <strong class=" text-green-800">{{ $message }}</strong>
        </div>
    @endif   
    
    @if ($message = Session::get('error'))
        <div class="w-full alert pl-5 bg-red-200 py-2">
            <strong class=" text-red-800">{{ $message }}</strong>
        </div>
    @endif
</div>