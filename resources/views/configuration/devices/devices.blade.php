<x-wms-layout>
    @section('content')
        <div class="h-auto" x-data="{ showModal: false }">
            @include('configuration.navbar')
            @include('flash-message')
            <x-page.title>{{ __('Mobile Devices') }}</x-page.title>
            <x-table>
                <x-buttons.primary>{{ __('Add New') }}</x-buttons.primary>
                <x-slot name="headings">
                    <x-table.heading>{{ __('Mobile Name') }}</x-table.heading>
                    <x-table.heading>{{ __('Mobile Type') }}</x-table.heading>
                    <x-table.heading>{{ __('Last Login') }}</x-table.heading>
                    <x-table.heading>{{ __('Logged In User') }}</x-table.heading>
                </x-slot>
                @forelse ($devices as $device)
                    <x-table.row>
                        <x-table.cell>{{ $device->name }}</x-table.cell>
                        <x-table.cell>{{ $device->deviceType->description }}</x-table.cell>
                        <x-table.cell>{{ __('last login date TODO') }}</x-table.cell>
                        <x-table.cell>{{ __('logged in user TODO') }}</x-table.cell>
                    </x-table.row>
                @empty
                    <x-table.row>
                        <x-table.cell>{{ __('No Devices Found.') }}</x-table.cell>
                    </x-table.row>
                @endforelse
            </x-table>
            {{ $devices->links() }}
            <!--new device modal-->
            <form method="POST" action="{{ route('config.devices.store') }}">
                @csrf
                <x-modal>
                    <x-slot name="title">{{ __('New Device') }}</x-slot>
                    <x-modal.input.textarea name="name" placeholder='Name...' required />
                    <x-modal.input.dropdown label="{{ __('Device Type:') }}" name="device_type_id" id="device_type_id"
                        required>
                        <option value=""></option>
                        @forelse ($deviceTypes as $deviceType)
                            <option value="{{ $deviceType->id }}">{{ $deviceType->description }}
                            </option>
                        @empty
                            <option value="">{{ __('No Device Types Found.') }}</option>
                        @endforelse
                    </x-modal.input.dropdown>
                </x-modal>
            </form>
        </div>
    @endsection
</x-wms-layout>
