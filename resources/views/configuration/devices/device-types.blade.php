<x-wms-layout>
    @section('content')
        <div class="h-auto" x-data="{ showModal: false }">
            @include('configuration.navbar')
            @include('flash-message')
            <x-page.title>{{ __('Mobile Device Types') }}</x-page.title>
            <x-table>
                <x-buttons.primary>{{ __('Add New') }}</x-buttons.primary>
                <x-slot name="headings">
                    <x-table.heading>{{ __('Mobile Type') }}</x-table.heading>
                    <x-table.heading>{{ __('Description') }}</x-table.heading>
                    <x-table.heading>{{ __('Device Count') }}</x-table.heading>
                </x-slot>
                @forelse ($deviceTypes as $type)
                    <x-table.row>
                        <x-table.cell>{{ $type->name }}</x-table.cell>
                        <x-table.cell>{{ $type->description }}</x-table.cell>
                        <x-table.cell>{{ $type->devices_count }}</x-table.cell>
                    </x-table.row>
                @empty
                    <x-table.row>
                        <x-table.cell>{{ __('No Device types Found.') }}</x-table.cell>
                    </x-table.row>
                @endforelse
            </x-table>
            {{ $deviceTypes->links() }}
            <!--new device type modal-->
            <form method="POST" action="{{ route('config.deviceTypes.store') }}">
                @csrf
                <x-modal>
                    <x-slot name="title">{{ __('New Device Type') }}</x-slot>
                    <x-modal.input.textarea name="name" placeholder='Name...' required />
                    <x-modal.input.textarea name="description" placeholder='Description...' required />
                </x-modal>
            </form>
        </div>
    @endsection
</x-wms-layout>
