<x-wms-layout>
    @section('content')
        <div class="flex-1">
            @include('configuration.navbar')
            @include('flash-message')
            <div class="bg-white w-1/4 p-2 ml-4 mt-2 rounded-lg border-2 border-gray-300">
                <h1 class="font-bold text-lg pb-2">{{ __('Upload Locations and Zones') }}</h1>
                <form action="{{ route('config.configupload.store') }}" method="POST" enctype="multipart/form-data">
                    <input type="file" name="location_upload" class="pb-4 focus:outline-none" required />
                    @error('file')
                        <div class="text-lg text-red-500 ml-1 pt-2">
                            {{ __('Please choose a file to upload.') }}
                        </div>
                    @enderror
                    <br />
                    <p class="text-sm font-bold">{{ __('Note:') }}</p>
                    <p class="text-sm pb-2">
                        {{ __('This will create storage locations, and if they
                                             don\'t exist already the following items will also be created:') }}
                    </p>
                    <p class="text-sm">&#8226; {{ __('Location Types') }}</p>
                    <p class="text-sm">&#8226; {{ __('Storage Zones') }}</p>
                    <p class="text-sm">&#8226; {{ __('Pick Zones') }}</p>
                    <p class="text-sm pb-2">&#8226; {{ __('Count Zones') }}</p>
                    <p class="text-sm italic mb-4">
                        {{ __('These Items will not be deleted if they\'re missing from the file but already exist with different names.') }}
                    </p>
                    <p class="text-sm italic mb-4 text-red-500">
                        {{ __('If Locations already exist they will be OVERWRITTEN with the content of this file.') }}
                    </p>
                    <p class="text-sm font-bold underline mb-2">
                        {{ __('Remember to check the Dashboard for config warnings after this upload!') }}</p>
                    <button type="submit"
                        class=" bg-yellow-300 hover:bg-yellow-600 p-2 rounded-lg shadow mb-2 focus:outline-none">{{ __('Upload
                                                Selected
                                                File') }}</button>

                    @csrf
                </form>
            </div>
        </div>
    @endsection
</x-wms-layout>
