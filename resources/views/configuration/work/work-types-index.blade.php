<x-wms-layout>
    @section('content')
        <div class="h-auto" x-data="{ showModal: false }">
            @include('configuration.navbar')
            @include('flash-message')
            <x-page.title>{{ __('Work Types') }}</x-page.title>
            <x-table>
                <x-buttons.primary>{{ __('Add New') }}</x-buttons.primary>
                <x-slot name="headings">
                    <x-table.heading>{{ __('System Name') }}</x-table.heading>
                    <x-table.heading>{{ __('Description') }}</x-table.heading>
                    <x-table.heading>{{ __('Creation Status') }}</x-table.heading>
                    <x-table.heading>{{ __('Base Priority') }}</x-table.heading>
                    <x-table.heading>{{ __('Auto Increments') }}</x-table.heading>
                    <x-table.heading>{{ __('Increments By') }}</x-table.heading>
                    <x-table.heading>{{ __('Increments Every') }}</x-table.heading>
                </x-slot>
                @forelse ($workTypes as $workType)
                    <x-table.row>
                        <x-table.cell>
                            <x-page.link href="#">{{ $workType->name }}</x-page.link>
                        </x-table.cell>
                        <x-table.cell>{{ $workType->description }}</x-table.cell>
                        <x-table.cell>{{ $workType->default_creation_status }}</x-table.cell>
                        <x-table.cell>{{ $workType->base_priority }}</x-table.cell>
                        @if ($workType->auto_increment)
                            <x-table.cell>{{ __('Yes') }}</x-table.cell>
                        @else
                            <x-table.cell>{{ __('No') }}</x-table.cell>
                        @endif
                        @if ($workType->auto_increment)
                            <x-table.cell>{{ $workType->increment_by }}</x-table.cell>
                        @else
                            <x-table.cell>{{ __('N/A') }}</x-table.cell>
                        @endif
                        @if ($workType->auto_increment)
                            <x-table.cell>{{ $workType->increment_interval }}</x-table.cell>
                        @else
                            <x-table.cell>{{ __('N/A') }}</x-table.cell>
                        @endif
                    </x-table.row>
                @empty
                    <x-table.row>
                        <x-table.cell>{{ __('No Work Types Found.') }}</x-table.cell>
                    </x-table.row>
                @endforelse
            </x-table>
            <!--new work type modal-->
            <form method="POST">
                @csrf
                <x-modal>
                    <x-slot name="title">{{ __('New Work Type') }}</x-slot>
                    <x-modal.input.textarea name="name" placeholder="Name..." required />
                    <x-modal.input.textarea name="description" placeholder="Description..." required />
                    <div class="form-group px-4">
                        <label for="base_priority" class="font-semibold">{{ __('Base Priority (1 = highest, 100 = lowest):') }}</label>
                        <input name="base_priority" type="number" min="1" max="100"
                            class="bg-gray-100 rounded border border-gray-400 leading-normal resize-none w-full h-12 px-3 font-medium placeholder-gray-700 focus:outline-none focus:bg-white"
                            placeholder='Required...' required />
                    </div>
                    <x-modal.input.dropdown label="{{ __('Creation Status:') }}" name="default_creation_status"
                        id="printer_type_id" required>
                        <option value=""></option>
                        @foreach ($workStatuses as $workStatus)
                            <option value="{{ $workStatus->status }}">{{ $workStatus->status }}</option>
                        @endforeach
                    </x-modal.input.dropdown>
                    <div class="form-group px-4 py-2">
                        <label for="auto_increment" class="font-semibold pr-2">{{ __('Auto Increment:') }}</label>
                        <label data-toggle="checkbox-toggle" data-rounded="rounded-xl" data-handle-size="12"
                            data-handle-color="bg-white" data-off-color="bg-gray-400" data-on-color="bg-blue-500">
                            <input type="checkbox" name="auto_increment" id="auto_increment" />
                    </div>
                    <div class="form-group px-4" id="increment_by" hidden>
                        <label for="increment_by" class="font-semibold">{{ __('Increment By:') }}</label>
                        <input name="increment_by" type="number" min="1"
                            class="bg-gray-100 rounded border border-gray-400 leading-normal resize-none w-full h-12 px-3 font-medium placeholder-gray-700 focus:outline-none focus:bg-white"
                            placeholder='Required..'/>
                    </div>
                    <div class="form-group px-4 mt-2" id="increment_interval" hidden>
                        <label for="increment_interval" class="font-semibold">{{ __('Increment Every:') }}</label>
                        <input name="increment_interval" type="number" min="1"
                            class="bg-gray-100 rounded border border-gray-400 leading-normal resize-none w-full h-12 px-3 font-medium placeholder-gray-700 focus:outline-none focus:bg-white"
                            placeholder='Enter Minutes..'/>
                    </div>
                </x-modal>
            </form>
        </div>
        <script>
            $(document).ready(function() {
                $("#auto_increment").click(function() {

                    if ($(this).is(":checked")) {

                        $('#increment_by').attr("hidden", false);
                        $('#increment_interval').attr("hidden", false);

                        $('input[name="increment_by"]').attr("required", true);
                        $('input[name="increment_interval"]').attr("required", true);

                    } else {

                        $('#increment_by').attr("hidden", true);
                        $('#increment_interval').attr("hidden", true);

                        $('input[name="increment_by"]').attr("required", false);
                        $('input[name="increment_interval"]').attr("required", false);
                    }
                });
            });
        </script>
    @endsection
</x-wms-layout>
