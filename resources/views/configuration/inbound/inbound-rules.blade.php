<x-wms-layout>
    @section('content')
        <div class="flex-1">
            @include('configuration.navbar')
            @include('flash-message')
            <div class="flex justify-between justify-items-center">
                <h1 class="flex font-semibold text-2xl px-4 items-center py-2">{{ __('Inbound Settings') }}
                </h1>
            </div>
            <div class=" mx-16">
                <div class="flex justify-around gap-4 w-full h-auto">
                    <div class="p-2 shadow-lg border-2 border-gray-300 mt-2 mx-4 rounded-lg bg-white w-full">
                        <div class="flex-1 justify-between">
                            <h1 class="flex font-semibold text-xl items-center pb-2">{{ __('Truck Required? ') }}</h1>
                            <p class="text-sm pb-2">
                                {{ __('If selected then ALL inbound deliveries can only be checked in after they\'re assigned to a truck') }}
                            </p>
                            <label data-toggle="checkbox-toggle" data-rounded="rounded-xl" data-handle-size="12"
                                data-handle-color="bg-white" data-off-color="bg-gray-300" data-on-color="bg-blue-500">
                                <input type="checkbox" name="visible" class="onOff" value="{{-- $menu->visible --}}"
                                    {{ $rules->truck_required ? 'checked' : '' }} />
                        </div>
                    </div>
                    <div class="p-2 shadow-lg border-2 border-gray-300 mt-2 mx-4 rounded-lg bg-white w-full">
                        <div class="flex-1 justify-between">
                            <h1 class="flex font-semibold text-xl items-center pb-2">{{ __('Truck based on supplier? ') }}
                            </h1>
                            <p class="text-sm pb-2">
                                {{ __('If selected then Supplier settings will be used to determine whether or not a truck is required.') }}
                            </p>
                            <label data-toggle="checkbox-toggle" data-rounded="rounded-xl" data-handle-size="12"
                                data-handle-color="bg-white" data-off-color="bg-gray-300" data-on-color="bg-blue-500">
                                <input type="checkbox" name="visible" class="onOff" value="{{-- $menu->visible --}}"
                                    {{ $rules->truck_required_by_supplier ? 'checked' : '' }} />
                        </div>
                    </div>
                </div>
                <div class="flex justify-around gap-4 w-full h-auto">
                    <div class="p-2 shadow-lg border-2 border-gray-300 mt-2 mx-4 rounded-lg bg-white w-full">
                        <div class="flex-1 justify-between">
                            <h1 class="flex font-semibold text-xl items-center pb-2">{{ __('Shorts Allowed? ') }}</h1>
                            <p class="text-sm pb-2">
                                {{ __('If selected then All short receipts will be allowed on ANY deliveries.') }}
                            </p>
                            <label data-toggle="checkbox-toggle" data-rounded="rounded-xl" data-handle-size="12"
                                data-handle-color="bg-white" data-off-color="bg-gray-300" data-on-color="bg-blue-500">
                                <input type="checkbox" name="visible" class="onOff" value="{{-- $menu->visible --}}"
                                    {{ $rules->short_receive ? 'checked' : '' }} />
                        </div>
                    </div>
                    <div class="p-2 shadow-lg border-2 border-gray-300 mt-2 mx-4 rounded-lg bg-white w-full">
                        <div class="flex-1 justify-between">
                            <h1 class="flex font-semibold text-xl items-center pb-2">
                                {{ __('Shorts based on supplier? ') }}</h1>
                            <p class="text-sm pb-2">
                                {{ __('If selected then short permissions will be based on supplier settings.') }}
                            </p>
                            <label data-toggle="checkbox-toggle" data-rounded="rounded-xl" data-handle-size="12"
                                data-handle-color="bg-white" data-off-color="bg-gray-300" data-on-color="bg-blue-500">
                                <input type="checkbox" name="visible" class="onOff" value="{{-- $menu->visible --}}"
                                    {{ $rules->shorts_by_supplier ? 'checked' : '' }} />
                        </div>
                    </div>
                </div>
                <div class="flex justify-around gap-4 w-full h-auto">
                    <div class="p-2 shadow-lg border-2 border-gray-300 mt-2 mx-4 rounded-lg bg-white w-full">
                        <div class="flex-1 justify-between">
                            <h1 class="flex font-semibold text-xl items-center pb-2">{{ __('Over Receipts Allowed? ') }}
                            </h1>
                            <p class="text-sm pb-2">
                                {{ __('If selected then All over receipts will be allowed on ANY deliveries.') }}
                            </p>
                            <label data-toggle="checkbox-toggle" data-rounded="rounded-xl" data-handle-size="12"
                                data-handle-color="bg-white" data-off-color="bg-gray-300" data-on-color="bg-blue-500">
                                <input type="checkbox" name="visible" class="onOff" value="{{-- $menu->visible --}}"
                                    {{ $rules->over_receive ? 'checked' : '' }} />
                        </div>
                    </div>
                    <div class="p-2 shadow-lg border-2 border-gray-300 mt-2 mx-4 rounded-lg bg-white w-full">
                        <div class="flex-1 justify-between">
                            <h1 class="flex font-semibold text-xl items-center pb-2">
                                {{ __('Over Receipts based on supplier? ') }}</h1>
                            <p class="text-sm pb-2">
                                {{ __('If selected then over receipt permissions will be based on supplier settings.') }}
                            </p>
                            <label data-toggle="checkbox-toggle" data-rounded="rounded-xl" data-handle-size="12"
                                data-handle-color="bg-white" data-off-color="bg-gray-300" data-on-color="bg-blue-500">
                                <input type="checkbox" name="visible" class="onOff" value="{{-- $menu->visible --}}"
                                    {{ $rules->over_receipt_by_supplier ? 'checked' : '' }} />
                        </div>
                    </div>
                </div>
                <div class="flex justify-around gap-4 w-full h-auto">
                    <div class="p-2 shadow-lg border-2 border-gray-300 mt-2 mx-4 rounded-lg bg-white w-full">
                        <div class="flex-1 justify-between">
                            <h1 class="flex font-semibold text-xl items-center pb-2">
                                {{ __('Staging Lane Required at Check In? ') }}
                            </h1>
                            <p class="text-sm pb-2">
                                {{ __('If selected then for ALL inbound deliveries users will be required to select a staging lane.') }}
                            </p>
                            <label data-toggle="checkbox-toggle" data-rounded="rounded-xl" data-handle-size="12"
                                data-handle-color="bg-white" data-off-color="bg-gray-300" data-on-color="bg-blue-500">
                                <input type="checkbox" name="visible" class="onOff" value="{{-- $menu->visible --}}"
                                    {{ $rules->checkin_lane_required ? 'checked' : '' }} />
                        </div>
                    </div>
                    <div class="p-2 shadow-lg border-2 border-gray-300 mt-2 mx-4 rounded-lg bg-white w-full">
                        <div class="flex-1 justify-between">
                            <h1 class="flex font-semibold text-xl items-center pb-2">
                                {{ __('Staging Lane Required at Check In based on supplier? ') }}</h1>
                            <p class="text-sm pb-2">
                                {{ __('If selected then a staging lane will be required at check in based on supplier settings.') }}
                            </p>
                            <label data-toggle="checkbox-toggle" data-rounded="rounded-xl" data-handle-size="12"
                                data-handle-color="bg-white" data-off-color="bg-gray-300" data-on-color="bg-blue-500">
                                <input type="checkbox" name="visible" class="onOff" value="{{-- $menu->visible --}}"
                                    {{ $rules->checkin_lane_required_by_supplier ? 'checked' : '' }} />
                        </div>
                    </div>
                </div>
                <div class="flex justify-around gap-4 w-full h-auto">
                    <div class="p-2 shadow-lg border-2 border-gray-300 mt-2 mx-4 rounded-lg bg-white w-full">
                        <div class="flex-1 justify-between">
                            <h1 class="flex font-semibold text-xl items-center pb-2">
                                {{ __('Override default staging lane? ') }}
                            </h1>
                            <p class="text-sm pb-2">
                                {{ __('If selected then users will be able to select ANY staging lane even if the door has a default staging lane assigned.') }}
                            </p>
                            <label data-toggle="checkbox-toggle" data-rounded="rounded-xl" data-handle-size="12"
                                data-handle-color="bg-white" data-off-color="bg-gray-300" data-on-color="bg-blue-500">
                                <input type="checkbox" name="visible" class="onOff" value="{{-- $menu->visible --}}"
                                    {{ $rules->can_override_default_door ? 'checked' : '' }} />
                        </div>
                    </div>
                </div>
                <div class="flex justify-around gap-4 w-full h-auto">
                    <div class="p-2 shadow-lg border-2 border-gray-300 mt-2 mx-4 rounded-lg bg-white w-full">
                        <div class="flex-1 justify-between">
                            <h1 class="flex font-semibold text-xl items-center pb-2">{{ __('Mobile can close? ') }}
                            </h1>
                            <p class="text-sm pb-2">
                                {{ __('If selected then mobile devices will be allowed to close deliveries & trucks.') }}
                            </p>
                            <label data-toggle="checkbox-toggle" data-rounded="rounded-xl" data-handle-size="12"
                                data-handle-color="bg-white" data-off-color="bg-gray-300" data-on-color="bg-blue-500">
                                <input type="checkbox" name="visible" class="onOff" value="{{-- $menu->visible --}}"
                                    {{ $rules->mobile_can_close ? 'checked' : '' }} />
                        </div>
                    </div>
                    <div class="p-2 shadow-lg border-2 border-gray-300 mt-2 mx-4 rounded-lg bg-white w-full">
                        <div class="flex-1 justify-between">
                            <h1 class="flex font-semibold text-xl items-center pb-2">
                                {{ __('Mobile can dispatch? ') }}</h1>
                            <p class="text-sm pb-2">
                                {{ __('If selected then mobile devices will be allowed to dispatch trucks.') }}
                            </p>
                            <label data-toggle="checkbox-toggle" data-rounded="rounded-xl" data-handle-size="12"
                                data-handle-color="bg-white" data-off-color="bg-gray-300" data-on-color="bg-blue-500">
                                <input type="checkbox" name="visible" class="onOff" value="{{-- $menu->visible --}}"
                                    {{ $rules->mobile_can_dispatch ? 'checked' : '' }} />
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <script>
            /*$(function() {
                                                                $('.onOff').click(function() {

                                                                    var screen = $.trim($(this).closest('tr').find('td:eq(1)').text()).toLowerCase();
                                                                    var id = $.trim($(this).closest('tr').find('td:eq(3)').text());
                                                                   

                                                                   $.ajax({
                                                                        type: "PATCH",
                                                                        url: 'mobileConfig/' + id,
                                                                        data: {
                                                                            "mobileMenu": id,
                                                                            "screen": screen,
                                                                            "_token": "{{ csrf_token() }}"
                                                                        },
                                                                        success: function(response) {

                                                                        },
                                                                        error: function(req, status, error) {
                                                                            console.log(error);
                                                                        }
                                                                    });
                                                                });
                                                            });*/
        </script>
    @endsection
</x-wms-layout>
