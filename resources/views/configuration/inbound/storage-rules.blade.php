<x-wms-layout>
    @section('content')
    <div class="h-auto" x-data="{ showModal: false }">
            @include('configuration.navbar')
            @include('flash-message')
            <x-page.title>{{ __('Storage Rules')}}</x-page.title>
            <p class=" text-md mx-12">{{ __('Storage Rules are used to determine how the application searches for appropriate storage locations during storage search and will be executed in sequence (low to high) until a valid rule is found.') }}</p>
            <p class=" text-sm font-bold italic mx-12 mt-2">{{ __('Please note: If you create 2 rules with the same sequence they will be executed based on when they were created, this could result in unexpected behaviour!') }}</p>
             <x-table>
                <x-page.link href="{{ route('config.storageRules.create') }}"><x-buttons.primary>{{ __('Add New') }}</x-buttons.primary></x-page.link>
                <x-slot name="headings">
                    <x-table.heading>{{ __('Sequence') }}</x-table.heading>
                    <x-table.heading>{{ __('Name') }}</x-table.heading>
                    <x-table.heading>{{ __('Description') }}</x-table.heading>
                </x-slot>
                @forelse ($storageRules as $rule)
                    <x-table.row>
                        <x-table.cell>{{ $rule->sequence }}</x-table.cell>
                        <x-table.cell><x-page.link href="">{{ $rule->name }}</x-page.link></x-table.cell>
                        <x-table.cell>{{ $rule->description }}</x-table.cell>
                    </x-table.row>
                @empty
                    <x-table.row>
                        <x-table.cell>{{ __('No Storage Rules Found.') }}</x-table.cell>
                        <x-table.cell></x-table.cell>
                        <x-table.cell></x-table.cell>
                    </x-table.row>
                @endforelse
            </x-table>
        </div>
    @endsection
</x-wms-layout>
