<x-wms-layout>
    @section('content')
        <div class="h-auto">
            @include('configuration.navbar')
            @include('flash-message')
            <x-page.title>{{ __('Create Storage Rule') }}</x-page.title>
            <form method="POST" action="{{ route('config.storageRules.store') }}">
                @csrf
                <div class="mx-20 mt-2 bg-white border-2 border-gray-300 py-4 rounded-lg shadow-2xl">
                    <div class="flex justify-between gap-4 mx-8 mt-2 rounded-lg">
                        <div class="flex-grow h-full rounded-lg">
                            <label for="name" class=" font-semibold">{{ __('Name:') }}</label>
                            <br>
                            <input type="text" name="name" id="name" placeholder="Required..."
                                class="rounded-lg focus:outline-none focus:ring-0 focus:bg-gray-100 w-full" value="{{ old('name') }}" required>
                        </div>
                        <div class="flex-grow h-full rounded-lg">
                            <label for="description" class=" font-semibold">{{ __('Description:') }}</label>
                            <br>
                            <input type="text" name="description" id="description" placeholder="Required..."
                                class="rounded-lg focus:outline-none focus:ring-0 focus:bg-gray-100 w-full" value="{{ old('description') }}" required>
                        </div>
                        <div class="flex-grow h-full rounded-lg">
                            <label for="sequence" class=" font-semibold">{{ __('Sequence:') }}</label>
                            <br>
                            <input type="number" name="sequence" id="sequence" placeholder="Required..."
                                class="rounded-lg focus:outline-none focus:ring-0 focus:bg-gray-100 w-full" value="{{ old('sequence') }}" required>
                        </div>
                    </div>
                    <!--------tables---------->
                    <div class="flex justify-between gap-2 mx-4 mt-2">
                        <div class="w-full">
                            <h1 class="text-lg font-semibold mt-2 ml-4">{{ __('Select Storage Zones:') }}</h1>
                            <x-table margin="mx-2" class=" max-h-64 overflow-auto">
                                <x-slot name="headings" class="border-0">
                                    <x-table.heading hidden class="border-0">{{ __('Storage Zone') }}
                                    </x-table.heading>
                                    <x-table.heading hidden class="border-0"></x-table.heading>
                                </x-slot>
                                @forelse ($storageZones as $zone)
                                    <x-table.row>
                                        <x-table.cell>{{ $zone->name }}</x-table.cell>
                                        <x-table.cell>
                                            <input type="checkbox" name="storageZones[]" value="{{ $zone->id }}"
                                                class="focus:ring-0 focus:border-white rounded">
                                        </x-table.cell>
                                    </x-table.row>
                                @empty
                                    <x-table.row>
                                        <x-table.cell>{{ __('No Storage Zones Found') }}</x-table.cell>
                                    </x-table.row>
                                @endforelse
                            </x-table>
                        </div>
                        <div class="w-full">
                            <h1 class="text-lg font-semibold mt-2 ml-4">{{ __('Select Product Types:') }}</h1>
                            <x-table margin="mx-2" class=" max-h-64 overflow-auto">
                                <x-slot name="headings" class="border-0">
                                    <x-table.heading hidden class="border-0">{{ __('Storage Zone') }}
                                    </x-table.heading>
                                    <x-table.heading hidden class="border-0"></x-table.heading>
                                </x-slot>
                                @forelse ($productTypes as $type)
                                    <x-table.row>
                                        <x-table.cell>{{ $type->name }}</x-table.cell>
                                        <x-table.cell>
                                            <input type="checkbox" name="productTypes[]" value="{{ $type->id }}"
                                                class="focus:ring-0 focus:border-white rounded">
                                        </x-table.cell>
                                    </x-table.row>
                                @empty
                                    <x-table.row>
                                        <x-table.cell>{{ __('No Product Types Found') }}</x-table.cell>
                                    </x-table.row>
                                @endforelse
                            </x-table>
                        </div>
                        <div class="w-full">
                            <h1 class="text-lg font-semibold mt-2 ml-4">{{ __('Select Uoms:') }}</h1>
                            <x-table margin="mx-2" class="max-h-96 overflow-auto">
                                <x-slot name="headings" class="border-0">
                                    <x-table.heading hidden class="border-0">{{ __('Storage Zone') }}
                                    </x-table.heading>
                                    <x-table.heading hidden class="border-0"></x-table.heading>
                                </x-slot>
                                @forelse ($uoms as $uom)
                                    <x-table.row>
                                        <x-table.cell>{{ $uom->description }}</x-table.cell>
                                        <x-table.cell>
                                            <input type="checkbox" name="uoms[]" value="{{ $uom->id }}"
                                                class="focus:ring-0 focus:border-white rounded">
                                        </x-table.cell>
                                    </x-table.row>
                                @empty
                                    <x-table.row>
                                        <x-table.cell>{{ __('No Uoms Found') }}</x-table.cell>
                                    </x-table.row>
                                @endforelse
                            </x-table>
                        </div>
                    </div>
                    <div class="flex mx-8 gap-2 mt-4 rounded-lg">
                        <button type="submit"
                            class="bg-blue-600 hover:bg-blue-700 p-2 rounded font-semibold text-white">{{ __('Save') }}</button>
                        <a href="{{ route('config.storageRules.index') }}"
                            class="bg-red-500 hover:bg-red-700 focus:outline-none rounded mr-2 px-2 pt-2 text-center font-semibold text-white">
                            <button type="button">{{ __('Cancel') }}</button></a>
                        </a>
                    </div>
                </div>

        </div>
        </form>
        </div>
    @endsection
</x-wms-layout>
