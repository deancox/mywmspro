<x-wms-layout>
    @section('content')
        <div class="flex-1" x-data="{ showModal: false }">
            @include('configuration.navbar')
            @include('flash-message')
            <div class="flex justify-between justify-items-center">
                <h1 class="flex font-semibold text-2xl px-4 items-center py-2">{{ __('Pick Types') }}</h1>
            </div>
            <div class="p-4 shadow-lg border-2 border-gray-300 mx-16 mt-2 bg-white rounded-lg">
                <table class="w-full bg-white text-left">
                    <thead class="w-full border-b-2 border-black">
                        <tr class="w-full border-b border-black bg-gray-200">
                            <th class="py-2 pl-2">{{ __('Category') }}</th>
                            <th class="py-2 pl-2">{{ __('Description') }}</th>
                            <th class="py-2 pl-2">{{ __('Enable/Disable') }}</th>
                            <th class="py-2 pl-2">{{ __('Created By') }}
                            </th>
                        </tr>
                    </thead>
                    <tbody class="bg-grey-light">
                        @forelse ($pickTypes as $pickType)
                            <tr class="border-b-2 hover:bg-gray-100 text-left text-sm pl-2">
                                <td class="pl-2 py-2">
                                    {{ $pickType->category }}
                                </td>
                                <td class="pl-2">
                                    {{ $pickType->description }}
                                </td>
                                <td class="pl-2">
                                    {{ $pickType->enabled }}
                                </td>
                                <td class="pl-2">
                                    {{ $pickType->created_by }}
                                </td>
                            </tr>
                        @empty
                            <td>{{ __('No pickTypes Found.') }}</td>
                        @endforelse
                    </tbody>
                    <x-buttons.primary>{{ __('Add New') }}</x-buttons.primary>
                </table>
                {{-- $pickTypes->links() --}}
                <!--new pickType modal-->
                <form method="POST" action="{{ route('config.pickTypes.store') }}">
                    @csrf
                    <x-modal>
                        <x-slot name="title">{{ __('New pickType') }}</x-slot>
                        <x-modal.input.textarea name="name" placeholder='Name...' required />
                    </x-modal>
                </form>
            </div>
        </div>
    @endsection
</x-wms-layout>
