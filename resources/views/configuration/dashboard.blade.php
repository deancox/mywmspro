<x-wms-layout>
    @section('content')
    <div class="h-auto">
    @include('configuration.navbar')
           <div class="flex justify-between justify-items-center h-14 shadow-lg">
                <h1 class="flex font-semibold text-2xl px-4 items-center py-2">{{__('Configuration Dashboard') }}</h1>
            </div>
            <div class="grid grid-flow-col grid-cols-3 grid-rows-3 gap-2 pt-2 px-2">
                <div class="border-2 rounded-lg bg-gray-200">
                    <div class=" font-bold text-xl bg-gray-300 text-center">{{ __('Users') }}</div>
                    <div class="w-full pt-2 font-extrabold text-2xl text-center ">{{ $users }}</div>
                </div>
                <div class="border-2 rounded-lg bg-gray-200">
                    <div class=" font-bold text-xl bg-gray-300 text-center">{{ __('Storage Zones') }}</div>
                    <div class="w-full pt-2 font-extrabold text-2xl text-center ">{{ $storageZones }}</div>
                </div>
                <div class="border-2 rounded-lg bg-gray-200">
                    <div class="font-bold text-xl bg-gray-300 text-center">{{ __('Pick Zones') }}</div>
                    <div class="w-full pt-2 font-extrabold text-2xl text-center ">{{ $pickZones }}</div>
                </div>
                <div class="border-2 rounded-lg bg-gray-200">
                    <div class=" font-bold text-xl bg-gray-300 text-center">{{ __('Count Zones') }}</div>
                    <div class="w-full pt-2 font-extrabold text-2xl text-center ">{{ $countZones }}</div>
                </div>
                <div class="border-2 rounded-lg bg-gray-200">
                    <div class=" font-bold text-xl bg-gray-300 text-center">{{ __('Location Types') }}</div>
                    <div class="w-full pt-2 font-extrabold text-2xl text-center ">{{ $locationTypes }}</div>
                </div>
                <div class="border-2 rounded-lg bg-gray-200">
                    <div class="font-bold text-xl bg-gray-300 text-center">{{ __('Locations') }}</div>
                    <a href="{{ route('location.index') }}">
                       <div class="w-full pt-2 font-extrabold text-2xl text-center text-blue-500 hover:text-blue-800">{{ $locations }}</div>
                    </a>
                </div>
                <div class="border-2 rounded-lg bg-gray-200">
                    <div class="px-8 pt-2 font-bold text-xl bg-gray-300 text-center">{{ __('Warnings') }}</div>
                    <div class="w-full pt-2 font-extrabold text-2xl text-center text-red-500 ">{{ __('10') }}</div>
                </div>
                <div class="border-2 rounded-lg bg-gray-200">
                    <div class="px-8 pt-2 font-bold text-xl bg-gray-300 text-center">{{ __('Devices') }}</div>
                    <div class="w-full pt-2 font-extrabold text-2xl text-center">{{ $devices }}</div>
                </div>
                <div class="border-2 rounded-lg bg-gray-200">
                    <div class="px-8 pt-2 font-bold text-xl bg-gray-300 text-center">{{ __('Logged in Users') }}</div>
                    <div class="w-full pt-2 font-extrabold text-2xl text-center">{{ __('10') }}</div>
                </div>
            </div>
    </div>
    @endsection
</x-wms-layout>
