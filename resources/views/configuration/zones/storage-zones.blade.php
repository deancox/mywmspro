<x-wms-layout>
    @section('content')
        <div class="h-auto" x-data="{ showModal: false }">
            @include('configuration.navbar')
            @include('flash-message')
            <x-page.title>{{ __('Storage Zones') }}</x-page.title>
            <x-table>
                <x-buttons.primary>{{ __('Add New') }}</x-buttons.primary>
                <x-slot name="headings">
                    <x-table.heading>{{ __('Name') }}</x-table.heading>
                    <x-table.heading>{{ __('Description') }}</x-table.heading>
                    <x-table.heading>{{ __('Locations') }}</x-table.heading>
                    <x-table.heading>{{ __('Actions') }}</x-table.heading>
                </x-slot>
                @forelse ($storageZones as $zone)
                    <x-table.row>
                        <x-table.cell>
                            <x-page.link href="#">{{ $zone->name }}</x-page.link>
                        </x-table.cell>
                        <x-table.cell>{{ $zone->description }}</x-table.cell>
                        <x-table.cell>
                            <x-page.link href="#">{{ $zone->locations_count }}</x-page.link>
                        </x-table.cell>
                        <x-table.actions>
                            <x-slot name="edit">#</x-slot>
                            <x-slot name="delete">#</x-slot>
                        </x-table.actions>
                    </x-table.row>
                @empty
                    <x-table.row>
                        <x-table.cell>{{ __('No Storage Zones Found.') }}</x-table.cell>
                    </x-table.row>
                @endforelse
            </x-table>
            {{ $storageZones->links() }}
            <!--new storage Zone modal-->
            <form method="POST" action="{{ route('config.storageZones.store') }}">
                @csrf
                <x-modal>
                    <x-slot name="title">{{ __('New Storage Zone') }}</x-slot>
                    <x-modal.infotext>
                        {{ __('Storage zones are assigned to locations, this means only product types or UOM\'s allowed in the specified storage zone will be stored in those locations once assigned.') }}
                    </x-modal.infotext>
                    <x-modal.input.textarea name="name" placeholder='Name...' required />
                    <x-modal.input.textarea name="description" placeholder='Description...' required />
                </x-modal>
            </form>
        </div>
    @endsection
</x-wms-layout>
