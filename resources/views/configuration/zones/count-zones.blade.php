<x-wms-layout>
    @section('content')
        <div class="h-auto" x-data="{ showModal: false }">
            @include('configuration.navbar')
            @include('flash-message')
            <x-page.title>{{ __('Count Zones') }}</x-page.title>
            <x-table>
                <x-buttons.primary>{{ __('Add New') }}</x-buttons.primary>
                <x-slot name="headings">
                    <x-table.heading>{{ __('Name') }}</x-table.heading>
                    <x-table.heading>{{ __('Description') }}</x-table.heading>
                    <x-table.heading>{{ __('Locations') }}</x-table.heading>
                    <x-table.heading>{{ __('Actions') }}</x-table.heading>
                </x-slot>
                @forelse ($countZones as $zone)
                    <x-table.row>
                        <x-table.cell>
                            <x-page.link href="#">{{ $zone->name }}</x-page.link>
                        </x-table.cell>
                        <x-table.cell>{{ $zone->description }}</x-table.cell>
                        <x-table.cell>
                            <x-page.link href="#">{{ $zone->locations_count }}</x-page.link>
                        </x-table.cell>
                        <x-table.actions>
                            <x-slot name="edit">#</x-slot>
                            <x-slot name="delete">#</x-slot>
                        </x-table.actions>
                    </x-table.row>
                @empty
                    <x-table.row>
                        <x-table.cell>{{ __('No Count Zones Found.') }}</x-table.cell>
                    </x-table.row>
                @endforelse
            </x-table>
            {{ $countZones->links() }}
            <!--new count Zone modal-->
            <form method="POST" action="{{ route('config.countZones.store') }}">
                @csrf
                <x-modal>
                    <x-slot name="title">{{ __('New Count Zone') }}</x-slot>
                    <x-modal.input.textarea name="name" placeholder='Name...' required />
                    <x-modal.input.textarea name="description" placeholder='Description...' required />
                </x-modal>
            </form>
        </div>
    @endsection
</x-wms-layout>
