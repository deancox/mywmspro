<x-wms-layout>
    @section('content')
        <div class="h-auto" x-data="{ showModal: false }">
            @include('configuration.navbar')
            @include('flash-message')
            <x-page.title>{{ __('Location Types') }}</x-page.title>
            <x-table>
                <x-buttons.primary>{{ __('Add New') }}</x-buttons.primary>
                <x-slot name="headings">
                    <x-table.heading>{{ __('Name') }}</x-table.heading>
                    <x-table.heading>{{ __('Description') }}</x-table.heading>
                    <x-table.heading>{{ __('Locations') }}</x-table.heading>
                    <x-table.heading>{{ __('Actions') }}</x-table.heading>
                </x-slot>
                @forelse ($locationTypes as $locationType)
                    <x-table.row>
                        <x-table.cell>{{ $locationType->name }}</x-table.cell>
                        <x-table.cell>{{ $locationType->description }}</x-table.cell>
                        <x-table.cell>
                            <x-page.link href="{{ route('config.showLocations', [$locationType]) }}">
                                {{ $locationType->locations_count }}</x-page.link>
                        </x-table.cell>
                        @if ($locationType->created_by !== 'DEFAULT')
                            <x-table.actions>
                                <x-slot name="edit">#</x-slot>
                                <x-slot name="delete">#</x-slot>
                            </x-table.actions>
                        @else
                            <x-table.cell>{{ __('Created By Default') }}</x-table.cell>
                        @endif
                    </x-table.row>
                @empty
                    <x-table.row>
                        <x-table.cell>{{ __('No Location Types Found.') }}</x-table.cell>
                    </x-table.row>
                @endforelse
            </x-table>
            {{ $locationTypes->links() }}
            <!--new location type modal-->
            <form method="POST" action="{{ route('config.locationTypes.store') }}">
                @csrf
                <x-modal>
                    <x-slot name="title">{{ __('New Location Type') }}</x-slot>
                    <x-modal.infotext>
                        {{ __('Location Types are to group locations for specific activities like rework or constraint processing for example.') }}
                    </x-modal.infotext>
                    <x-modal.input.textarea name="name" placeholder='Name...' required />
                    <x-modal.input.textarea name="description" placeholder='Description...' required />
                </x-modal>
            </form>
        </div>
    @endsection
</x-wms-layout>
