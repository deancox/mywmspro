<div class="flex sticky top-16 h-14 justify-items-start gap-4 text-gray-800 pl-2 font-semibold bg-white border-b-2 border-gray-500">
    <ul class="inline-flex items-center">
        <li x-data="{locationOptions:false}" class="px-2">
            <button class="font-semibold hover:text-blue-400 outline-none focus:outline-none"
                @click="locationOptions = !locationOptions">{{ __('Warehouse') }}
                <svg xmlns="http://www.w3.org/2000/svg" class="h-4 w-4 inline" fill="none" viewBox="0 0 24 24"
                    stroke="currentColor">
                    <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M19 9l-7 7-7-7" />
                </svg>
            </button>
            <div class="absolute bg-white text-black  rounded-md p-2 mt-5 shadow-lg"
                :class="{'hidden':!locationOptions,'flex flex-col':locationOptions}"
                @click.away="locationOptions = false">
                <ul class="space-y-2">
                    <li>
                        <a href="{{ route('config.locationTypes.index') }}"
                            class="flex p-2 rounded-md hover:bg-gray-200">{{ __('Location Types') }}</a>
                    </li>
                    <li>
                        <a href="{{ route('config.storageZones.index') }}"
                            class="flex p-2 rounded-md hover:bg-gray-200">{{ __('Storage Zones') }}</a>
                    </li>
                    <li>
                        <a href="{{ route('config.pickZones.index') }}"
                            class="flex p-2 rounded-md hover:bg-gray-200">{{ __('Pick Zones') }}</a>
                    </li>
                    <li>
                        <a href="{{ route('config.countZones.index') }}"
                            class="flex p-2 rounded-md hover:bg-gray-200">{{ __('Count Zones') }}</a>
                    </li>
                    <li>
                        <a href="{{ route('config.workTypes.index') }}"
                            class="flex p-2 rounded-md hover:bg-gray-200">{{ __('Work Types') }}</a>
                    </li>
                </ul>
            </div>
        </li>
        <li x-data="{productOptions:false}" class="px-2">
            <button class="font-semibold outline-none focus:outline-none hover:text-blue-400"
                @click="productOptions = !productOptions">{{ __('Data Uploads') }}
                <svg xmlns="http://www.w3.org/2000/svg" class="h-4 w-4 inline" fill="none" viewBox="0 0 24 24"
                    stroke="currentColor">
                    <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M19 9l-7 7-7-7" />
                </svg></button>
            <div class="absolute bg-white text-black  rounded-md p-2 mt-5 shadow-lg"
                :class="{'hidden':!productOptions,'flex flex-col':productOptions}" @click.away="productOptions = false">
                <ul class="space-y-2">
                    <li>
                        <a href="{{ route('config.configupload.index') }}"
                            class="flex p-2 rounded-md hover:bg-yellow-300 bg-yellow-100">{{ __('Locations/Zones Upload') }}</a>
                    </li>
                    <li>
                        <a href="#" class="flex p-2 bg-red-200 rounded-md hover:bg-yellow-300">{{ __('Mobiles Upload') }}</a>
                    </li>
                    <li>
                        <a href="#" class="flex p-2 bg-red-200 rounded-md hover:bg-yellow-300">{{ __('Printers Upload') }}</a>
                    </li>
                    <li>
                        <a href="#" class="flex p-2 bg-red-200 rounded-md hover:bg-yellow-300">{{ __('Products Upload') }}</a>
                    </li>
                    <li>
                        <a href="#"
                            class="flex p-2 bg-red-200 rounded-md hover:bg-yellow-300">{{ __('Product Details Upload') }}</a>
                    </li>
                    <li>
                        <a href="#"
                            class="flex p-2 bg-red-200 rounded-md hover:bg-yellow-300">{{ __('Pickfaces Upload') }}</a>
                    </li>
                </ul>
            </div>
        </li>
        <li x-data="{equipmentOptions:false}" class="px-2">
            <button class="font-semibold outline-none focus:outline-none hover:text-blue-400"
                @click="equipmentOptions = !equipmentOptions">{{ __('Hardware') }}
                <svg xmlns="http://www.w3.org/2000/svg" class="h-4 w-4 inline" fill="none" viewBox="0 0 24 24"
                    stroke="currentColor">
                    <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M19 9l-7 7-7-7" />
                </svg></button>
            <div class="absolute bg-white text-black  rounded-md p-2 mt-5 shadow-lg"
                :class="{'hidden':!equipmentOptions,'flex flex-col':equipmentOptions}" @click.away="equipmentOptions = false">
                <ul class="space-y-2">
                    <li>
                        <a href="{{ route('config.deviceTypes.index') }}" class="flex p-2 rounded-md hover:bg-gray-200">{{ __('Mobile Device types') }}</a>
                    </li>
                    <li>
                        <a href="{{ route('config.devices.index') }}" class="flex p-2 rounded-md hover:bg-gray-200">{{ __('Mobile Devices') }}</a>
                    </li>
                    <li>
                        <a href="{{ route('config.mobileConfig.index') }}" class="flex p-2 rounded-md hover:bg-gray-200">{{ __('Mobile Display Options') }}</a>
                    </li>
                    <li>
                        <a href="#" class="flex p-2 bg-red-200 rounded-md hover:bg-gray-200">{{ __('Device Location Access') }}</a>
                    </li>
                    <li>
                        <a href="{{ route('config.printerTypes.index') }}" class="flex p-2 rounded-md hover:bg-gray-200">{{ __('Printer Types') }}</a>
                    </li>
                    <li>
                        <a href="{{ route('config.printers.index') }}" class="flex p-2 rounded-md hover:bg-gray-200">{{ __('Printers') }}</a>
                    </li>

                </ul>
            </div>
        </li>
        <li x-data="{outboundOptions:false}" class="px-2">
            <button class="font-semibold outline-none focus:outline-none hover:text-blue-400"
                @click="outboundOptions = !outboundOptions">{{ __('Outbound') }}
                <svg xmlns="http://www.w3.org/2000/svg" class="h-4 w-4 inline" fill="none" viewBox="0 0 24 24"
                    stroke="currentColor">
                    <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M19 9l-7 7-7-7" />
                </svg></button>
            <div class="absolute bg-white text-black  rounded-md p-2 mt-5 shadow-lg"
                :class="{'hidden':!outboundOptions,'flex flex-col':outboundOptions}" @click.away="outboundOptions = false">
                <ul class="space-y-2">
                    <li>
                        <a href="#" class="flex p-2 bg-red-200 rounded-md hover:bg-yellow-300">{{ __('Allocation Rules') }}</a>
                    </li>
                    <li>
                        <a href="#" class="flex p-2 bg-red-200 rounded-md hover:bg-yellow-300">{{ __('Carrier Rules') }}</a>
                    </li>
                    <li>
                        <a href="{{ route('config.pickTypes.index') }}" class="flex p-2 rounded-md hover:bg-gray-200">{{ __('Pick Types') }}</a>
                    </li>
                    <li>
                        <a href="#" class="flex p-2 bg-red-200 rounded-md hover:bg-gray-200">{{ __('Pick Verification') }}</a>
                    </li>
                    <li>
                        <a href="#" class="flex p-2 bg-red-200 rounded-md hover:bg-gray-200">{{ __('Outbound Paths') }}</a>
                    </li>
                    <li>
                        <a href="#" class="flex p-2 bg-red-200 rounded-md hover:bg-gray-200">{{ __('Outbound Staging') }}</a>
                    </li>
                </ul>
            </div>
        </li>
        <li x-data="{replenOptions:false}" class="px-2">
            <button class="font-semibold outline-none focus:outline-none hover:text-blue-400"
                @click="replenOptions = !replenOptions">{{ __('Replenishment') }}
                <svg xmlns="http://www.w3.org/2000/svg" class="h-4 w-4 inline" fill="none" viewBox="0 0 24 24"
                    stroke="currentColor">
                    <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M19 9l-7 7-7-7" />
                </svg></button>
            <div class="absolute bg-white text-black  rounded-md p-2 mt-5 shadow-lg"
                :class="{'hidden':!replenOptions,'flex flex-col':replenOptions}" @click.away="replenOptions = false">
                <ul class="space-y-2">
                    <li>
                        <a href="#" class="flex p-2 bg-red-200 rounded-md hover:bg-gray-200">{{ __('Pickfaces') }}</a>
                    </li>
                    <li>
                        <a href="#" class="flex p-2 bg-red-200 rounded-md hover:bg-gray-200">{{ __('Replenishment Rules') }}</a>
                    </li>
                </ul>
            </div>
        </li>
        <li x-data="{inboundOptions:false}" class="px-2">
            <button class="font-semibold outline-none focus:outline-none hover:text-blue-400"
                @click="inboundOptions = !inboundOptions">{{ __('Inbound') }}
                <svg xmlns="http://www.w3.org/2000/svg" class="h-4 w-4 inline" fill="none" viewBox="0 0 24 24"
                    stroke="currentColor">
                    <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M19 9l-7 7-7-7" />
                </svg></button>
            <div class="absolute bg-white text-black  rounded-md p-2 mt-5 shadow-lg"
                :class="{'hidden':!inboundOptions,'flex flex-col':inboundOptions}" @click.away="inboundOptions = false">
                <ul class="space-y-2">
                    <li>
                        <a href="{{ route('config.inboundConfig.index') }}" class="flex p-2 rounded-md hover:bg-gray-200">{{ __('Inbound Settings') }}</a>
                    </li>
                    <li>
                        <a href="#" class="flex p-2 bg-red-200 rounded-md hover:bg-gray-200">{{ __('Inbound Types') }}</a>
                    </li>
                    <li>
                        <a href="{{ route('config.storageRules.index') }}" class="flex p-2 rounded-md hover:bg-gray-200">{{ __('Storage Rules') }}</a>
                    </li>
                </ul>
            </div>
        </li>
        <li x-data="{integrationOptions:false}" class="px-2">
            <button class="font-semibold outline-none focus:outline-none hover:text-blue-400"
                @click="integrationOptions = !integrationOptions">{{ __('Integration') }}
                <svg xmlns="http://www.w3.org/2000/svg" class="h-4 w-4 inline" fill="none" viewBox="0 0 24 24"
                    stroke="currentColor">
                    <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M19 9l-7 7-7-7" />
                </svg></button>
            <div class="absolute bg-white text-black  rounded-md p-2 mt-5 shadow-lg w-1/12"
                :class="{'hidden':!integrationOptions,'flex flex-col':integrationOptions}" @click.away="integrationOptions = false">
                <ul class="space-y-2">
                    <li>
                        <a href="#" class="flex p-2 bg-red-200 rounded-md hover:bg-yellow-300">{{ __('Host') }}</a>
                    </li>
                    <li>
                        <a href="#" class="flex p-2 bg-red-200 rounded-md hover:bg-yellow-300">{{ __('Carrier') }}</a>
                    </li>
                    <li>
                        <a href="#" class="flex p-2 bg-red-200 rounded-md hover:bg-yellow-300">{{ __('Events') }}</a>
                    </li>
                </ul>
            </div>
        </li>
        <li x-data="{invOptions:false}" class="px-2">
            <button class="font-semibold outline-none focus:outline-none hover:text-blue-400"
                @click="invOptions = !invOptions">{{ __('Inventory') }}
                <svg xmlns="http://www.w3.org/2000/svg" class="h-4 w-4 inline" fill="none" viewBox="0 0 24 24"
                    stroke="currentColor">
                    <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M19 9l-7 7-7-7" />
                </svg></button>
            <div class="absolute bg-white text-black  rounded-md p-2 mt-5 shadow-lg"
                :class="{'hidden':!invOptions,'flex flex-col':invOptions}" @click.away="invOptions = false">
                <ul class="space-y-2">
                    <li>
                        <a href="{{ route('config.productTypes.index') }}"
                            class="flex p-2 rounded-md hover:bg-gray-200">{{ __('Product Types') }}</a>
                    </li>
                    <li>
                        <a href="{{ route('config.UOMS.index') }}" class="flex p-2 rounded-md hover:bg-gray-200">{{ __('UOM\'s') }}</a>
                    </li>
                    <li>
                        <a href="#" class="flex p-2 bg-red-200 rounded-md hover:bg-gray-200">{{ __('Mixing Restrictions') }}</a>
                    </li>
                    <li>
                        <a href="#" class="flex p-2 bg-red-200 rounded-md hover:bg-gray-200">{{ __('Count Settings') }}</a>
                    </li>
                    <li>
                        <a href="#" class="flex p-2 bg-red-200 rounded-md hover:bg-yellow-300">{{ __('Barcode Types') }}</a>
                    </li>
                    <li>
                        <a href="#" class="flex p-2 bg-red-200 rounded-md hover:bg-yellow-300">{{ __('Barcode Settings') }}</a>
                    </li>
                </ul>
            </div>
        </li>
        <li class="px-2">
            <button class="font-semibold outline-none focus:outline-none hover:text-blue-400"><a
                    href="#">{{ __('Jobs') }}</a></button>
        </li>
        <li class="px-2">
            <button class="font-semibold outline-none focus:outline-none hover:text-blue-400"><a
                    href="#">{{ __('VAS') }}</a></button>
        </li>
    </ul>
</div>
