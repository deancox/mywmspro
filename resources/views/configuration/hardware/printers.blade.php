<x-wms-layout>
    @section('content')
        <div class="h-auto" x-data="{ showModal: false }">
            @include('configuration.navbar')
            @include('flash-message')
            <x-page.title>{{ __('Printers') }}</x-page.title>
            <x-table>
                <x-buttons.primary>{{ __('Add New') }}</x-buttons.primary>
                <x-slot name="headings">
                    <x-table.heading>{{ __('Name') }}</x-table.heading>
                    <x-table.heading>{{ __('Type') }}</x-table.heading>
                    <x-table.heading>{{ __('IP Address') }}</x-table.heading>
                    <x-table.heading>{{ __('Enable/Disable') }}</x-table.heading>
                </x-slot>
                @forelse ($printers as $printer)
                    <x-table.row>
                        <x-table.cell>
                            <x-page.link href="#">{{ $printer->name }}</x-page.link>
                        </x-table.cell>
                        <x-table.cell>{{ $printer->printerType->type }}</x-table.cell>
                        <x-table.cell>{{ $printer->ip }}</x-table.cell>
                        <x-table.cell>
                            @if ($printer->enabled === 1)
                                <i class="fas fa-check text-green-500 fa-lg onOff" id="toggle_{{ $printer->id }}"></i>
                            @else
                                <i class="fas fa-times text-red-500 fa-lg onOff" id="toggle_{{ $printer->id }}"></i>
                            @endif
                        </x-table.cell>
                        <x-table.cell class="hidden">
                            {{ $printer->id }}
                        </x-table.cell>
                    </x-table.row>
                @empty
                    <x-table.row>
                        <x-table.cell>{{ __('No Printers Found.') }}</x-table.cell>
                    </x-table.row>
                @endforelse
            </x-table>
            {{ $printers->links() }}
            <!--new printer modal-->
            <form method="POST" action="{{ route('config.printers.store') }}">
                @csrf
                <x-modal>
                    <x-slot name="title">{{ __('Add Printer') }}</x-slot>
                    <x-modal.input.textarea name="name" placeholder='Name...' required />
                    <x-modal.input.textarea name="ip" placeholder='IP Address...' required />
                    <x-modal.input.dropdown label="{{ __('Printer Type:') }}" name="printer_type_id" id="printer_type_id"
                        required>
                        <option value=""></option>
                        @forelse ($printerTypes as $printerType)
                            <option value="{{ $printerType->id }}">{{ $printerType->type }}
                            </option>
                        @empty
                            <option value="">{{ __('No Printer Types Found.') }}</option>
                        @endforelse
                    </x-modal.input.dropdown>
                </x-modal>
            </form>
        </div>
        <script>
            $(function() {
                $('.onOff').click(function() {

                    var id = $.trim($(this).closest('tr').find('td:eq(4)').text());

                    $.ajax({
                        headers: {
                            'X-HTTP-Method-Override': 'PATCH'
                        },
                        type: "POST",
                        url: 'printers/' + id,
                        data: {
                            "printer": id,
                            "_token": "{{ csrf_token() }}"
                        },
                        success: function(response) {
                            $("#toggle_" + id).toggleClass('fa-check text-green-500').toggleClass(
                                'fa-times text-red-400');
                            console.log('updated');
                        },
                        error: function(req, status, error) {
                            console.log(error);
                        }
                    });
                });
            });
        </script>
    @endsection
</x-wms-layout>
