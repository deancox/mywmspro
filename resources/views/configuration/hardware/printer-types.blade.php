<x-wms-layout>
    @section('content')
        <div class="h-auto" x-data="{ showModal: false }">
            @include('configuration.navbar')
            @include('flash-message')
            <x-page.title>{{ __('Printer Types') }}</x-page.title>
            <x-table>
                <x-buttons.primary>{{ __('Add New') }}</x-buttons.primary>
                <x-slot name="headings">
                    <x-table.heading>{{ __('Type') }}</x-table.heading>
                    <x-table.heading>{{ __('Printers') }}</x-table.heading>
                </x-slot>
                @forelse ($printerTypes as $printerType)
                    <x-table.row>
                        <x-table.cell>{{ $printerType->type }}</x-table.cell>
                        <x-table.cell>{{ $printerType->printers_count }}</x-table.cell>
                    </x-table.row>
                @empty
                    <x-table.row>
                        <x-table.cell>{{ __('No Printer Types Found.') }}</x-table.cell>
                    </x-table.row>
                @endforelse
            </x-table>
            <!--new printer type modal-->
            <form method="POST" action="{{ route('config.printerTypes.store') }}">
                @csrf
                <x-modal>
                    <x-slot name="title">{{ __('New Printer Type') }}</x-slot>
                    <x-modal.input.textarea name="type" placeholder="Type..." required />
                </x-modal>
            </form>
        </div>
    @endsection
</x-wms-layout>
