<x-wms-layout>
    @section('content')
    <div class="h-full w-full">
    @include('outbound.navbar')
    <div class="flex justify-between justify-items-center h-14 shadow-lg">
        <h1 class="flex font-semibold text-2xl px-4 items-center py-2">{{__('Constraints (TODO)') }}</h1>
    </div>
    <h1>constraints by customer
        <p>
            on/off <br />
            on/off by customer <br />
            on/off by product (add this to product settings) <br />
        </p>    
    </h1>
    @endsection
    </div>
</x-wms-layout>