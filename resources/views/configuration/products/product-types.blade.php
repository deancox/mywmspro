<x-wms-layout>
    @section('content')
        <div class="h-auto" x-data="{ showModal: false }">
            @include('configuration.navbar')
            @include('flash-message')
            <x-page.title>{{ __('Product Types') }}</x-page.title>
            <x-table>
                <x-buttons.primary>{{ __('Add New') }}</x-buttons.primary>
                <x-slot name="headings">
                    <x-table.heading>{{ __('Type') }}</x-table.heading>
                    <x-table.heading>{{ __('Description') }}</x-table.heading>
                    <x-table.heading>{{ __('Created') }}</x-table.heading>
                    <x-table.heading>{{ __('Modified') }}</x-table.heading>
                    <x-table.heading>{{ __('Created By') }}</x-table.heading>
                </x-slot>
                @forelse ($productTypes as $type)
                    <x-table.row>
                        <x-table.cell>{{ $type->name }}</x-table.cell>
                        <x-table.cell>{{ $type->description }}</x-table.cell>
                        <x-table.cell>{{ $type->created_at }}</x-table.cell>
                        <x-table.cell>{{ $type->updated_at }}</x-table.cell>
                        <x-table.cell>{{ $type->created_by }}</x-table.cell>
                    </x-table.row>
                @empty
                    <x-table.row>
                        <x-table.cell>{{ __('No Product Types Found.') }}</x-table.cell>
                    </x-table.row>
                @endforelse
            </x-table>
            {{ $productTypes->links() }}
            <!--new device type modal-->
            <form method="POST" action="{{ route('config.productTypes.store') }}">
                @csrf
                <x-modal>
                    <x-slot name="title">{{ __('New Product Type') }}</x-slot>
                    <x-modal.input.textarea name="name" placeholder='Name...' required />
                    <x-modal.input.textarea name="description" placeholder='Description...' required />
                </x-modal>
            </form>
        </div>
    @endsection
</x-wms-layout>
