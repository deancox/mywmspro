<x-wms-layout>
    @section('content')
        <div class="flex-1 bg-blue-50">
            @include('configuration.navbar')
            @include('flash-message')
            <div class="flex justify-between justify-items-center">
                <h1 class="flex font-semibold text-2xl px-4 items-center py-2">{{ __('Mobile Display Configuration') }}
                </h1>
            </div>
            <div class="flex justify-around gap-8 w-full">
                <form class="w-1/2">
                    <div class="p-4 shadow-lg border-2 border-gray-300 mt-2 mx-4 rounded-lg bg-white w-full">
                        <div class="flex-1 justify-between">
                            <h1 class="flex font-semibold text-xl items-center">{{ __('Mobile Main Menu') }}</h1>
                            <p class="text-sm pb-2">
                                {{ __('Drag Menu items to match how you want them to be displayed on mobile devices.') }}
                            </p>
                            <p class="text-sm pb-2">
                                {{ __('Click on a Menu to manage access by role or to rename, ') }}<span
                                    class="font-bold">{{ __('you cannot add new menus.') }}
                                </span>
                            </p>
                            <p class="text-sm text-red-500 pb-2 font-semibold">
                                {{ __('Note: If you disable the menu here it will not be visible to ANY mobile user.') }}
                            </p>
                        </div>
                        <table class="w-full bg-white text-left">
                            <thead class="w-full">
                                <tr class="w-full border-b border-black bg-blue-50 ">
                                    <th class=" pl-2">{{ __('Menu') }}</th>
                                    <th class="">{{ __('Screen') }}</th>
                                    <th class="text-right pr-4">{{ __('Enabled') }}</th>
                                </tr>
                            </thead>
                            <tbody class="bg-grey-light" id="menuList">
                                @forelse ($menus as $menu)
                                    <tr class="border-b-2 hover:bg-gray-100 text-left text-sm pl-2"
                                        id="{{ $menu->id }}">
                                        <td class="pl-2 py-2 text-md">
                                            <a href="#">{{ $menu->description }}</a>
                                        </td>
                                        <td class="">
                                            {{ ucfirst($menu->screen) }}
                                        </td>
                                        <td class="text-right pr-8">
                                            <label data-toggle="checkbox-toggle" data-rounded="rounded-xl"
                                                data-handle-size="12" data-handle-color="bg-white"
                                                data-off-color="bg-gray-400" data-on-color="bg-blue-400">
                                                <input type="checkbox" name="visible" class="onOff"
                                                    value="{{ $menu->visible }}"
                                                    {{ $menu->visible ? 'checked' : '' }} />
                                        </td>
                                        <td class="hidden">
                                            {{ $menu->id }}
                                        </td>
                                    </tr>
                                @empty
                                    <td>{{ __('No menus Found.') }}</td>
                                @endforelse
                            </tbody>
                        </table>
                </form>
            </div>
            <div class="p-4 shadow-lg border-2 border-gray-300 mx-4 mt-2 rounded-lg bg-white w-full">
                <h1 class="flex font-semibold text-xl items-center">{{ __('Advanced Configuration') }}</h1>
                <p class="text-sm pb-2">{{ __('Here you can manage sub menus and specifc screen displays.') }}</p>
                <p class="text-sm text-red-500 pb-2 font-semibold">
                    {{ __('Note: You cannot disable or remove a field or menu if it is required for any form of verfication (for example Pick Verification)') }}
                </p>
                <table class="w-full bg-white text-left">
                    <thead class="w-full">
                        <tr class="w-full border-b border-black bg-blue-50">
                            <th class=" pl-2">{{ __('Screen Name') }}</th>
                        </tr>
                    </thead>
                    <tbody class="bg-grey-light">
                        @forelse ($menus as $menu)
                            <tr class="border-b-2 hover:bg-gray-100 text-left text-sm pl-2">
                                <td class="pl-2 py-2">
                                    <x-page.link href="#">
                                        {{ $menu->description }}
                                    </x-page.link>
                                </td>
                            </tr>
                        @empty
                            <td>{{ __('No menus Found.') }}</td>
                        @endforelse
                    </tbody>
                </table>
            </div>
        </div>
        </div>
        <script>
            $(function() {
                var fixHelper = function(e, ui) {
                    ui.children().each(function() {
                        $(this).width($(this).width());
                    });
                    return ui;
                }

                $('#menuList').sortable({
                    helper: fixHelper,
                    axis: 'y',
                    stop: function() {
                        var sorted = new Array();
                        $('#menuList>tr').each(function() {
                            sorted.push($(this).attr("id"));
                        });

                        updateDisplayOrder(sorted);

                        console.log(sorted);
                    },
                });
            });

            function updateDisplayOrder(newOrder) {

                $.ajax({

                    type: "POST",
                    url: '{{ url('/') }}/Configuration/mobileConfigSort/',
                    data: {
                        displayOrder: newOrder,
                        "_token": "{{ csrf_token() }}"
                    },
                    success: function() {
                        console.log('positions updated');
                        //location.reload();
                    },
                    error: function(req, status, error) {
                        console.log(error);
                    },
                });
            };

            $(function() {
                $('.onOff').click(function() {

                    var screen = $.trim($(this).closest('tr').find('td:eq(1)').text())
                        .toLowerCase();
                    var id = $.trim($(this).closest('tr').find('td:eq(3)').text());


                    $.ajax({
                        type: "PATCH",
                        url: 'mobileConfig/' + id,
                        data: {
                            "mobileMenu": id,
                            "screen": screen,
                            "_token": "{{ csrf_token() }}"
                        },
                        success: function(response) {

                        },
                        error: function(req, status, error) {
                            console.log(error);
                        }
                    });
                });
            });
        </script>
    @endsection
</x-wms-layout>
