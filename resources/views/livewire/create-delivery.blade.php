<div>
    <div class="flex justify-between justify-items-center">
        <h1 class="flex font-semibold text-2xl px-4 items-center py-2">{{ __('Create Delivery') }}</h1>
    </div>
    <form wire:submit.prevent="createDelivery">
        @csrf
        <div>
            @if (session()->has('message'))
                <div class="alert alert-success">
                    {{ session('message') }}
                </div>
            @endif
        </div>
        <h1 class="flex font-bold text-xl px-4 items-center py-2 border-b-2  border-gray-600 bg-gray-200 mx-8 mt-2">
            {{ __('Delivery Details') }}</h1>
        <div class="flex justify-between  bg-gray-100 mx-8 mt-2 border-2 border-gray-300 rounded-lg shadow-lg">
            <div class="flex-grow h-full p-4 overflow-y-auto rounded-lg">
                <label for="reference" class=" font-semibold">{{ __('Reference:') }}</label>
                <br>
                <input type="text" wire:model="reference" name="reference" id="reference"
                    placeholder="System Generated if Blank"
                    class="rounded-lg focus:outline-none focus:ring-0 focus:bg-gray-100 w-full">
                @error('reference') <span class="error text-red-500">{{ $message }}</span> @enderror
            </div>
            <div class="flex-grow h-full p-4 overflow-y-auto rounded-lg">
                <label for="supplier" class=" font-semibold">{{ __('Supplier:') }}</label>
                <br>
                <select wire:model="supplier" name="supplier" id="supplier"
                    class="rounded-lg focus:outline-none focus:ring-0 focus:bg-gray-100 w-full">
                    <option value=""></option>
                    @forelse ($suppliers as $supplier)
                        <option value="{{ $supplier->id }}">{{ $supplier->name }}</option>
                    @empty
                        <option value="">{{ __('No Suppliers Found') }}</option>
                    @endforelse
                </select>
                @error('supplier') <span class="error text-red-500">{{ $message }}</span> @enderror
            </div>
            <div class="flex-grow h-full p-4 overflow-y-auto rounded-lg">
                <label for="dueDate" class="font-semibold">{{ __('Due Date:') }}</label>
                <br>
                <input type="date" wire:model="dueDate" name="dueDate" id="dueDate"
                    class="rounded-lg focus:outline-none focus:ring-0 focus:bg-gray-100 w-full">
                @error('dueDate') <span class="error text-red-500">{{ $message }}</span> @enderror
            </div>
            <div class="flex-grow h-full p-4 overflow-y-auto rounded-lg">
                <label for="dueDate" class="font-semibold">{{ __('Due Time (HHMM):') }}</label>
                <br>
                <input type="number" wire:model.debounce.1000ms="time" name="time" id="time" placeholder="HHMM"
                    class="rounded-lg focus:outline-none focus:ring-0 focus:bg-gray-100 w-full">
                @error('time') <span class="error text-red-500">{{ $message }}</span> @enderror
            </div>
            <div class="flex-grow h-full p-4 overflow-y-auto rounded-lg">
                <label for="shorts_allowed_flg" class="font-semibold">{{ __('Shorts Allowed:') }}</label>
                <br>
                <input type="checkbox" wire:model="shorts_allowed_flg" name="shorts_allowed_flg" id="shorts_allowed_flg"
                    class=" focus:outline-none focus:ring-0 focus:bg-gray-200 rounded">
                @error('shorts_allowed_flg') <span class="error text-red-500">{{ $message }}</span> @enderror
            </div>
        </div>
        <div>
            <h1
                class="flex font-bold text-xl px-4 py-2 items-center border-b-2  border-gray-600 bg-gray-200 mx-8 mt-2">
                {{ __('Line Details') }}
            </h1>
        </div>
        <div class="flex flex-col justify-start bg-red-300 rounded mx-32 mt-2 px-2">
            <div>
                @error('lines.*.product') <span
                        class="error text-red-500 font-bold">{{ __('Product is Required on Each Line.') }}</span>
                @enderror
            </div>
            <div>
                @error('lines.*.quantity') <span
                    class="error text-red-500 font-bold">{{ __('Quantity is Required on Each Line') }}</span> @enderror
            </div>
            <div>
                @error('lines.*.uom') <span
                        class="error text-red-500 font-bold">{{ __('Uom is Required on Each Line.') }}</span>
                @enderror
            </div>
        </div>
        @foreach ($lines as $index => $line)
            <div>
                <div class="flex justify-around bg-gray-100 border-2 border-gray-300 rounded-lg mx-8 pb-2 shadow-lg">
                    <div class="flex items-end pb-4">
                        <button wire:click.prevent="addLine"
                            class="class=hover:border-gray-300 rounded-lg pl-4 py-2 font-bold text-sm focus:outline-none">
                            <svg xmlns="http://www.w3.org/2000/svg" class="h-6 w-6 text-green-500" fill="none"
                                viewBox="0 0 24 24" stroke="currentColor">
                                <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2"
                                    d="M12 9v3m0 0v3m0-3h3m-3 0H9m12 0a9 9 0 11-18 0 9 9 0 0118 0z" />
                            </svg>
                        </button>
                    </div>
                    <div class="flex-grow p-2 overflow-y-auto rounded-lg">
                        <label for="product" class="font-semibold">{{ __('Product:') }}</label>
                        <br>
                        <select wire:model="lines.{{ $index }}.product" id="product"
                            class="rounded-lg focus:outline-none focus:ring-0 focus:bg-gray-100 w-full">
                            <option value=""></option>
                            @forelse ($products as $product)
                                <option value="{{ $product->product }}">{{ $product->product }}</option>
                            @empty
                                <option value="">{{ __('No Products Found') }}</option>
                            @endforelse
                        </select>
                    </div>
                    <div class="flex-grow h-full p-2 overflow-y-auto rounded-lg">
                        <label for="batch" class=" font-semibold">{{ __('Batch:') }}</label>
                        <br>
                        <input type="text" wire:model="lines.{{ $index }}.batch" id="batch"
                            placeholder="Optional" value="batch"
                            class="rounded-lg focus:outline-none focus:ring-0 focus:bg-gray-100 w-full">
                    </div>
                    <div class="flex-grow h-full p-2 overflow-y-auto rounded-lg">
                        <label for="cust_batch" class=" font-semibold">{{ __('Customer Batch:') }}</label>
                        <br>
                        <input type="text" wire:model="lines.{{ $index }}.cust_batch" id="cust_batch"
                            placeholder="Optional" value="cust_batch"
                            class="rounded-lg focus:outline-none focus:ring-0 focus:bg-gray-100 w-full">
                    </div>
                    <div class="flex-grow h-full p-2 overflow-y-auto rounded-lg ">
                        <label for="quantity" class=" font-semibold">{{ __('Quantity:') }}</label>

                        <br>
                        <input type="number" wire:model="lines.{{ $index }}.quantity" id="quantity"
                            value="quantity" min="1"
                            class="rounded-lg focus:outline-none focus:ring-0 focus:bg-gray-100 w-full">
                    </div>
                    <div class="flex-grow h-full p-2 overflow-y-auto rounded-lg">
                        <label for="uom" class=" font-semibold">{{ __('UOM:') }}</label>
                        <br>
                        <select wire:model="lines.{{ $index }}.uom" id="uom"
                            class="rounded-lg focus:outline-none focus:ring-0 focus:ring-transparent w-full">
                            <option value="">{{ __('Required') }}</option>
                            @forelse ($uoms as $uom)
                                <option value="{{ $uom->name }}">{{ $uom->description }}</option>
                            @empty
                                <option value="">{{ __('No Uoms Found') }}</option>
                            @endforelse
                        </select>
                    </div>
                    <div class="flex items-end pb-4">
                        <button wire:click.prevent="deleteLine({{ $index }})" class="hover:border-gray-300 rounded-lg pr-4 py-2 font-bold text-sm focus:outline-none">
                            <svg xmlns="http://www.w3.org/2000/svg" class="h-6 w-6 text-red-600" fill="none"
                                viewBox="0 0 24 24" stroke="currentColor">
                                <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2"
                                    d="M19 7l-.867 12.142A2 2 0 0116.138 21H7.862a2 2 0 01-1.995-1.858L5 7m5 4v6m4-6v6m1-10V4a1 1 0 00-1-1h-4a1 1 0 00-1 1v3M4 7h16" />
                            </svg>
                        </button>
                    </div>
                </div>
            </div>
        @endforeach
        <div class="flex flex-row-reverse mr-12 mt-2 gap-2">
            <a href="{{ route('inbound.deliveries.index') }}"
                class="p-2 bg-red-400 rounded hover:bg-red-500">{{ __('Cancel') }}</a>
            <button type="submit" class="p-2 bg-green-400 rounded hover:bg-green-500">{{ __('Save') }}</button>
        </div>
    </form>
</div>
