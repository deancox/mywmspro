<div class="flex items-center pl-6">

    <div class="relative text-gray-600 focus-within:text-gray-400">
        <span class="absolute inset-y-0 left-0 flex items-center pl-2">
          <button type="submit" class="p-1 focus:outline-none focus:shadow-outline">
            <svg fill="none" stroke="currentColor" stroke-linecap="round" stroke-linejoin="round" stroke-width="2" viewBox="0 0 24 24" class="w-4 h-4"><path d="M21 21l-6-6m2-5a7 7 0 11-14 0 7 7 0 0114 0z"></path></svg>
          </button>
        </span>
        <input type="search" name="q" class="py-2 text-sm text-white rounded-md pl-10 pr-60 focus:outline-none focus:bg-white focus:text-gray-900" placeholder="Search..." autocomplete="off">
      </div>
    <!--</div>-->
    {{-- <div wire:loading.remove class="absolute bg-white rounded mt-4 w-full">
            <ul>
                @foreach ($taskResults as $result)
                    <li class="hover:bg-gray-200 cursor-pointer py-2 px-2 border-b">
                        <div class="flex justify-between row-span-1">
                        <a href="{{ route('tasks.show', $result['id']) }}">{{ $result->title }}</a>
                        <h3 class="font-semibold bg-green-300 rounded-xl text-sm text-center p-1">Task</h3>
                        </div>
                    </li>
                @endforeach
                @foreach ($projectResults as $result)
                <li class="hover:bg-gray-200 cursor-pointer py-2 px-2 border-b">
                    <div class="flex justify-between row-span-1">
                    <a href="{{ route('projects.show', $result['id']) }}">{{ $result->name }}</a>
                    <h3 class="font-semibold bg-blue-300 rounded-xl text-sm text-center p-1">Project</h3>
                    </div>
                </li>
            @endforeach
            </ul>
        </div> --}}
</div>
