<x-wms-layout>
    @section('content')
        <div class="h-auto" x-data="{ showModal: false }">
            @include('inbound.navbar')
            @include('flash-message')
            <div class="flex justify-items-center justify-start border-b-2 border-gray-500 h-14">
                <h1 class="flex font-semibold text-2xl px-4 items-center">
                    {{ __('Receiving - ') }}{{ $deliveryDetail->delivery->delivery_ref }}</h1>
            </div>
            <h1 class="flex font-semibold text-xl px-4 items-center my-2">
                {{ __('Staging Lane: ') }}{{ $deliveryDetail->delivery->lane }}</h1>
            <div class="flex justify-between">
                <form class="w-full mx-44 mt-2 bg-white border-2 border-gray-300 shadow-xl rounded-xl" @keydown.enter.prevent
                    action="{{ route('inbound.receipt.update', $deliveryDetail->id) }}" method="POST">
                    @csrf
                    @method('PATCH')
                    <input type="text" name="delivery_detail_id" value="{{ $deliveryDetail->id }}" hidden />
                    <input type="text" name="lpn_level" value="{{ $deliveryDetail->lpnlevel }}" hidden />
                    <div class="flex justify-around bg-gray-200 rounded-t-lg">
                        <h1 class="text-lg font-semibold p-2">{{ __('Receiving Item: ') }}{{ $deliveryDetail->product }}
                        </h1>
                        <h1 class="text-lg font-semibold p-2">
                            {{ __('Expected Quantity: ') }}{{ $deliveryDetail->expected_qty }}</h1>
                        <h1 class="text-lg font-semibold p-2">{{ __('Uom: ') }}{{ $deliveryDetail->uom }}</h1>
                        <h1 class="text-lg font-semibold p-2">
                            {{ __('Received Quantity: ') }}{{ $deliveryDetail->received_qty }}</h1>
                    </div>
                    <div class="flex justify-items-start gap-4 mt-2 mr-4">
                        <div>
                            <label class="text-lg font-semibold p-2">{{ __('Quantity:') }}</label>
                            <br>
                            <input type="number" min="1" placeholder="Quantity..." name="quantity" id="quantity"
                                class="p-2 m-2 bg-gray-50 h-auto rounded-lg focus:outline-none focus:ring-0 focus:border-gray-500 w-full" />
                        </div>
                        <div class="w-1/3">
                            <label class="text-lg font-semibold p-2">{{ __('LPN Level:') }}</label>
                            <br>
                            <select placeholder="System generated if blank..." name="rcvlpnlevel"
                                class="p-2 m-2 bg-gray-50 h-auto rounded-lg focus:outline-none focus:ring-0 focus:border-gray-500 w-full">
                                <option value="EA">{{ __('Each') }}</option>
                                <option value="CS">{{ __('Case') }}</option>
                                <option value="PA">{{ __('Pallet') }}</option>
                            </select>
                        </div>
                    </div>
                    <div class="flex justify-items-start gap-4 mt-2 mr-4">
                        <div>
                            <label class="text-lg font-semibold p-2">{{ __('Batch:') }}</label>
                            <br>
                            <input type="text" placeholder="Batch..." name="batch"
                                class="p-2 m-2 bg-gray-50 h-auto rounded-lg focus:outline-none focus:ring-0 focus:border-gray-500 w-full" value="{{ $deliveryDetail->batch }}" />
                        </div>
                        <div>
                            <label class="text-lg font-semibold p-2">{{ __('Customer Batch:') }}</label>
                            <br>
                            <input type="text" placeholder="Customer Batch..." name="customer_batch"
                                class="p-2 m-2 bg-gray-50 h-auto rounded-lg focus:outline-none focus:ring-0 focus:border-gray-500 w-full" value="{{ $deliveryDetail->cust_batch }}"  />
                        </div>
                        <div>
                            <label class="text-lg font-semibold p-2">{{ __('Staging Lane:') }}</label>
                            <br>
                            <select
                                class="p-2 m-2 rounded-lg focus:outline-none focus:ring-0 focus:border-gray-500 border-gray-400 bg-gray-50 w-full"
                                name="staging_lane" required>
                                <option value="{{ $deliveryDetail->delivery->lane }}">
                                    {{ $deliveryDetail->delivery->lane }}</option>
                                @foreach ($stagingLanes as $lane)
                                    @if ($lane->name != $deliveryDetail->delivery->lane)
                                        <option value="{{ $lane->name }}">{{ $lane->name }}</option>
                                    @endif
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <label class="text-lg font-semibold p-2">{{ __('LPNs:') }}</label>
                    <br>
                    <div id="rcvlpns">
                        <input type="text" placeholder="System generated if blank..." name="rcvlpns[]"
                            class="p-2 m-2 bg-gray-50 h-auto rounded-lg focus:outline-none focus:ring-0 focus:border-gray-500 w-3/4" />
                    </div>
                    <button type="submit"
                        class="bg-blue-500 hover:bg-blue-700 focus:outline-none rounded m-2 px-2 py-2 text-center font-semibold text-white inline">{{ __('Save') }}</button>
                    <a href="{{ route('inbound.receipt.show', $deliveryDetail->delivery->id) }}">
                        <button type="button"
                            class="bg-red-500 hover:bg-red-700 focus:outline-none rounded px-2 py-2 text-center font-semibold text-white inline">{{ __('Cancel') }}</button>
                    </a>
                </form>
            </div>
        </div>
        <script>
            $(document).ready(function() {

                var quantity;
                var lpn_level = 'EA';

                $("select[name='rcvlpnlevel']").change(function() {

                        lpn_level = $(this).val();

                        showLpnInput(lpn_level, quantity);

                    }),
                    $("#quantity").change(function() {

                        quantity = $(this).val();

                        showLpnInput(lpn_level, quantity);

                    });

            });

            function showLpnInput(lpn_level, quantity) {

                if (lpn_level != 'EA') {

                    $("#rcvlpns").empty();

                    for (var i = 0; i < quantity; i++) {
                        $("#rcvlpns").append($(
                            '<input type="text" placeholder="System generated if blank..." name="rcvlpns[]"' +
                            'class="p-2 m-2 bg-gray-50 h-auto rounded-lg focus:outline-none focus:ring-0 focus:border-gray-500 w-3/4" />'
                        ))
                    }

                } else {

                    $("#rcvlpns").empty();

                    $("#rcvlpns").append($(
                        '<input type="text" placeholder="System generated if blank..." name="rcvlpns[]"' +
                        'class="p-2 m-2 bg-gray-50 h-auto rounded-lg focus:outline-none focus:ring-0 focus:border-gray-500 w-3/4" />'
                    ))
                };
            };
        </script>
    @endsection
</x-wms-layout>
