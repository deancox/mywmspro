<x-wms-layout>
    @section('content')
        <div class="flex-1">
            @include('inbound.navbar')
            <div class="flex justify-between justify-items-center">
                <h1 class="flex font-semibold text-2xl px-4 items-center py-2">{{ __('Deliveries') }}</h1>
            </div>
            {{-- @include('flash-message') --}}
            <div class="p-4 shadow-lg border-2 border-gray-300 mx-12 mt-2 bg-white rounded-lg">
                <table class="w-full bg-white text-left">
                    <thead class="w-full border-b-2 border-black">
                        <tr class="w-full border-b border-black bg-gray-200">
                            <th class="py-2 pl-2">{{ __('Delivery Number') }}</th>
                            <th class="py-2 pl-2">{{ __('Supplier') }}</th>
                            <th class="py-2 pl-2">{{ __('Due') }}</th>
                            <th class="py-2 pl-2">{{ __('Checked In') }}</th>
                            <th class="py-2 pl-2">{{ __('Status') }}</th>
                            <th class="py-2 pl-2">{{ __('Short') }}</th>
                            <th class="py-2 pl-2">{{ __('Lines') }}</th>
                        </tr>
                    </thead>
                    <tbody class="bg-grey-light">
                        @forelse ($deliveries as $delivery)
                            <tr class="border-b-2 hover:bg-gray-100 text-left text-sm p-2">
                                <td class="pl-2 py-2">
                                    <a href="{{ route('inbound.deliveries.show', $delivery) }}"
                                        class="hover:underline text-blue-600 hover:text-blue-800">{{ $delivery->delivery_ref }}</a>
                                </td>
                                <td class="pl-2">
                                    {{ $delivery->supplier->name }}
                                </td>
                                <td class="pl-2">
                                    @if ($delivery->checked_in_flg == true)
                                    {{ Carbon\Carbon::parse($delivery->due_date)->format('d-m-Y \@H:i') }}
                                    @else
                                        @if (!Carbon\Carbon::parse($delivery->due_date)->isPast())
                                            <h1 class="text-green-500 font-semibold inline">
                                                {{ Carbon\Carbon::parse($delivery->due_date)->format('d-m-Y \@H:i') }}
                                            </h1> -
                                            ({{ Carbon\Carbon::parse($delivery->due_date)->diffForHumans() }})
                                        @else
                                            <h1 class="text-red-500 font-semibold">{{ __('Overdue') }} -
                                                ({{ 'Due ' . Carbon\Carbon::parse($delivery->due_date)->diffForHumans() }})
                                            </h1>
                                        @endif
                                    @endif
                                </td>
                                <td class="pl-2">
                                    @if ($delivery->checked_in_flg == true)
                                        <h1 class="text-green-500 font-semibold inline">
                                            {{ Carbon\Carbon::parse($delivery->check_in_date)->format('d-m-Y \@H:i') }}
                                        </h1> -
                                        ({{ Carbon\Carbon::parse($delivery->check_in_date)->diffForHumans() }})
                                    @endif
                                </td>
                                <td class="pl-2">
                                    {{ $delivery->status }}
                                </td>
                                <td class="pl-2">
                                    {{ $delivery->short_flg }}
                                </td>
                                <td class="pl-2">
                                    {{ $delivery->delivery_details_count }}
                                </td>
                            @empty
                                <td>{{ __('No Deliveries Found.') }}</td>
                            </tr>
                        @endforelse
                    </tbody>
                    <a href="{{ route('inbound.deliveries.create') }}">
                        <button
                            class="bg-blue-500 hover:bg-blue-700 focus:outline-none rounded mr-6 px-4 mb-2 text-center h-8 font-bold text-white">{{ __('Add New') }}</button>
                    </a>
                </table>
                {{ $deliveries->links() }}
            </div>
        </div>
    @endsection
</x-wms-layout>
