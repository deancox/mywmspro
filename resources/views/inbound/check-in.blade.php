<x-wms-layout>
    @section('content')
        <div class="h-auto" x-data="{ showModal: false }">
            @include('inbound.navbar')
            @include('flash-message')
            @error('lane')
                <div class="text-md font-semibold text-red-500 bg-red-200 p-2">
                    {{ __('A Staging is lane required.') }}
                </div>
            @enderror
            @error('door')
                <div class="text-md font-semibold text-red-500 bg-red-200 p-2">
                    {{ __('A Door is required.') }}
                </div>
            @enderror
            <div class="flex justify-between justify-items-center items-center">
                <h1 class="flex font-semibold text-2xl px-4 items-center py-2">
                    {{ __('Check in ') }}{{ $delivery->delivery_ref }}</h1>
            </div>
            <form method="POST" action=" {{ route('inbound.checkInConfirm', $delivery) }}">
                @csrf
                <div class="ml-5">
                    <button type="submit"
                        class="bg-blue-500 hover:bg-blue-700 focus:outline-none rounded mr-2 py-2 px-4 mb-2 text-center font-semibold text-white">{{ __('Check in') }}</button>
                        <a href="{{ route('inbound.deliveries.show', $delivery) }}"      
                        class="bg-red-500 hover:bg-red-700 focus:outline-none rounded mr-2 py-2 px-4 mb-2 text-center font-semibold text-white">{{ __('Back') }}</a>
                    </a>
                </div>
                <div class="p-4 shadow-lg border-2 border-gray-300 mx-12 mt-2 bg-white rounded-lg">
                    <table class="w-full bg-white text-left">
                        <thead class="w-full border-b-2 border-black">
                            <tr class="w-full border-b border-black bg-gray-200">
                                <th class="py-2 pl-2">{{ __('Name') }}</th>
                                <th class="py-2 pl-2">{{ __('Type') }}</th>
                                <th class="py-2 pl-2">{{ __('Max Trucks') }}</th>
                                <th class="py-2 pl-2">{{ __('Current Trucks') }}</th>
                                <th class="py-2 pl-2">{{ __('Current Deliveries') }}</th>
                                <th class="py-2 pl-2">{{ __('Select') }}</th>
                            </tr>
                        </thead>
                        <tbody class="bg-grey-light" id="tbody">
                            @forelse ($doors as $door)
                                <tr class=" border-b hover:bg-gray-100 text-left text-sm pl-2 sortable draggable">
                                    <td class="pl-2 py-2">
                                        {{ $door->name }}
                                    </td>
                                    <td class="pl-2">
                                        {{ $door->doorType->description }}
                                    </td>
                                    <td class="pl-2">
                                        {{ $door->max_trucks }}
                                    </td>
                                    <td class="pl-2">
                                        {{ $door->cur_truck_count }}
                                    </td>
                                    <td class="pl-2">
                                        {{ $door->cur_delivery_count }}
                                    </td>
                                    <td class="pl-2">
                                        <input type="checkbox" id="selectedDoor" name="door" value="{{ $door->id }}"
                                            class="focus:ring-0 focus:border-white rounded" />
                                    </td>
                                @empty
                                    <td>{{ __('No doors Found.') }}</td>
                            @endforelse
                        </tbody>
                        <div class="flex justify-between justify-items-center">
                            <h1 class="flex font-semibold text-xl items-center pb-2">{{ __('Available Doors') }}</h1>
                        </div>
                    </table>
                </div>
            </form>
        </div>
        <script>
            //only select one checkbox
            $(document).ready(function() {
                $('input:checkbox').click(function() {
                    $('input:checkbox').not(this).prop('checked', false)
                    //to work on the toggle classes..
                    //$(this).closest('tr').toggleClass('bg-green-500');
                });
            });
        </script>
    @endsection
</x-wms-layout>
