<x-wms-layout>
    @section('content')
        <div class="h-auto" x-data="{ showModal: false }">
            @include('inbound.navbar')
            @include('flash-message')
            <div class="flex justify-items-center justify-start border-b-2 border-gray-500 h-14">
                @if ($delivery->checked_in_flg == true)
                    <h1 class="flex font-bold text-xl px-4 items-center py-2">
                        {{ __('Delivery: ') }}{{ $delivery->delivery_ref }}{{ ' - ' }}<span
                            class="text-green-500 pl-2">{{ __('Checked In') }}</span></h1>
                    @if ($delivery->lane == null)
                        <form action="{{ route('inbound.stagingLanes.show', $delivery) }}" method="GET">
                            @csrf
                            <button type="submit"
                                class="bg-blue-600 hover:bg-blue-700 focus:outline-none rounded mr-2 px-2 text-center h-8 mt-2 font-semibold text-white inline">{{ __('Start Receiving') }}</button>
                            <input hidden value="{{ $delivery }}" name="delivery">
                            <a href="{{ route('inbound.deliveries.index') }}"><button @submit.prevent
                                    class="bg-red-500 hover:bg-red-700 focus:outline-none rounded mr-2 px-4 h-8 mt-2 text-center font-semibold text-white">{{ __('Back') }}
                                </button></a>
                            </a>
                        </form>
                    @else
                        <form action="{{ route('inbound.receipt.show', $delivery) }}" method="GET">
                            @csrf
                            <button type="submit"
                                class="bg-blue-600 hover:bg-blue-700 focus:outline-none rounded mr-2 py-2 px-2 text-center mt-2 font-semibold text-white inline">{{ __('Start Receiving') }}</button>
                            <input hidden value="{{ $delivery }}" name="delivery">
                            <a href="{{ route('inbound.deliveries.index') }}"
                                class="bg-red-500 hover:bg-red-700 focus:outline-none rounded mr-2 py-2 px-4 my-2 text-center font-semibold text-white">{{ __('Back') }}</a>
                            </a>
                        </form>
                    @endif
                @else
                    <h1 class="flex font-bold text-xl px-4 items-center py-2">
                        {{ __('Delivery: ') }}{{ $delivery->delivery_ref }}</h1>
                    <form action="{{ route('inbound.checkIn', $delivery) }}" method="GET">
                        @csrf
                        <button type="submit"
                            class="bg-blue-500 hover:bg-blue-700 focus:outline-none rounded mr-2 px-2 text-center h-8 mt-2 font-semibold text-white inline">{{ __('Check in') }}</button>
                        <input hidden value="{{ $delivery }}" name="delivery">
                    </form>
                    <form action="{{ route('inbound.deliveries.destroy', $delivery) }}" method='POST'>
                        @csrf
                        @method('DELETE')
                        <button type="submit"
                            class="bg-red-500 hover:bg-red-700 focus:outline-none rounded mr-2 px-2 text-center h-8 my-2 font-semibold text-white inline">{{ __('Cancel Delivery') }}</button>
                    </form>
                @endif
            </div>
            <div class="flex items-center h-full w-full">
                <div class="flex-grow h-full p-4 overflow-y-auto rounded-lg">
                    <label for="dueDate" class="font-semibold">{{ __('Due Date:') }}</label>
                    <br>
                    @if ($delivery->checked_in_flg == true)
                        <input type="date" name="dueDate" value={{ $delivery->due_date }} disabled
                            class="rounded-lg focus:outline-none focus:ring-0 focus:bg-gray-100">
                    @else
                        <input type="date" name="dueDate" value={{ $delivery->due_date }}
                            class="rounded-lg focus:outline-none focus:ring-0 focus:bg-gray-100">
                    @endif
                    @error('dueDate') <span class="error text-red-500">{{ $message }}</span>
                    @enderror
                    <h2 class="text-md py-2 pl-12 inline"><span
                            class="font-bold">{{ __('Supplier Name: ') }}</span>{{ $delivery->supplier->name }}
                    </h2>
                    <h2 class="text-md py-2 pl-12 inline"><span class="font-bold">
                            {{ __('Lines: ') }}</span>{{ count($delivery->deliveryDetails) }}</h2>
                    @if ($delivery->checked_in_flg == true)
                        <h2 class="text-md py-2 pl-12 inline"><span
                                class="font-bold">{{ __('Door: ') }}</span>{{ $delivery->door }}
                        </h2>
                    @endif
                    <div class="my-2">
                        @if ($delivery->checked_in_flg == false)
                            <x-buttons.primary class="mt-2 h-8">{{ __('Add Line') }}</x-buttons.primary>
                        @endif
                    </div>
                    <table class="w-full bg-white ">
                        <thead class="bg-gray-200 text-left">
                            <th class="py-2 pl-2">{{ __('Item') }}</th>
                            <th class="py-2 pl-2">{{ __('Batch') }}</th>
                            <th class="py-2 pl-2">{{ __('Expected Qty') }}</th>
                            <th class="py-2 pl-2">{{ __('Received Qty') }}</th>
                            <th class="py-2 pl-2">{{ __('Short Qty') }}</th>
                            <th class="py-2 pl-2">{{ __('Uom') }}</th>
                        </thead>
                        <tbody class="bg-grey-light">
                            @forelse ($delivery->deliveryDetails as $deliveryDetail)
                                <tr class="hover:bg-gray-100 border-b-2">
                                    <td class="pl-2">{{ $deliveryDetail->product }}</td>
                                    <td class="pl-2">{{ $deliveryDetail->batch ?: 'n\\a' }}</td>
                                    <td class="pl-2">{{ $deliveryDetail->expected_qty }}</td>
                                    <td class="pl-2">{{ $deliveryDetail->received_qty }}</td>
                                    <td class="pl-2">{{ $deliveryDetail->short_qty }}</td>
                                    <td class="pl-2">{{ $deliveryDetail->uom }}</td>
                                @empty
                                    <td class="pl-2 text-red-500 italic">
                                        {{ __('This delivery has no pre-advised lines.') }}</td>
                                </tr>
                        </tbody>
                        @endforelse
                    </table>
                    @if ($delivery->checked_in_flg == false)
                        <!-- add delivery line Modal-->
                        <form method="POST" action="{{ route('inbound.deliveryDetails.store') }}">
                            @csrf
                            <x-modal>
                                <x-slot name="title" class="mb-2">
                                    {{ __('Add Line To Delivery: ') }}{{ $delivery->delivery_ref }}
                                </x-slot>
                                <input type="text" name="delivery_id" value="{{ $delivery->id }}" hidden />
                                <x-modal.input.dropdown label="{{ __('Product:') }}" name="product_id" required>
                                    <option value=""></option>
                                    @forelse ($products as $product)
                                        <option value="{{ $product->id }}">{{ $product->product }} -
                                            {{ $product->description }}
                                        </option>
                                    @empty
                                        <option value="">{{ __('No Products Found.') }}</option>
                                    @endforelse
                                </x-modal.input.dropdown>
                                <br />
                                <x-modal.input.dropdown label="{{ __('Uom:') }}" name="uom_id" class="bg-gray-200"
                                    disabled required>
                                    <option value=""></option>
                                </x-modal.input.dropdown>
                                <br />
                                <label class="mx-4 font-semibold">{{ __('Quantity:') }}</label>
                                <input
                                    class="mx-4 rounded-lg focus:outline-none focus:ring-0 focus:bg-gray-100 bg-gray-200 border-gray-400 "
                                    disabled type="number" min="1" name="qty" id="qty" placeholder="1" required />
                                <br>
                                <x-modal.input.textarea name="batch" placeholder='Batch...' />
                                <x-modal.input.textarea name="cust_batch" placeholder='Supplier Batch...' />
                            </x-modal>
                        </form>
                    @endif
                </div>
            </div>
        </div>
        <script>
            $(document).ready(function() {
                $("select[name='product_id']").change(function() {

                        $("select[name='uom_id']").empty();

                        $('select[name="uom_id"]').append('<option value=""' + '</option>');

                        var productID = $(this).val();

                        if (productID) {

                            $.ajax({
                                type: "GET",
                                url: '{{ url('/') }}/inventory/productDetails/' + productID,

                                success: function(data) {
                                    console.log(data);
                                    $('select[name="uom_id"]').prop("disabled", false);
                                    $('select[name="uom_id"]').removeClass('bg-gray-200');

                                    $.each(data, function(key, value) {
                                        $('select[name="uom_id"]').append('<option value="' +
                                            value.uom_id + '">' + value.uom + ' - ' + value
                                            .description + '</option>');
                                    });
                                }
                            });
                        } else {
                            $('select[name="uom_id"]').prop("disabled", true);
                            $('select[name="uom_id"]').addClass('bg-gray-200');
                        };
                    }),
                    $("select[name='uom_id']").change(function() {

                        var uomID = $(this).val()

                        if (uomID) {

                            $("#qty").prop("disabled", false);
                            $("#qty").toggleClass('bg-gray-200 bg-gray-100');

                        } else {

                            $("#qty").prop("disabled", true);
                            $("#qty").toggleClass('bg-gray-200 bg-gray-100');
                        };

                    });
            });
        </script>
    @endsection
</x-wms-layout>
