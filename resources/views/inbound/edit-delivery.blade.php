<x-wms-layout>
    @section('content')
        <div class="flex-1">
            @include('inbound.navbar')
            @include('flash-message')
            @livewire('create-delivery')
        </div>
        @endsection
</x-wms-layout>