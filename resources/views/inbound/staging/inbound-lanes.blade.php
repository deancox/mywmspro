<x-wms-layout>
    @section('content')
        <div class="h-auto" x-data="{ showModal: false }">
            @include('inbound.navbar')
            <div class="flex justify-between justify-items-center items-center">
                <h1 class="flex font-semibold text-2xl px-4 items-center py-2">
                    {{ __('Assign Staging Lane for - ') }}{{ $delivery->delivery_ref }}</h1>
            </div>
            <form method="POST" action="{{ route('inbound.stagingLanes.store') }}">
                @csrf
                <input type="text" name="delivery_id" value="{{ $delivery->id }}" hidden>
                <div class="ml-5">
                    <button type="submit"
                        class="bg-blue-500 hover:bg-blue-700 focus:outline-none rounded mr-2 py-2 px-4 mb-2 text-center font-semibold text-white">{{ __('Use Selected') }}</button>
                        <a href="{{ route('inbound.deliveries.show', $delivery) }}"      
                        class="bg-red-500 hover:bg-red-700 focus:outline-none rounded mr-2 py-2 px-4 mb-2 text-center font-semibold text-white">{{ __('Back') }}</a>
                    </a>
                </div>
                <div class="p-4 shadow-lg border-2 border-gray-300 mx-12 mt-2 bg-white rounded-lg">
                    <table class="w-full bg-white text-left">
                        <thead class="w-full border-b-2 border-black">
                            <tr class="w-full border-b border-black bg-gray-200">
                                <th class="py-2 pl-2">{{ __('Name') }}</th>
                                <th class="py-2 pl-2">{{ __('Type') }}</th>
                                <th class="py-2 pl-2">{{ __('Max Trucks') }}</th>
                                <th class="py-2 pl-2">{{ __('Current Truck Count') }}</th>
                                <th class="py-2 pl-2">{{ __('Select') }}</th>
                            </tr>
                        </thead>
                        <tbody class="bg-grey-light" id="tbody">
                            @forelse ($lanes as $lane)
                                <tr class=" border-b hover:bg-gray-100 text-left text-sm pl-2 sortable draggable">
                                    <td class="pl-2 py-2">
                                        {{ $lane->name }}
                                    </td>
                                    <td class="pl-2">
                                        {{ $lane->area }}
                                    </td>
                                    <td class="pl-2">
                                        {{ $lane->max_deliveries }}
                                    </td>
                                    <td class="pl-2">
                                        {{ $lane->cur_delivery_count }}
                                    </td>
                                    <td class="pl-2">
                                        <input type="checkbox" id="selectedLane" name="lane_id" value="{{ $lane->id }}"
                                            class="focus:ring-0 focus:border-white rounded" />
                                    </td>
                                @empty
                                    <td>{{ __('No Valid Staging Lanes Found.') }}</td>
                            @endforelse
                        </tbody>
                        <div class="flex justify-between justify-items-center">
                            <h1 class="flex font-semibold text-xl items-center pb-2">{{ __('Available Staging Lanes') }}
                            </h1>
                        </div>
                    </table>
                </div>
            </form>
        </div>
        <script>
            //only select one checkbox
            $(document).ready(function() {
                $('input:checkbox').click(function() {
                    $('input:checkbox').not(this).prop('checked', false)
                    //to work on the toggle classes..
                    //$(this).closest('tr').toggleClass('bg-green-500');
                });
            });
        </script>
    @endsection
</x-wms-layout>
