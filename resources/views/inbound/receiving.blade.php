<x-wms-layout>
    @section('content')
        <div class="h-auto" x-data="{ showModal: false }">
            @include('inbound.navbar')
            @include('flash-message')
            <div class="flex justify-items-center justify-start border-b-2 border-gray-500 h-14">
                <h1 class="flex font-semibold text-2xl px-4 items-center">
                    {{ __('Receiving - ') }}{{ $delivery->delivery_ref }}</h1>
                <button type="submit"
                    class="bg-green-500 hover:bg-green-700 focus:outline-none rounded mr-2 px-4 mt-2 h-8 text-center font-semibold text-white inline">{{ __('Close & Dispatch') }}</button>
                <a href="{{ route('inbound.deliveries.index') }}">
                    <button type="button"
                    class="bg-red-500 hover:bg-red-700 focus:outline-none rounded px-4 mt-2 h-8 text-center font-semibold text-white inline">{{ __('Back') }}</button>
                </a>
            </div>
            <h1 class="flex font-semibold text-xl px-4 items-center my-2">
                {{ __('Lines - Staging Lane: ') }}{{ $delivery->lane }}</h1>
            <div class="flex w-full">
                <table class="w-full mx-6 mt-2 bg-white ">
                    <thead class="bg-gray-200 text-left">
                        <th class="py-2 pl-2">{{ __('Item') }}</th>
                        <th class="py-2 pl-2">{{ __('Batch') }}</th>
                        <th class="py-2 pl-2">{{ __('Expected Qty') }}</th>
                        <th class="py-2 pl-2">{{ __('LPN Level') }}</th>
                        <th class="py-2 pl-2">{{ __('Received Qty') }}</th>
                        <th class="py-2 pl-2">{{ __('Short Qty') }}</th>
                        <th class="py-2 pl-2">{{ __('Uom') }}</th>
                        <th class="py-2 pl-2"></th>
                    </thead>
                    <tbody class="bg-grey-light">
                        @foreach ($delivery->deliveryDetails as $deliveryDetail)
                            <tr class="hover:bg-gray-100 border-b-2">
                                <td class="pl-2">{{ $deliveryDetail->product }}</td>
                                <td class="pl-2">{{ $deliveryDetail->batch ?: 'n\\a' }}</td>
                                <td class="pl-2">{{ $deliveryDetail->expected_qty }}</td>
                                <td class="pl-2">{{ $deliveryDetail->lpnlevel }}</td>
                                <td class="pl-2">{{ $deliveryDetail->received_qty }}</td>
                                <td class="pl-2">{{ $deliveryDetail->short_qty }}</td>
                                <td class="pl-2">{{ $deliveryDetail->uom }}</td>
                                <td class="pl-2">
                                    <form method="GET" action="{{ route('inbound.receipt.edit', $delivery) }}">
                                        @csrf
                                        <button type="submit" name="deliveryDetail" value="{{ $deliveryDetail->id }}"
                                            class="bg-blue-500 hover:bg-blue-700 focus:outline-none rounded m-2 px-2 py-2 text-center font-semibold text-white inline">{{ __('Receive') }}</button>
                                        </form>
                                </td>
                            </tr>
                    </tbody>
                    @endforeach
                </table>
            </div>
        @endsection
</x-wms-layout>
