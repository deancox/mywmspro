<div class="flex sticky top-16 h-14 justify-items-start gap-4 text-gray-800 pl-2 font-semibold bg-white border-b-2 border-gray-500">
    <div class="flex items-center hover:text-blue-400"><a href="{{ route('doors.index') }}">{{ __('Doors') }}</a></div>
    <div class="flex items-center hover:text-blue-400"><a href="{{ route('stagingLanes.index') }}">{{ __('Staging Lanes') }}</a></div>
    <div class="flex items-center hover:text-blue-400"><a href="{{ route('inbound.deliveries.index') }}">{{ __('Deliveries') }}</a></div>
    <div class="flex items-center hover:text-blue-400"><a href="#">{{ __('Returns') }}</a></div>
    <div class="flex items-center hover:text-blue-400"><a href="#">{{ __('View Planner') }}</a></div>
    <div class="flex items-center hover:text-blue-400"><a href="#">{{ __('Suppliers') }}</a></div>
    <div class="flex items-center hover:text-blue-400"><a href="#">{{ __('Check-in') }}</a></div>
    <div class="flex items-center hover:text-blue-400"><a href="{{ route('work.index') }}">{{ __('Work') }}</a></div>
</div>
