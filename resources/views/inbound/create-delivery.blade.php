<x-wms-layout>
    @section('content')
        <div class="flex-1">
            @include('inbound.navbar')
            @include('flash-message')
            <div class="flex justify-between justify-items-center">
                <h1 class="flex font-semibold text-2xl px-4 items-center py-2">{{ __('Create Delivery') }}</h1>
            </div>
            <form method="POST" action="{{ route('inbound.deliveries.store') }}">
                @csrf
                <div class=" mx-20 mt-2 bg-white border-2 border-gray-300 py-4 rounded-lg shadow-2xl">
                    <div class="flex justify-between gap-4 mx-8 mt-2 rounded-lg">
                        <div class="flex-grow h-full rounded-lg">
                            <label for="reference" class=" font-semibold">{{ __('Reference:') }}</label>
                            <br>
                            <input type="text" name="reference" id="reference" placeholder="System Generated if Blank"
                                class="rounded-lg focus:outline-none focus:ring-0 focus:bg-gray-100 w-full">
                            @error('reference') <span class="error text-red-500">{{ $message }}</span> @enderror
                        </div>
                        <div class="flex-grow h-full rounded-lg">
                            <label for="supplier" class=" font-semibold">{{ __('Supplier:') }}</label>
                            <br>
                            <select name="supplier" id="supplier"
                                class="rounded-lg focus:outline-none focus:ring-0 focus:bg-gray-100 w-full">
                                <option value="{{ old('supplier') }}">{{  old('supplier') }}</option>
                                @forelse ($suppliers as $supplier)
                                    <option value="{{ $supplier->name }}">{{ $supplier->name }}</option>
                                @empty
                                    <option value="">{{ __('No Suppliers Found') }}</option>
                                @endforelse
                            </select>
                            @error('supplier') <span class="error text-red-500">{{ $message }}</span> @enderror
                        </div>
                        <div class="flex-grow h-full rounded-lg">
                            <label for="supplier" class=" font-semibold">{{ __('Delivery Type:') }}</label>
                            <br>
                            <select name="delivery_type" id="delivery_type"
                                class="rounded-lg focus:outline-none focus:ring-0 focus:bg-gray-100 w-full">
                                <option value="{{ old('delivery_type') }}">{{  old('delivery_type') }}</option>
                                @forelse ($inboundtypes as $type)
                                    <option value="{{ $type->id }}">{{ $type->name }}</option>
                                @empty
                                    <option value="">{{ __('No Inbound types Found.') }}</option>
                                @endforelse
                            </select>
                        </div>
                    </div>
                    <div class="flex justify-between gap-4 mx-8 mt-4 rounded-lg">
                        <div class="flex-grow h-full rounded-lg">
                            <label for="dueDate" class="font-semibold">{{ __('Due Date:') }}</label>
                            <br>
                            <input type="date" name="dueDate" id="dueDate" value="{{ old('dueDate',date('Y-m-d')) }}"
                                class="rounded-lg focus:outline-none focus:ring-0 focus:bg-gray-100 w-full">
                            @error('dueDate') <span class="error text-red-500">{{ $message }}</span> @enderror
                        </div>
                        <div class="flex-grow h-full rounded-lg">
                            <label for="dueDate" class="font-semibold">{{ __('Due Time (HHMM):') }}</label>
                            <br>
                            <input type="time" step="300" name="time" id="time"
                                class="rounded-lg focus:outline-none focus:ring-0 focus:bg-gray-100 w-full">
                            @error('time') <span class="error text-red-500">{{ $message }}</span> @enderror
                        </div>
                        <div class="flex-grow h-full rounded-lg">
                            <label for="short_receipt_allowed" class="font-semibold">{{ __('Shorts Allowed?') }}</label>
                            <br>
                            <label data-toggle="checkbox-toggle" data-rounded="rounded-xl" data-handle-size="12"
                                data-handle-color="bg-white" data-off-color="bg-gray-400" data-on-color="bg-blue-400">
                                <input type="checkbox" name="short_receipt_allowed" id="short_receipt_allowed"
                                    class=" focus:outline-none focus:ring-0 focus:bg-gray-200 rounded">
                                @error('short_receipt_allowed') <span class="error text-red-500">{{ $message }}</span>
                                @enderror
                        </div>
                    </div>
                    <div class="flex justify-between gap-4 mx-8 mt-4 rounded-lg">
                        <x-buttons.primary>{{ __('Create Delivery & Start Adding Lines') }}</x-buttons.primary>
                        <a href="{{ route('inbound.deliveries.index') }}"
                        class="bg-red-500 hover:bg-red-700 focus:outline-none rounded mr-2 px-4 pt-2 text-center font-semibold text-white">{{ __('Cancel') }}</a>
                    </a>
                    </div>
                </div>
            </form>
        </div>
    @endsection
</x-wms-layout>
