<x-wms-layout>
    @section('content')
    <div class="h-auto">
            @include('inbound.navbar')
            <div class="flex justify-between justify-items-center h-14 shadow-lg">
                <h1 class="flex font-semibold text-2xl px-4 items-center py-2">{{__('Inbound Dashboard') }}</h1>
            </div>
            <div>Inbound Dashboard Things..</div>
        </div>
    @endsection
</x-wms-layout>
