<x-wms-layout>
    @section('content')
        <div class="h-auto" x-data="{ showModal: false }">
            @include('inbound.navbar')
            @include('flash-message')
            <div class="flex justify-between justify-items-center">
                <h1 class="flex font-semibold text-2xl px-4 items-center py-2">{{ __('Staging Lanes') }}</h1>
            </div>
            <div class="p-4 shadow-lg border-2 border-gray-300 mx-16 mt-2 bg-white rounded-lg">
                <table class="w-full bg-white text-left">
                    <thead class="w-full border-b-2 border-black">
                        <tr class="w-full border-b border-black bg-gray-200">
                            <th class="py-2 pl-2">{{ __('Name') }}</th>
                            <th class="py-2 pl-2">{{ __('In Service') }}</th>
                            <th class="py-2 pl-2">{{ __('Used') }}</th>
                            <th class="py-2 pl-2">{{ __('Avilable') }}</th>
                            <th class="py-2 pl-2">{{ __('Max Deliveries') }}</th>
                            <th class="py-2 pl-2">{{ __('Used Capacity%') }}</th>
                            <th class="py-2 pl-2">{{ __('Current Deliveries') }}</th>
                        </tr>
                    </thead>
                    <tbody class="bg-grey-light" id="tbody">
                        @forelse ($lanes as $lane)
                            <tr class=" border-b hover:bg-gray-100 text-left text-sm pl-2 sortable draggable">
                                <td class="pl-2 py-2">
                                    <a href="#"
                                        class="hover:underline text-blue-600 hover:text-blue-800">{{ $lane->name }}</a>
                                </td>
                                <td class="pl-2">
                                    @if ($lane->in_service === 1)
                                        <i class="fas fa-check text-green-500 fa-lg onOff"
                                            id="toggle_inService{{ $lane->id }}"></i>
                                    @else
                                        <i class="fas fa-times text-red-500 fa-lg onOff"
                                            id="toggle_InService{{ $lane->id }}"></i>
                                    @endif
                                </td>
                                <td class="pl-2">{{ $lane->current_qty }}</td>
                                <td class="pl-2">{{ $lane->maxqty - $lane->current_qty }}</td>
                                <td class="pl-2">{{ $lane->max_deliveries }}</td>
                                <td class="pl-2 inline">
                                    @if ($lane->used_percent === 0)
                                        <div class="text-xs text-center p-1 leading-none rounded-xl"
                                            style="width: {{ $lane->used_percent }}%"></div>
                                        {{ $lane->used_percent }}%
                                    @elseif($lane->used_percent < 51) <div
                                            class="bg-green-400 text-xs text-center p-1 leading-none rounded-xl w-full"
                                            style="width: {{ $lane->used_percent }}%">
            </div>
            {{ $lane->used_percent }}%
        @elseif($lane->used_percent < 80) <div class=" bg-yellow-500 text-xs text-center p-1 leading-none rounded-xl"
                style="width: {{ $lane->used_percent }}%">
        </div>
        {{ $lane->used_percent }}%
    @elseif($lane->used_percent >= 80) <div class=" bg-red-500 text-xs text-center p-1 leading-none rounded-xl"
            style="width: {{ $lane->used_percent }}%">
        </div>
        {{ $lane->used_percent }}%
        @endif
        </td>
        <td class="pl-2">{{ $lane->cur_delivery_count }}
        </td>
    @empty
        <td>{{ __('No Staging Lanes Found.') }}</td>
        @endforelse
        </tbody>
        <x-buttons.primary>{{ __('Add New') }}</x-buttons.primary>
        </table>

        {{ $lanes->links() }}
        <!--new staging lane modal-->
        <form method="POST">
            @csrf
            <x-modal>
                <x-slot name="title">{{ __('Add Staging Lane') }}</x-slot>
                <div class="form-group px-4">
                    <textarea name="name"
                        class="bg-gray-100 rounded border border-gray-400 leading-normal resize-none w-full h-12 px-3 font-medium placeholder-gray-700 focus:outline-none focus:bg-white"
                        placeholder='Name...' required></textarea>
                </div>
                <div class="form-group px-4">
                    <label for="max_deliveries" class="font-semibold">{{ __('Max Deliveries:') }}</label>
                    <input name="max_deliveries" type="number" min="1"
                        class="bg-gray-100 rounded border border-gray-400 leading-normal resize-none w-full h-12 px-3 font-medium placeholder-gray-700 focus:outline-none focus:bg-white"
                        placeholder='Required..' required />
                </div>
                <div class="form-group px-4 py-2">
                    <label for="in_service" class="font-semibold pr-2">{{ __('In Service:') }}</label>
                    <label data-toggle="checkbox-toggle" data-rounded="rounded-xl" data-handle-size="12"
                        data-handle-color="bg-white" data-off-color="bg-gray-400" data-on-color="bg-blue-500">
                        <input type="checkbox" name="in_service" class="in_service" />
                </div>
                <div class="form-group px-4 py-2">
                    <label for="tracked" class="font-semibold pr-2">{{ __('Capacity Tracked:') }}</label>
                    <label data-toggle="checkbox-toggle" data-rounded="rounded-xl" data-handle-size="12"
                        data-handle-color="bg-white" data-off-color="bg-gray-400" data-on-color="bg-blue-500">
                        <input type="checkbox" name="tracked" id="tracked" />
                </div>
                <div id="tracked_by" hidden>
                    <x-modal.input.dropdown label="{{ __('Tracked By:') }}" name="tracked_by">
                        <option value="VOL">{{ __('Volume (cubic cm)') }}</option>
                        <option value="PA">{{ __('Pallets') }}</option>
                    </x-modal.input.dropdown>
                </div>
                <div class="form-group px-4 mt-2" id="maxqty" hidden>
                    <label for="maxqty" class="font-semibold">{{ __('Max Quantity:') }}</label>
                    <input name="maxqty" type="number" min="1"
                        class="bg-gray-100 rounded border border-gray-400 leading-normal resize-none w-full h-12 px-3 font-medium placeholder-gray-700 focus:outline-none focus:bg-white"
                        placeholder='Required..' />
                </div>
            </x-modal>
        </form>
        </div>
        </div>
        <script>
            $(function() {
                var fixHelper = function(e, ui) {
                    ui.children().each(function() {
                        $(this).width($(this).width());
                    });
                    return ui;
                };

                $("#tbody").sortable({
                    helper: fixHelper
                }).disableSelection();

            });
            $(document).ready(function() {
                $('td').each(function() {
                    $(this).css('width', $(this).width() + 'px');
                });
                $('th').each(function() {
                    $(this).css('width', $(this).width() + 'px');
                });
                $("#tracked").click(function() {
                    if ($(this).is(":checked")) {
                        $('#tracked_by').attr("hidden", false);
                        $('#maxqty').attr("hidden", false);
                    } else {
                        $('#tracked_by').attr("hidden", true);
                        $('#maxqty').attr("hidden", true);
                    }
                });
            });
        </script>
    @endsection
</x-wms-layout>
