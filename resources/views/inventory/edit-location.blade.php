<x-wms-layout>
    @section('content')
        <div class="flex-1 h-full">
            @include('inventory.navbar')
            <div class="flex justify-between justify-items-center border-t-2 border-gray-500">
                <h1 class="flex font-semibold text-xl px-4 items-center py-2">{{ __('Edit Location ') }}</h1>
            </div>
            <div class="flex-1 px-20 py-12 h-full">
                <form class="bg-gray-200 form-group shadow-xl rounded-lg border-2 border-gray-300"
                    action="{{ route('location.update', $location) }}" method="POST">
                    @method('PATCH')
                    @csrf
                    <h1 class="font-bold text-xl text-left pl-4 py-2 bg-gray-300 ">{{ __('Editing Location ') }}
                        {{ $location->location_name }}
                    </h1>
                    <div class="flex justify-around items-center">
                        <div class="p-4 text-gray-700">
                            <h1 class="text-lg text-left">{{ __('Count Zone') }}</h1>
                            <select name="count_zone" class="rounded-lg focus:outline-none focus:ring-0 focus:bg-gray-100 w-full">
                                <option value="{{ $location->count_zone }}">{{ strtoupper($location->count_zone) }}
                                </option>
                                @forelse ($countZones as $countZone)
                                    <option value="{{ $countZone->name }}">{{ $countZone->name }}</option>
                                @empty
                                    <option value="">{{ __('No Count Zones Found') }}</option>
                                @endforelse
                            </select>
                            <h1 class="text-lg text-left pt-2">{{ __('Pick Zone') }}</h1>
                            <select name="pick_zone" class="rounded-lg focus:outline-none focus:ring-0 focus:bg-gray-100 w-full">
                                <option value="{{ $location->pick_zone }}">{{ strtoupper($location->pick_zone) }}
                                </option>
                                @forelse ($pickZones as $pickZone)
                                    <option value="{{ $pickZone->name }}">{{ $pickZone->name }}</option>
                                @empty
                                    <option value="">{{ __('No Pick Zones Found') }}</option>
                                @endforelse
                            </select>
                        </div>
                        <div class="p-4 text-gray-700">
                            <h1 class="text-lg text-left">{{ __('Storage Zone') }}</h1>
                            <select name="storage_zone" class="rounded-lg focus:outline-none focus:ring-0 focus:bg-gray-100 w-full">
                                <option value="{{ $location->storage_zone }}">
                                    {{ strtoupper($location->storage_zone) }}</option>
                                @forelse ($storageZones as $storageZone)
                                    <option value="{{ $storageZone->name }}">{{ $storageZone->name }}</option>
                                @empty
                                    <option value="">{{ __('No Storage Zones Found') }}</option>
                                @endforelse
                            </select>
                            <h1 class="text-lg text-left pt-2">{{ __('Location Type') }}</h1>
                            <select name="location_type"
                                class="rounded-lg focus:outline-none focus:ring-0 focus:bg-gray-100 w-full">
                                <option value="{{ $location->location_type }} ">
                                    {{ strtoupper($location->locationType->description) }}</option>
                                @forelse ($locationTypes as $locationType)
                                    <option value="{{ $locationType->name }}">
                                        {{ strtoupper($locationType->description) }}</option>
                                @empty
                                    <option value="">{{ __('No Location Types Found') }}</option>
                                @endforelse
                            </select>
                        </div>
                        <div class="p-4 text-gray-700">
                            <h1 class="text-lg text-left">{{ __('Height') }}</h1>
                            <input type="text"
                                class="inline focus:outline-none focus:ring-0 border focus:border-gray-100 rounded-lg"
                                placeholder="{{ $location->height }}" name="height" value="{{ $location->height }}">
                            <h1 class="text-lg text-left pt-2">{{ __('Width') }}</h1>
                            <input type="text"
                                class="inline focus:outline-none focus:ring-0 border focus:border-gray-100 rounded-lg"
                                placeholder="{{ $location->width }}" name="width" value="{{ $location->width }}">
                        </div>
                        <div class="p-4 text-gray-700">
                            <h1 class="text-lg text-left">{{ __('Length') }}</h1>
                            <input type="text"
                                class="inline focus:outline-none focus:ring-0 border focus:border-gray-100 rounded-lg"
                                placeholder="{{ $location->length }}" name="length" value="{{ $location->length }}">
                            <h1 class="text-lg text-left pt-2">{{ __('Max Weight') }}</h1>
                            <input type="text"
                                class="inline focus:outline-none focus:ring-0 border focus:border-gray-100 rounded-lg"
                                placeholder="{{ $location->maxweight }}" name="maxweight" value="{{ $location->maxweight }}">
                        </div>
                    </div>
                    <div class="flex justify-between px-4 py-2">
                        <div class="justify-items-center space-x-2">
                            <button class="bg-green-400 hover:bg-green-300 rounded px-2 py-2 text-lg"
                                type="submit">{{ __('Save') }}</button>
                            <a href="{{ url()->previous() }}" class="bg-red-400 hover:bg-red-300 rounded p-2 text-lg">
                                {{ __('Cancel') }}
                            </a>
                        </div>
                        <div class="pb-2 pr-10 justify-between space-x-2 text-gray-700">
                            <h1 class="inline text-lg text-left">{{ __('Usable') }}</h1>
                            <label data-toggle="checkbox-toggle" data-rounded="rounded-xl" data-handle-size="15"
                                data-handle-color="bg-white" data-off-color="bg-gray-400" data-on-color="bg-blue-500">
                                <input type="checkbox" name="is_usable" {{ $location->is_usable ? 'checked' : '' }} />
                            </label>
                            <h1 class="inline text-lg text-left pt-2">{{ __('Storable') }}</h1>
                            <label data-toggle="checkbox-toggle" data-rounded="rounded-xl" data-handle-size="15"
                                data-handle-color="bg-white" data-off-color="bg-gray-400" data-on-color="bg-blue-500">
                                <input type="checkbox" name="is_storable" {{ $location->is_storable ? 'checked' : '' }} />
                            </label>
                            <h1 class="inline text-lg text-left pt-2">{{ __('Pickable') }}</h1>
                            <label data-toggle="checkbox-toggle" data-rounded="rounded-xl" data-handle-size="15"
                                data-handle-color="bg-white" data-off-color="bg-gray-400" data-on-color="bg-blue-500">
                                <input type="checkbox" name="is_pickable" {{ $location->is_pickable ? 'checked' : '' }} />
                            </label>
                        </div>
                    </div>
                </form>
            </div>     
    @endsection
</x-wms-layout>
