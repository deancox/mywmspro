<x-wms-layout>
    @section('content')
        <div class="h-auto" x-data="{ showModal: false }">
            @include('inventory.navbar')
            @include('flash-message')
            <x-page.title>{{ __('Locations') }}</x-page.title>
            <x-table>
                <x-buttons.primary>{{ __('Add New') }}</x-buttons.primary>
                <x-slot name="headings">
                    <x-table.heading>{{ __('Location') }}</x-table.heading>
                    <x-table.heading>{{ __('Pickable') }}</x-table.heading>
                    <x-table.heading>{{ __('Storable') }}</x-table.heading>
                    <x-table.heading>{{ __('Usable') }}</x-table.heading>
                    <x-table.heading>{{ __('Storage Zone') }}</x-table.heading>
                    <x-table.heading>{{ __('Count Zone') }}</x-table.heading>
                    <x-table.heading>{{ __('Pick Zone') }}</x-table.heading>
                </x-slot>
                @forelse ($locations as $location)
                    <x-table.row>
                        <x-table.cell>
                            <x-page.link href="{{ route('location.show', $location) }}">{{ $location->location_name }}
                            </x-page.link>
                        </x-table.cell>
                        <x-table.cell>
                            @if ($location->is_pickable === 1)
                                <svg xmlns="http://www.w3.org/2000/svg" class="h-4 w-4 text-green-400" fill="none"
                                    viewBox="0 0 24 24" stroke="currentColor">
                                    <path stroke-linecap="round" stroke-linejoin="round" stroke-width="4"
                                        d="M5 13l4 4L19 7" />
                                </svg>
                            @else
                                <svg xmlns="http://www.w3.org/2000/svg" class="h-4 w-4 text-red-400" fill="none"
                                    viewBox="0 0 24 24" stroke="currentColor">
                                    <path stroke-linecap="round" stroke-linejoin="round" stroke-width="4"
                                        d="M6 18L18 6M6 6l12 12" />
                                </svg>
                            @endif
                        </x-table.cell>
                        <x-table.cell>
                            @if ($location->is_storable === 1)
                                <svg xmlns="http://www.w3.org/2000/svg" class="h-4 w-4 text-green-400" fill="none"
                                    viewBox="0 0 24 24" stroke="currentColor">
                                    <path stroke-linecap="round" stroke-linejoin="round" stroke-width="4"
                                        d="M5 13l4 4L19 7" />
                                </svg>
                            @else
                                <svg xmlns="http://www.w3.org/2000/svg" class="h-4 w-4 text-red-400" fill="none"
                                    viewBox="0 0 24 24" stroke="currentColor">
                                    <path stroke-linecap="round" stroke-linejoin="round" stroke-width="4"
                                        d="M6 18L18 6M6 6l12 12" />
                                </svg>
                            @endif
                        </x-table.cell>
                        <x-table.cell>
                            @if ($location->is_usable === 1)
                                <svg xmlns="http://www.w3.org/2000/svg" class="h-4 w-4 text-green-400" fill="none"
                                    viewBox="0 0 24 24" stroke="currentColor">
                                    <path stroke-linecap="round" stroke-linejoin="round" stroke-width="4"
                                        d="M5 13l4 4L19 7" />
                                </svg>
                            @else
                                <svg xmlns="http://www.w3.org/2000/svg" class="h-4 w-4 text-red-400" fill="none"
                                    viewBox="0 0 24 24" stroke="currentColor">
                                    <path stroke-linecap="round" stroke-linejoin="round" stroke-width="4"
                                        d="M6 18L18 6M6 6l12 12" />
                                </svg>
                            @endif
                        </x-table.cell>
                        <x-table.cell>{{ $location->storage_zone }}</x-table.cell>
                        <x-table.cell>{{ $location->count_zone }}</x-table.cell>
                        <x-table.cell>{{ $location->pick_zone }}</x-table.cell>
                    </x-table.row>
                @empty
                    <x-table.row>
                        <x-table.cell>{{ __('No Locations found.') }}</x-table.cell>
                    </x-table.row>
                @endforelse
            </x-table>
            <div class="p-2 mx-16 mb-10">
            {{ $locations->links() }}
            </div>
        </div>
    @endsection
</x-wms-layout>
