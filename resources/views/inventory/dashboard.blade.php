<x-wms-layout>
    @section('content')
    <div class="h-auto">
            @include('inventory.navbar')
            <div class="flex w-full justify-between justify-items-center h-14 shadow-lg">
                <h1 class="flex font-semibold text-2xl px-4 items-center py-2">{{ __('Inventory Dashboard') }}</h1>
            </div>
            <div class="flex">
            <div>{{ __('Inventory Dashboard Things...') }}</div>
            </div>
        </div>
    @endsection
</x-wms-layout>
