<x-wms-layout>
    @section('content')
    <div class="h-auto" x-data="{ showModal: false }">
            @include('inventory.navbar')
            @include('flash-message')
            <x-page.title>{{ __('Product Details') }}</x-page.title>
            <x-table>
                <x-buttons.primary>{{ __('Add New') }}</x-buttons.primary>
                <x-slot name="headings">
                            <x-table.heading>{{ __('Product') }}</x-table.heading>
                            <x-table.heading>{{ __('Description') }}</x-table.heading>
                            <x-table.heading>{{ __('UOM') }}</x-table.heading>
                            <x-table.heading>{{ __('Quantity') }}</x-table.heading>
                            <x-table.heading>{{ __('Height') }}</x-table.heading>
                            <x-table.heading>{{ __('Width') }}</x-table.heading>
                            <x-table.heading>{{ __('Length') }}</x-table.heading>
                            <x-table.heading>{{ __('Weight') }}</x-table.heading>
                </x-slot>
                        @forelse ($productDetails as $productDetail)
                        <x-table.row>
                                <x-table.cell>
                                    <x-page.link href="#">{{ $productDetail->product->product }}</x-page.link>
                                </x-table.cell>
                                <x-table.cell>
                                    {{ $productDetail->description }}
                                </x-table.cell>
                                <x-table.cell>
                                    {{ $productDetail->Uom->description }}
                                </x-table.cell>
                                <x-table.cell>
                                    {{ $productDetail->uom_qty }}
                                </x-table.cell>
                                <x-table.cell>
                                    {{ $productDetail->uom_height }}
                                </x-table.cell>
                                <x-table.cell>
                                    {{ $productDetail->uom_width }}
                                </x-table.cell>
                                <x-table.cell>
                                    {{ $productDetail->uom_length }}
                                </x-table.cell>
                                <x-table.cell>
                                    {{ $productDetail->uom_grossweight }}
                                </x-table.cell>
                        </x-table.row>
                            @empty
                            <x-table.row>
                                <x-table.cell>{{ __('No Product Details Found.') }}</x-table.cell>
                        </x-table.row>
                        @endforelse
                    </tbody>
                </x-table>
                {{ $productDetails->links() }}
            </div>
        @endsection
</x-wms-layout>