<x-wms-layout>
    @section('content')
        <div class="h-auto" x-data="{ showModal: false }">
            @include('inventory.navbar')
            @include('flash-message')
            <x-page.title>{{ __('Hold Types') }}</x-page.title>
            <x-table>
                <x-buttons.primary>{{ __('Add New') }}</x-buttons.primary>
                <x-slot name="headings">
                    <x-table.heading>{{ __('Hold Type') }}</x-table.heading>
                    <x-table.heading>{{ __('Description') }}</x-table.heading>
                    <x-table.heading>{{ __('Holds Count') }}</x-table.heading>
                </x-slot>
                @forelse ($holdTypes as $type)
                    <x-table.row>
                        <x-table.cell>{{ $type->name }}</x-table.cell>
                        <x-table.cell>{{ $type->description }}</x-table.cell>
                        <x-table.cell>{{ $holdType->hold_count ?? 0 }}</x-table.cell>
                    </x-table.row>
                @empty
                    <x-table.row>
                        <x-table.cell>{{ __('No Hold Types Found.') }}</x-table.cell>
                    </x-table.row>
                @endforelse
            </x-table>
            <div class="mx-16">
                {{ $holdTypes->links() }}
            </div>
            <!--new hold type-->
            <form method="POST" action="{{ route('inventory.holdTypes.store') }}">
                @csrf
                <x-modal>
                    <x-slot name="title">{{ __('New Hold Type') }}</x-slot>
                    <x-modal.input.textarea name="name" placeholder='Name...' required />
                    <x-modal.input.textarea name="description" placeholder='Description...' required />
                </x-modal>
            </form>
        </div>
    @endsection
</x-wms-layout>
