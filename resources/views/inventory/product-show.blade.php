<x-wms-layout>
    @section('content')
        <div class="h-auto" x-data="{ showModal: false }">
            @include('inventory.navbar')
            @include('flash-message')
            <x-page.title>{{ __('Product Details: ') }}{{ $product->product }} - {{ $product->description }}
            </x-page.title>
            <div class="flex justify-start bg-white mx-16 border-2 border-gray-300 rounded-lg">
                <div class="ml-4 my-2">
                    <h2 class="font-semibold pb-2">{{ 'Product Type ' }}</h2>
                    <select name="productType"
                        class="border-2 border-gray-300 p-2 rounded-lg focus:ring-0 focus:border-gray-300 w-full">
                        <option>{{ $product->product_type }}</option>
                        @foreach ($productTypes as $type)
                            @if ($type->name != $product->product_type)
                                <option value="{{ $type->id }}">{{ $type->name }}
                                </option>
                            @endif
                        @endforeach
                    </select>
                </div>
                <div class="ml-4 my-2">
                    <h2 class="font-semibold pb-2">{{ 'Default Receive Status' }}</h2>
                    <select name="productType"
                        class="border-2 border-gray-300 p-2 rounded-lg focus:ring-0 focus:border-gray-300 w-full">
                        <option>{{ $product->def_rcv_status }}</option>
                        @foreach ($inventoryStatuses as $status)
                            @if ($status->name != $product->def_rcv_status)
                                <option value="{{ $status->id }}">{{ $status->name }}
                                </option>
                            @endif
                        @endforeach
                    </select>
                </div>
                <div class="ml-4 my-2">
                    <h2 class="font-semibold pb-2">{{ 'Product Description' }}</h2>
                    <input type="textarea" name="description" placeholder="{{ $product->description }}"
                        class="border-2 border-gray-300 p-2 rounded-lg focus:outline-none focus:border-gray-300 placeholder-black">
                </div>

            </div>
            <div class="flex justify-items-start bg-white mx-16 border-2 border-gray-300 rounded-lg mt-2">
                <div class="ml-4 my-2">
                    <h2 class="font-semibold">{{ __('Receivable? ') }}</h2>
                    <p class="text-sm">
                        {{ __('Check to enable receiving for this product.') }}
                    </p>
                    <label data-toggle="checkbox-toggle" data-rounded="rounded-xl" data-handle-size="12"
                        data-handle-color="bg-white" data-off-color="bg-gray-300" data-on-color="bg-blue-500">
                        <input type="checkbox" name="rcv_flg" class="onOff"
                            {{ $product->rcv_flg ? 'checked' : '' }} />
                </div>
                <div class="ml-2 my-2">
                    <h2 class="font-semibold">{{ __('Hot Product?') }}</h2>
                    <p class="text-sm">
                        {{ __('If checked the product will be treated as Hot by all areas of the application.') }}
                    </p>
                    <label data-toggle="checkbox-toggle" data-rounded="rounded-xl" data-handle-size="12"
                        data-handle-color="bg-white" data-off-color="bg-gray-300" data-on-color="bg-blue-500">
                        <input type="checkbox" name="hot_flg" class="onOff"
                            {{ $product->hot_flg ? 'checked' : '' }} />
                </div>
                <div class="ml-4 my-2">
                    <h2 class="font-semibold">{{ __('Send to Host?') }}</h2>
                    <p class="text-sm">
                        {{ __('Must be checked if the product needs to interact with a host.') }}
                    </p>
                    <label data-toggle="checkbox-toggle" data-rounded="rounded-xl" data-handle-size="12"
                        data-handle-color="bg-white" data-off-color="bg-gray-300" data-on-color="bg-blue-500">
                        <input type="checkbox" name="send_to_host_flg" class="onOff"
                            {{ $product->send_to_host_flg ? 'checked' : '' }} />
                </div>
                <div class="ml-4 my-2">
                    <h2 class="font-semibold">{{ __('Hazardous Product?') }}</h2>
                    <p class="text-sm">
                        {{ __('If checked the product will be treated as Hazardous by all areas of the application.') }}
                    </p>
                    <label data-toggle="checkbox-toggle" data-rounded="rounded-xl" data-handle-size="12"
                        data-handle-color="bg-white" data-off-color="bg-gray-300" data-on-color="bg-blue-500">
                        <input type="checkbox" name="haz_flg" class="onOff"
                            {{ $product->haz_flg ? 'checked' : '' }} />
                </div>
                <div class="ml-4 my-2 mr-2">
                    <h2 class="font-semibold">{{ __('Returnable Product?') }}</h2>
                    <p class="text-sm">
                        {{ __('If checked the product will be treated as returnable by all areas of the application.') }}
                    </p>
                    <label data-toggle="checkbox-toggle" data-rounded="rounded-xl" data-handle-size="12"
                        data-handle-color="bg-white" data-off-color="bg-gray-300" data-on-color="bg-blue-500">
                        <input type="checkbox" name="returnable_flg" class="onOff"
                            {{ $product->returnable_flg ? 'checked' : '' }} />
                </div>
            </div>
            <div class="p-2">
                <button type="button"
                    class="p-2 px-4 bg-green-500 hover:bg-green-700 text-white ml-16 my- rounded-lg">{{ __('Save Product Changes') }}</button>
                <button type="button"
                    class="p-2 px-4 bg-red-500 hover:bg-red-700 text-white ml-2 my- rounded-lg">{{ __('Cancel') }}</button>
            </div>
            <x-table>
                <x-buttons.primary>{{ __('Assign New UOM') }}</x-buttons.primary>
                <p class="italic">{{ __('Click the UOM Link for more UOM specific options for this item.') }}
                    <span
                        class="font-semibold italic text-blue-800">{{ __(' note: flags changed in the UOM table are effective immediatley.') }}</span>
                </p>
                <x-slot name="headings">
                    <x-table.heading>{{ __('UOM') }}</x-table.heading>
                    <x-table.heading>{{ __('LPN Level') }}</x-table.heading>
                    <x-table.heading>{{ __('Unit Quantity') }}</x-table.heading>
                    <x-table.heading>{{ __('Default') }}</x-table.heading>
                    <x-table.heading>{{ __('Height(cm)') }}</x-table.heading>
                    <x-table.heading>{{ __('Width(cm)') }}</x-table.heading>
                    <x-table.heading>{{ __('Length(cm)') }}</x-table.heading>
                    <x-table.heading>{{ __('Weight(gross)') }}</x-table.heading>
                    <x-table.heading>{{ __('Receivable') }}</x-table.heading>
                    <x-table.heading>{{ __('Pick') }}</x-table.heading>
                    <x-table.heading>{{ __('Replenishment') }}</x-table.heading>
                </x-slot>
                @forelse ($product->productDetails as $productDetail)
                    <x-table.row>
                        <x-table.cell>
                            <x-page.link href="#">{{ $productDetail->Uom->description }}</x-page.link>
                        </x-table.cell>
                        <x-table.cell>{{ $productDetail->lpn_level }}</x-table.cell>
                        <x-table.cell>{{ $productDetail->uom_qty }}</x-table.cell>
                        <x-table.cell>{{ $productDetail->default_uom_qty }}</x-table.cell>
                        <x-table.cell>{{ $productDetail->uom_height }}</x-table.cell>
                        <x-table.cell>{{ $productDetail->uom_width }}</x-table.cell>
                        <x-table.cell>{{ $productDetail->uom_length }}</x-table.cell>
                        <x-table.cell>{{ $productDetail->uom_grossweight }}</x-table.cell>
                        <x-table.cell>{{ $productDetail->uom_receive_flg }}</x-table.cell>
                        <x-table.cell>{{ $productDetail->uom_pick_flg }}</x-table.cell>
                        <x-table.cell>{{ $productDetail->uom_replen_flg }}</x-table.cell>
                    </x-table.row>
                @empty
                    <x-table.row>
                        <x-table.cell>{{ __('No Product Details Found.') }}</x-table.cell>
                    </x-table.row>
                @endforelse
                </tbody>
            </x-table>
        </div>
    @endsection
</x-wms-layout>
