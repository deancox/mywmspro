<x-wms-layout>
    @section('content')
        <div class="h-auto" x-data="{ showModal: false }">
            @include('inventory.navbar')
            @include('flash-message')
            <x-page.title>{{ __('Inventory Statuses') }}</x-page.title>
            <x-table>
                <x-buttons.primary>{{ __('Add New') }}</x-buttons.primary>
                <x-slot name="headings">
                    <x-table.heading>{{ __('Status') }}</x-table.heading>
                    <x-table.heading>{{ __('Description') }}</x-table.heading>
                    <x-table.heading>{{ __('In Use') }}</x-table.heading>
                    <x-table.heading>{{ __('Created') }}</x-table.heading>
                    <x-table.heading>{{ __('Modified') }}</x-table.heading>
                    <x-table.heading>{{ __('Created By') }}</x-table.heading>
                </x-slot>
                @forelse ($statuses as $status)
                    <x-table.row>
                        <x-table.cell>
                            <x-page.link href="#">{{ $status->name }}</x-page.link>
                        </x-table.cell>
                        <x-table.cell>{{ $status->description }}</x-table.cell>
                        <td class="pl-2">
                            @if ($status->in_use === 1)
                                <svg xmlns="http://www.w3.org/2000/svg" class="h-4 w-4 text-green-400" fill="none"
                                    viewBox="0 0 24 24" stroke="currentColor">
                                    <path stroke-linecap="round" stroke-linejoin="round" stroke-width="4"
                                        d="M5 13l4 4L19 7" />
                                </svg>
                            @else
                                <svg xmlns="http://www.w3.org/2000/svg" class="h-4 w-4 text-red-400" fill="none"
                                    viewBox="0 0 24 24" stroke="currentColor">
                                    <path stroke-linecap="round" stroke-linejoin="round" stroke-width="4"
                                        d="M6 18L18 6M6 6l12 12" />
                                </svg>
                            @endif
                        </td>
                        <x-table.cell>{{ $status->created_at }}</x-table.cell>
                        <x-table.cell>{{ $status->updated_at }}</x-table.cell>
                        <x-table.cell>{{ $status->created_by }}</x-table.cell>
                    </x-table.row>
                @empty
                    <x-table.row>
                        <x-table.cell>{{ __('No Inventory Statuses found.') }}</x-table.cell>
                    </x-table.row>
                @endforelse
            </x-table>
            <div class="mx-16">
                {{ $statuses->links() }}
            </div>
            <!--new Inventory Status modal-->
            <form method="POST" action="{{ route('inventory.inventoryStatus.store') }}">
                @csrf
                <x-modal>
                    <x-slot name="title">{{ __('New Inventory Status') }}</x-slot>
                    <x-modal.input.textarea name="name" placeholder='Name...' required />
                    <x-modal.input.textarea name="description" placeholder='Description...' required />
                </x-modal>
            </form>
        </div>
    @endsection
</x-wms-layout>
