<div class="flex sticky top-16 h-14 justify-items-start gap-4 text-gray-800 pl-2 font-semibold bg-white border-b-2 border-gray-500">
     <ul class="inline-flex items-center">
         <li x-data="{countOptions:false}" class="px-2">
             <button class="font-semibold outline-none focus:outline-none hover:text-blue-400"
                 @click="countOptions = !countOptions">{{ __('Counts') }}
                 <svg xmlns="http://www.w3.org/2000/svg" class="h-4 w-4 inline" fill="none" viewBox="0 0 24 24"
                     stroke="currentColor">
                     <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M19 9l-7 7-7-7" />
                 </svg>
             </button>
             <div class="absolute bg-white text-black  rounded-md mt-5 shadow-lg px-2"
                 :class="{'hidden':!countOptions,'flex flex-col':countOptions}" @click.away="countOptions = false">
                 <ul class="space-y-2">
                     <li>
                         <a href="#"
                             class="flex p-2 bg-red-200 rounded-md hover:bg-gray-200">{{ __('Outstanding Counts') }}</a>
                     </li>
                     <li>
                         <a href="#"
                             class="flex p-2 bg-red-200 rounded-md hover:bg-gray-200">{{ __('Count Discrepancies') }}</a>
                     </li>
                     <li>
                         <a href="#"
                             class="flex p-2 bg-red-200 rounded-md hover:bg-gray-200">{{ __('Create Count Batch') }}</a>
                     </li>
                     <li>
                         <a href="#"
                             class="flex p-2 bg-red-200 rounded-md hover:bg-gray-200">{{ __('Count History') }}</a>
                     </li>
                 </ul>
             </div>
         </li>
         <li x-data="{holdOptions:false}" class="px-2">
             <button class="font-semibold outline-none focus:outline-none hover:text-blue-400"
                 @click="holdOptions = !holdOptions">{{ __('Holds') }}
                 <svg xmlns="http://www.w3.org/2000/svg" class="h-4 w-4 inline" fill="none" viewBox="0 0 24 24"
                     stroke="currentColor">
                     <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M19 9l-7 7-7-7" />
                 </svg></button>

             <div class="absolute bg-white text-black  rounded-md p-2 mt-5 shadow-lg"
                 :class="{'hidden':!holdOptions,'flex flex-col':holdOptions}" @click.away="holdOptions = false">
                 <ul class="space-y-2">
                     <li>
                         <a href="{{ route('inventory.holdTypes.index') }}"
                             class="flex p-2 rounded-md hover:bg-gray-200">{{ __('Hold Types') }}</a>
                     </li>
                     <li>
                         <a href="#"
                             class="flex p-2 bg-red-200 rounded-md hover:bg-gray-200">{{ __('Applied Holds') }}</a>
                     </li>
                 </ul>
             </div>
         </li>
         <li x-data="{productOptions:false}" class="px-2">
             <button class="font-semibold outline-none focus:outline-none hover:text-blue-400"
                 @click="productOptions = !productOptions">{{ __('Products') }}
                 <svg xmlns="http://www.w3.org/2000/svg" class="h-4 w-4 inline" fill="none" viewBox="0 0 24 24"
                     stroke="currentColor">
                     <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M19 9l-7 7-7-7" />
                 </svg></button>

             <div class="absolute bg-white text-black  rounded-md p-2 mt-5 shadow-lg"
                 :class="{'hidden':!productOptions,'flex flex-col':productOptions}"
                 @click.away="productOptions = false">
                 <ul class="space-y-2">
                     <li>
                         <a href="{{ route('inventory.product.index') }}"
                             class="flex p-2 rounded-md hover:bg-gray-200">{{ __('All Products') }}</a>
                     </li>
                     <li class="hidden">
                         <!--50/50 about this - maybe duplicate the view-->
                         <a href="{{ route('config.UOMS.index') }}"
                             class="flex p-2 rounded-md hover:bg-gray-200">{{ __('UOM\'s') }}</a>
                     </li>
                     <li>
                         <a href="{{ route('inventory.productDetail.index') }}"
                             class="flex p-2 bg-red-200 rounded-md hover:bg-gray-200">{{ __('Product Details') }}</a>
                     </li>
                 </ul>
             </div>
         </li>
         <li x-data="{statusOptions:false}" class="px-2">
             <button class="font-semibold outline-none focus:outline-none hover:text-blue-400"
                 @click="statusOptions = !statusOptions">{{ __('Inventory Status') }}
                 <svg xmlns="http://www.w3.org/2000/svg" class="h-4 w-4 inline" fill="none" viewBox="0 0 24 24"
                     stroke="currentColor">
                     <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M19 9l-7 7-7-7" />
                 </svg></button>
             <div class="absolute bg-white text-black  rounded-md p-2 mt-5 shadow-lg"
                 :class="{'hidden':!statusOptions,'flex flex-col':statusOptions}" @click.away="statusOptions = false">
                 <ul class="space-y-2">
                     <li>
                         <a href="{{ route('inventory.inventoryStatus.index') }}"
                             class="flex p-2 rounded-md hover:text-blue-400">{{ __('Inventory Statuses') }}</a>
                     </li>
                 </ul>
             </div>
         </li>
         <li class="px-2">
             <button
                 class="font-semibold outline-none focus:outline-none hover:text-blue-400">{{ __('PickFaces') }}</button>
         </li>
         <li class="px-2">
             <button class="font-semibold outline-none focus:outline-none hover:text-blue-400"><a
                     href="{{ route('location.index') }}">{{ __('Locations') }}</a></button>
         </li>
         <li class="px-2">
            <button class="font-semibold outline-none focus:outline-none hover:text-blue-400"><a
                    href="{{ route('work.index') }}">{{ __('Work') }}</a></button>
        </li>
     </ul>     
 </div>


