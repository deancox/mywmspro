<div class="flex-1">
    <div class="h-14 w-full flex justify-items-center gap-4 text-gray-800  pl-2 font-semibold">
        <div class="flex items-center font-semibold outline-none focus:outline-none hover:text-blue-400"><a
                href="#">{{ __('Customers') }}</a></div>
        <div class="flex items-center font-semibold outline-none focus:outline-none hover:text-blue-400"><a
                href="#">{{ __('Orders') }}</a></div>
        <div class="flex items-center font-semibold outline-none focus:outline-none hover:text-blue-400"><a
                href="#">{{ __('Order History') }}</a></div>
        <div class="flex items-center font-semibold outline-none focus:outline-none hover:text-blue-400"><a
                href="#">{{ __('Staging') }}</a></div>
        <div class="flex items-center font-semibold outline-none focus:outline-none hover:text-blue-400"><a
                href="#">{{ __('Doors') }}</a></div>
        <div class="flex items-center font-semibold outline-none focus:outline-none hover:text-blue-400"><a
                href="#">{{ __('Work') }}</a></div>
        <div class="flex items-center font-semibold outline-none focus:outline-none hover:text-blue-400"><a
                href="{{ route('outbound.constraints.index') }}">{{ __('Constraints') }}</a></div>
    </div>
</div>
