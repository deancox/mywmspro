<x-wms-layout>
    @section('content')
        <div class="flex-1">
            @include('outbound.navbar')
            <div class="flex justify-between justify-items-center border-t-2 border-gray-500 h-14 shadow-lg">
                <h1 class="flex font-semibold text-2xl px-4 items-center py-2">{{__('Outbound Dashboard') }}</h1>
            </div>
            <div>{{ __('Outbound Dashboard Things...') }}</div>
        </div>
    @endsection
</x-wms-layout>
