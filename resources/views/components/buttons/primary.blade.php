<button @click={showModal=true} {{ $attributes->merge(['class' => "bg-blue-500 hover:bg-blue-700 focus:outline-none rounded mr-6 px-4 mb-2 text-center items-center h-8 font-bold text-white"])}}>
    {{ $slot }}
</button>