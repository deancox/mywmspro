<div class="w-full">
    <nav class="h-16 w-full flex items-center lg:items-stretch justify-end lg:justify-between bg-gray-500 shadow-1xl relative">
        <div class="hidden lg:flex w-full pr-6">
            <div class="w-1/2 hidden lg:flex">
                <div class="w-full flex pl-8 justify-end">
                    <!--notification bell-->
                    <div class="hidden">
                        <div class="h-full w-20 flex items-center justify-center border-r border-l">
                            <div class="relative cursor-pointer text-gray-600">
                                <svg xmlns="http://www.w3.org/2000/svg" class="icon icon-tabler icon-tabler-bell"
                                    width="28" height="28" viewBox="0 0 24 24" stroke-width="1.5" stroke="currentColor"
                                    fill="none" stroke-linecap="round" stroke-linejoin="round">
                                    <path stroke="none" d="M0 0h24v24H0z"></path>
                                    <path
                                        d="M10 5a2 2 0 0 1 4 0a7 7 0 0 1 4 6v3a4 4 0 0 0 2 3h-16a4 4 0 0 0 2 -3v-3a7 7 0 0 1 4 -6">
                                    </path>
                                    <path d="M9 17v1a3 3 0 0 0 6 0v-1"></path>
                                </svg>
                                <!--red on notification bell-->
                                <div
                                    class="w-2 h-2 rounded-full bg-red-400 border border-white absolute inset-0 mt-1 mr-1 m-auto">
                                </div>
                            </div>
                        </div>
                    </div>
                    <!--message icon-->
                    <div class="hidden">
                        <div
                            class="h-full w-20 flex items-center justify-center border-r mr-4 cursor-pointer text-gray-600">
                            <svg xmlns="http://www.w3.org/2000/svg" class="icon icon-tabler icon-tabler-messages"
                                width="28" height="28" viewBox="0 0 24 24" stroke-width="1.5" stroke="currentColor"
                                fill="none" stroke-linecap="round" stroke-linejoin="round">
                                <path stroke="none" d="M0 0h24v24H0z" />
                                <path d="M21 14l-3 -3h-7a1 1 0 0 1 -1 -1v-6a1 1 0 0 1 1 -1h9a1 1 0 0 1 1 1v10" />
                                <path d="M14 15v2a1 1 0 0 1 -1 1h-7l-3 3v-10a1 1 0 0 1 1 -1h2" />
                            </svg>
                        </div>
                    </div>
                    <div class="flex items-end relative cursor-pointer z-50" onclick="dropdownHandler(this)">
                        <div class="rounded-full">
                            <ul
                                class="p-2 w-full border-r bg-gray-200 absolute rounded left-0 shadow mt-12 sm:mt-16 hidden">
                                <li
                                    class="flex w-full justify-between text-gray-600  cursor-pointer items-center">
                                    <div class="flex items-center">
                                        <svg xmlns="http://www.w3.org/2000/svg"
                                            class="icon icon-tabler icon-tabler-user" width="18" height="18"
                                            viewBox="0 0 24 24" stroke-width="1.5" stroke="currentColor" fill="none"
                                            stroke-linecap="round" stroke-linejoin="round">
                                            <path stroke="none" d="M0 0h24v24H0z" />
                                            <circle cx="12" cy="7" r="4" />
                                            <path d="M6 21v-2a4 4 0 0 1 4 -4h4a4 4 0 0 1 4 4v2" />
                                        </svg>
                                        <x-jet-responsive-nav-link href="{{ route('profile.show') }}"
                                            :active="request()->routeIs('profile.show')">
                                            {{ __('Profile') }}
                                        </x-jet-responsive-nav-link>
                                    </div>
                                </li>
                                <li
                                    class="flex w-full justify-between text-gray-600  cursor-pointer items-center mt-2">
                                    <div class="flex items-center">
                                        <svg xmlns="http://www.w3.org/2000/svg"
                                            class="icon icon-tabler icon-tabler-logout" width="20" height="20"
                                            viewBox="0 0 24 24" stroke-width="1.5" stroke="currentColor" fill="none"
                                            stroke-linecap="round" stroke-linejoin="round">
                                            <path stroke="none" d="M0 0h24v24H0z" />
                                            <path
                                                d="M14 8v-2a2 2 0 0 0 -2 -2h-7a2 2 0 0 0 -2 2v12a2 2 0 0 0 2 2h7a2 2 0 0 0 2 -2v-2" />
                                            <path d="M7 12h14l-3 -3m0 6l3 -3" />
                                        </svg>
                                        <form method="POST" action="{{ route('logout') }}">
                                            @csrf
                                            <x-jet-responsive-nav-link href="{{ route('logout') }}" onclick="event.preventDefault();
                                            this.closest('form').submit();">
                                                {{ __('Logout') }}
                                            </x-jet-responsive-nav-link>
                                        </form>
                                        {{-- <span class="xl:text-base md:text-2xl text-base ml-2"><a href="{{ route('logout') }}">Sign out</a></span> --}}
                                    </div>
                                </li>
                            </ul>
                            <div class="relative w-10">
                                <!--profile image-->
                                {{-- <img class="rounded-full h-10 w-10 object-cover"
                                    src="#"
                                    alt="avatar" /> --}}
                               <!-- <div
                                    class="w-2 h-2 rounded-full bg-green-400 border border-white absolute inset-0 mb-0 mr-0 m-auto">
                                </div>-->
                            </div>
                        </div>
                        <p class="text-gray-100 font-bold text-sm mx-3">{{ Auth::user()->name }}</p>
                        <div class="cursor-pointer text-gray-100">
                            <svg aria-haspopup="true" xmlns="http://www.w3.org/2000/svg"
                                class="icon icon-tabler icon-tabler-chevron-down" width="20" height="20"
                                viewBox="0 0 24 24" stroke-width="1.5" stroke="currentColor" fill="none"
                                stroke-linecap="round" stroke-linejoin="round">
                                <path stroke="none" d="M0 0h24v24H0z" />
                                <polyline points="6 9 12 15 18 9" />
                            </svg>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="text-gray-600 mr-8 visible lg:hidden relative" onclick="sidebarHandler(true)" id="menu">
            <svg aria-label="Main Menu" aria-haspopup="true" xmlns="http://www.w3.org/2000/svg"
                class="icon icon-tabler icon-tabler-menu cursor-pointer" width="30" height="30" viewBox="0 0 24 24"
                stroke-width="1.5" stroke="currentColor" fill="none" stroke-linecap="round" stroke-linejoin="round">
                <path stroke="none" d="M0 0h24v24H0z" />
                <line x1="4" y1="8" x2="20" y2="8" />
                <line x1="4" y1="16" x2="20" y2="16" />
            </svg>
        </div>
    </nav>


