<div class="flex justify-between justify-items-center">
    <h1 class="flex font-semibold text-2xl px-4 items-center py-2">{{ $slot }}</h1>
</div>