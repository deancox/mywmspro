<div class="flex justify-center">
    <div class="z-40 overflow-auto left-0 top-20 bottom-0 right-0 w-full h-full fixed" x-show="showModal" tabindex="0"
        x-transition:enter="transition ease-in duration-200" x-transition:enter-start="transform opacity-0"
        x-transition:enter-end="transform opacity-100" x-transition:leave="transition ease-out duration-200"
        x-transition:leave-start="transform opacity-100" x-transition:leave-end="transform opacity-0">
        <div @click.away="showModal = false" class="z-50 relative p-3 mx-auto my-0 max-w-full" style="width: 600px;">
            <div class="bg-white rounded shadow-lg border flex flex-col overflow-hidden" id="showModal">
                <div {{ $attributes->merge(['class' => 'px-6 py-3 text-xl border-b font-bold']) }}>{{ $title }}</div>
                {{ $slot }}
                <div class="relative inline-block pl-4 pb-2">
                </div>
                <div class="px-6 py-3 border-t">
                    <div class="flex justify-end">
                        <x-modal.buttons.save></x-modal.buttons.save>
                        <button @click={showModal=false} type="button"
                            class="bg-red-400 hover:bg-red-500 rounded px-4 py-2 ml-2 focus:outline-none">{{ __('Cancel') }}</Button>
                    </div>
                </div>
                <button @click={showModal=false} type="button"
                    class="fill-current h-6 w-6 absolute right-0 top-0 m-6 text-2xl font-bold hover:text-red-500 focus:outline-none">&times;</button>
            </div>
        </div>
        <div class="z-40 overflow-auto left-0 top-0 bottom-0 right-0 w-full h-full fixed bg-black opacity-50">
        </div>
    </div>
</div>
