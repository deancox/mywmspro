<div class="form-group px-4">
    @if (isset($label))
        <label class="font-semibold">{{ $label }}</label>
        <br>
    @endif
    <select name="{{ $name ?? '' }}" id="{{ $id ?? '' }}"
        {{ $attributes->merge(['class' => 'rounded-lg focus:outline-none focus:ring-0 focus:bg-gray-100 border-gray-400 bg-gray-100 w-full' ]) }}>
        {{ $slot }}
    </select>
</div>
