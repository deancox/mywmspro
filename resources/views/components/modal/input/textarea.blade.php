<div class="form-group px-4">
    <textarea {{ $attributes->merge(['class'=>'bg-gray-100 rounded border border-gray-400 leading-normal resize-none w-full h-12 px-3 font-medium placeholder-gray-700 focus:outline-none focus:bg-white']) }}></textarea>
</div>