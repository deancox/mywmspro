<div {{ $attributes->merge(['class' => 'px-6 py-3 text-md italic']) }}>
    {{ $slot }}
</div>