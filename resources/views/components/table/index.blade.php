@props(['margin' => 'mx-16'])

<div {{ $attributes->merge(['class' => 'p-4 shadow-lg border-2 border-gray-300 mt-2 bg-white rounded-lg '. $margin]) }}>
    <table class="w-full bg-white text-left">
        <thead class="w-full border-b-2 border-black">
            <tr class="w-full border-b border-black bg-gray-200">
                {{ $headings }}
            </tr>
        </thead>

        <tbody {{ $attributes->merge(['class' => 'bg-grey-light sortable']) }}>
            {{ $slot }}
        </tbody>
    </table>
</div>