<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'MyWMS') }}</title>

    <!-- Fonts -->
    <link rel="stylesheet" href="https://fonts.googleapis.com/css2?family=Nunito:wght@400;600;700&display=swap">

    <!-- Styles -->
    <link rel="stylesheet" href="{{ mix('css/app.css') }}">
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.25/css/jquery.dataTables.css">


    @livewireStyles

    <!-- Scripts -->
    <script src="{{ mix('js/app.js') }}" defer></script>


    <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script>
    <script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/1.10.25/js/jquery.dataTables.js"></script>

    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.min.js"
        integrity="sha256-VazP97ZCwtekAsvgPBSUwPFKdrwD3unUfSGVYrahUqU=" crossorigin="anonymous"></script>
    <script src="{{ URL::asset('js/jquery-tailwind-toggle.js') }}"></script>



</head>

<body class="font-sans antialiased">
    <!--<x-jet-banner />-->

    <!-- Page Heading -->
    @if (isset($header))
        <header class="bg-white shadow">
            <div class="max-w-7xl mx-auto py-6 px-4 sm:px-6 lg:px-8">
                {{ $header }}
            </div>
        </header>
    @endif

    <!-- Page Content -->
    <main class="flex bg-blue-50">
        @include('wms-sidebar')
        <div class="flex flex-col w-full">

            @include('search-bar')

            @yield('content')
            <!--loading display-->
            <div id="loading" hidden>
                <div class="z-50 flex justify-center top-60 bottom-0 left-0 right-0 fixed">
                    <div
                        class="flex items-center justify-center bg-gray-100 h-36 w-96 top-0 rounded-lg border-2 border-gray-700 shadow-xl">
                        <div
                            class="spinner-border animate-spin  inline-block w-8 h-8 mr-3 border-4 border-blue-600 rounded-xl bg-blue-600">
                        </div>
                        <p class="font-semibold text-xl">{{ __('Processing...') }}</p>
                    </div>
                </div>
                <div class="z-40 overflow-auto left-0 top-0 bottom-0 right-0 w-full h-full fixed bg-black opacity-70">
                </div>
            </div>

            {{-- $slot --}}
    </main>


    @stack('modals')

    @livewireScripts

    <script src="https://cdn.jsdelivr.net/gh/alpinejs/alpine@v2.x.x/dist/alpine.min.js" defer></script>

    <script>
        $("#loading").fadeIn(1000);

        $(document).ready(function() {

            $(".alert").delay(2000).slideUp(500);

            $("#loading").hide();

            $("form").submit(function() {
                $("#loading").fadeIn(1000);
            });
        });
    </script>

</body>

</html>
