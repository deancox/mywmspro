<x-wms-layout>
    @section('content')
        <div class="h-auto" x-data="{ showModal: false }">
            @include('flash-message')
            <x-page.title>{{ __('Outstanding Work') }}</x-page.title>
            <x-table>
                <x-slot name="headings">
                    <x-table.heading>{{ __('Type') }}</x-table.heading>
                    <x-table.heading>{{ __('Status') }}</x-table.heading>
                    <x-table.heading>{{ __('Base Priority') }}</x-table.heading>
                    <x-table.heading>{{ __('Priority') }}</x-table.heading>
                    <x-table.heading>{{ __('Source') }}</x-table.heading>
                    <x-table.heading>{{ __('Destination') }}</x-table.heading>
                    <x-table.heading>{{ __('Estimated Effort') }}</x-table.heading>
                </x-slot>
                @forelse ($works as $work)
                    <x-table.row>
                        <x-table.cell>{{ $work->type }}</x-table.cell>
                        <x-table.cell>{{ $work->status }}</x-table.cell>
                        <x-table.cell>{{ $work->base_priority }}</x-table.cell>
                        <x-table.cell>{{ $work->priority }}</x-table.cell>
                        <x-table.cell>{{ $work->source_location }}</x-table.cell>
                        <x-table.cell>{{ $work->destination_location }}</x-table.cell>
                        @if($work->estimate_mins > 60)
                        <x-table.cell class="text-red-600 font-semibold">{{ Carbon\CarbonInterval::minutes($work->estimate_mins)->cascade()->forHumans() }}</x-table.cell>
                        @else
                        <x-table.cell class="text-green-600 font-semibold">{{ Carbon\CarbonInterval::minutes($work->estimate_mins)->cascade()->forHumans() }}</x-table.cell>
                        @endif
                    </x-table.row>
                @empty
                    <x-table.row>
                        <x-table.cell>{{ __('No Work Found.') }}</x-table.cell>
                    </x-table.row>
                @endforelse
               
            </x-table>
            <div class="mx-20 m-4">
                {{ $works->links() }}
            </div>
        
        </div>
    @endsection
</x-wms-layout>