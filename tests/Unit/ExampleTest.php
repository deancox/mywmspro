<?php

namespace Tests\Unit;

//use PHPUnit\Framework\TestCase;
use Tests\TestCase;

class ExampleTest extends TestCase
{
    /**
     * A basic test example.
     *
     * @return void
     */
    public function test_example()
    {
        $this->assertTrue(true);
    }

    public function test_example1()
    {
        //exploring testing 
        $response = $this->get('/');
        $response->assertSee('login');
        $response->assertStatus(200);
    }
}
