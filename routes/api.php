<?php

use App\Http\Controllers\Api\EDI\AsnController;
use App\Http\Controllers\Api\MobileDisplay\MenuController;
use App\Models\Location;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});
  
Route::get('mainMenu', [MenuController::class,'index']);

//Route::resource('asnIn', AsnController::class);

Route::get('asnIn', [AsnController::class, 'index']);