<?php

//Dashboards
use App\Http\Controllers\Configuration\Dashboard\DashboardController as ConfigurationDashboardController;
use App\Http\Controllers\Inventory\Dashboard\DashboardController as InventoryDashboardController;
use App\Http\Controllers\Outbound\Dashboard\DashboardController as OutboundDashboardController;
use App\Http\Controllers\Inbound\Dashboard\DashboardController as InboundDashboardController;
//Configuration
use App\Http\Controllers\Configuration\Devices\DeviceController;
use App\Http\Controllers\Configuration\Devices\DeviceTypeController;
use App\Http\Controllers\Configuration\Hardware\PrinterController;
use App\Http\Controllers\Configuration\Hardware\PrinterTypeController;
use App\Http\Controllers\Configuration\Inbound\InboundConfigController;
use App\Http\Controllers\Configuration\Inbound\StorageRuleController;
use App\Http\Controllers\Configuration\Locations\LocationTypeController;
use App\Http\Controllers\Configuration\MobileConfiguration\MobileMenuController;
use App\Http\Controllers\Configuration\Outbound\PickTypeController;
use App\Http\Controllers\Configuration\Products\ProductTypeController;
use App\Http\Controllers\Configuration\Products\UOMController;
use App\Http\Controllers\Configuration\Uploads\ConfigUploadController;
use App\Http\Controllers\Configuration\Work\WorkTypeController;
use App\Http\Controllers\Configuration\Zones\CountZoneController;
use App\Http\Controllers\Configuration\Zones\PickZoneController;
use App\Http\Controllers\Configuration\Zones\StorageZoneController;
//Inbound
use App\Http\Controllers\Inbound\Deliveries\DeliveryReceiptController;
use App\Http\Controllers\Inbound\Deliveries\DeliveryStagingController;
use App\Http\Controllers\Inbound\Deliveries\DeliveryController;
use App\Http\Controllers\Inbound\Deliveries\DeliveryDetailController;
use App\Http\Controllers\Inbound\CheckInController;
//Outbound
use App\Http\Controllers\Outbound\Constraints\ConstraintController;
//Inventory
use App\Http\Controllers\Inventory\Products\ProductDetailController;
use App\Http\Controllers\Inventory\Products\ProductController;
use App\Http\Controllers\Inventory\Holds\HoldController;
use App\Http\Controllers\Inventory\InventoryStatus\InventoryStatusController;
//Locations
use App\Http\Controllers\Locations\LocationController;
use App\Http\Controllers\Doors\DoorController;
use App\Http\Controllers\Inventory\Holds\HoldTypeController;
use App\Http\Controllers\StagingLanes\InboundStagingLaneController;
//Work
use App\Http\Controllers\Work\WorkController;
use Illuminate\Support\Facades\Route;


/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::middleware(['auth:sanctum', 'verified'])->group(function () {

    //home
    Route::view('/dashboard', 'dashboard')->name('dashboard');

    //locations
    Route::resource('location', LocationController::class);

    //work
    Route::resource('work', WorkController::class);

    //inbound
    Route::group(['prefix' => 'inbound', 'as' => 'inbound.'], function () {

        Route::get('dashboard', InboundDashboardController::class)->name('dashboard');
        Route::get('checkIn/{delivery}', [CheckInController::class, 'index'])->name('checkIn');
        Route::post('checkInConfirm/{delivery}', [CheckInController::class, 'store'])->name('checkInConfirm');

        Route::resource('deliveries', DeliveryController::class);
        Route::resource('deliveryDetails', DeliveryDetailController::class);
        Route::resource('stagingLanes', DeliveryStagingController::class);
        Route::resource('receipt', DeliveryReceiptController::class);
    });

    //outbound
    Route::group(['prefix' => 'outbound', 'as' => 'outbound.'], function () {
        //Route::get('dashboard', OutboundDashboardController::class)->name('dashboard');
        Route::resource('constraints', ConstraintController::class);
    });

    //inventory
    Route::group(['prefix' => 'inventory', 'as' => 'inventory.'], function () {

        Route::get('dashboard', InventoryDashboardController::class)->name('dashboard');
        Route::resource('product', ProductController::class);
        Route::get('productDetails/{product}', [ProductDetailController::class, 'ajax']);
        Route::resource('productDetail', ProductDetailController::class);
        Route::resource('inventoryStatus', InventoryStatusController::class);
        Route::resource('holdTypes', HoldTypeController::class);
        Route::resource('holds', HoldController::class);
    });

    //Doors
    Route::resource('doors', DoorController::class);


    //Staging Lanes
    Route::resource('stagingLanes', InboundStagingLaneController::class);


    //Configuration
    Route::group(['prefix' => 'Configuration', 'as' => 'config.'], function () {
        Route::get('dashboard', ConfigurationDashboardController::class)->name('dashboard');

        //Mobile Devices
        Route::post('mobileConfigSort', [MobileMenuController::class, 'updateSortOrder'])->name('mobileConfigSort');       
        Route::resource('mobileConfig', MobileMenuController::class);

        //Products
        Route::resource('productTypes', ProductTypeController::class);
        Route::resource('UOMS', UOMController::class);

        //Locations 
        Route::get('locationTypes/{location_type}', [LocationTypeController::class, 'showLocations'])->name('showLocations');
        Route::resource('locationTypes', LocationTypeController::class);

        //Zones 
        Route::resource('countZones', CountZoneController::class);
        Route::resource('pickZones', PickZoneController::class);
        Route::resource('storageZones', StorageZoneController::class);

        //Devices
        Route::resource('deviceTypes', DeviceTypeController::class);
        Route::resource('devices', DeviceController::class);

        //Printers
        Route::resource('printers', PrinterController::class);
        Route::resource('printerTypes', PrinterTypeController::class);

        //Outbound
        Route::resource('pickTypes', PickTypeController::class);

        //Inbound
        Route::resource('inboundConfig', InboundConfigController::class);
        Route::resource('storageRules', StorageRuleController::class);

        //Uploads
        Route::resource('configupload', ConfigUploadController::class);

        //Work
        Route::resource('workTypes', WorkTypeController::class);
    });
});
