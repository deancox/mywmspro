<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Printer extends Model
{
    use HasFactory;

    protected $fillable = [
        'name',
        'name_on_server',
        'ip',
        'printer_type_id',
        'enabled'
    ];

    public function printerType()
    {
        return $this->belongsTo(PrinterType::class);
    }

    public function toggleEnabled(Bool $value)
    {
        $this->update(['enabled' => $value]);
    }
}
