<?php

namespace App\Models\Inbound;

use App\Models\Delivery;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Supplier extends Model
{
    use HasFactory;

    public function deliveries()
    {
        return $this->hasMany(Delivery::class);
    }
}
