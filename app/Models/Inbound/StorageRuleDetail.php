<?php

namespace App\Models\Inbound;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class StorageRuleDetail extends Model
{
    use HasFactory;

    protected $guarded = [];
}
