<?php

namespace App\Models\Inbound;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ProductTypeStorage extends Model
{
    use HasFactory;

    protected $guarded = [];
}
