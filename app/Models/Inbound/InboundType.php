<?php

namespace App\Models\Inbound;

use App\Models\Doors\Door;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;

class InboundType extends Model
{
    use HasFactory;

    public function doors()
    {
        return $this->belongsToMany(Door::class);
    }

    public function inServiceDoors()
    {
        return $this->hasMany(Door::class)->where('in_service',true);
    }
}
