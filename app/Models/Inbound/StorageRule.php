<?php

namespace app\Models\Inbound;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class StorageRule extends Model
{
    use HasFactory;

    protected $fillable = [
        'name',
        'description',
        'sequence'
    ];
}
