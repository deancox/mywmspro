<?php

namespace App\Models\Inbound;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class InboundConfig extends Model
{
    use HasFactory;

    protected $fillable =[
        'truck_required',
        'truck_required_by_supplier',
        'short_receive',
        'shorts_by_supplier',
        'over_receive',
        'over_receipt_by_supplier',
        'mobile_can_close',
        'mobile_can_dispatch'
    ];
}
