<?php

namespace App\Models\Holds;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class HoldType extends Model
{
    use HasFactory;

    protected $fillable = [
        'name',
        'description',
        'created_by',
    ];

    public function holds()
    {
        return $this->hasMany(Hold::class);
    }
}
