<?php

namespace App\Models\Holds;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Hold extends Model
{
    use HasFactory;

    public function hold()
    {
        return $this->belongsTo(HoldType::class);
    }
}
