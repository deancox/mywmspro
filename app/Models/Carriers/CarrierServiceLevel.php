<?php

namespace App\Models\Carriers;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class CarrierServiceLevel extends Model
{
    use HasFactory;
}
