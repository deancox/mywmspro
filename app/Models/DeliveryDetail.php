<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class DeliveryDetail extends Model
{
    use HasFactory;
    //use SoftDeletes;

    protected $guarded = [];

    public function delivery()
    {
        return $this->belongsTo(Delivery::class);
    }
}
