<?php

namespace App\Models\Allocation;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class AllocationRuleDetail extends Model
{
    use HasFactory;
}
