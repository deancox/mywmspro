<?php

namespace App\Models;

use App\Models\Inbound\Supplier;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Delivery extends Model
{
    use HasFactory;
    //use SoftDeletes;

    protected $guarded = [];

    public function deliveryDetails()
    {
        return $this->hasMany(DeliveryDetail::class);
    }

    public function supplier()
    {
        return $this->belongsTo(Supplier::class);
    }
}
