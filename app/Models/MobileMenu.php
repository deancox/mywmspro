<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class MobileMenu extends Model
{
    use HasFactory;

    protected $fillable = [
        'name',
        'visible',
        'description',
        'displayorder'
    ];

    public function toggleEnabled(Bool $value)
    {
        $this->update(['visible' => $value]);
    }
}
