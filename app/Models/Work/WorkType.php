<?php

namespace App\Models\Work;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class WorkType extends Model
{
    use HasFactory;

    protected $fillable = [
        'name',
        'description',
        'default_creation_status',
        'base_priority',
        'auto_increment',
        'increment_by',
        'increment_interval',
    ];

    public function hardware()
    {
        return $this->hasMany(WorkTypeHardware::class);
    }

    public function work()
    {
        return $this->belongsToMany(Work::class);
    }

    
}
