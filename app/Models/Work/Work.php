<?php

namespace App\Models\Work;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Work extends Model
{
   use HasFactory;

    public function type()
    {
        return $this->hasOne(WorkType::class);
    }

    public function status()
    {
        return $this->hasOne(WorkStatus::class);
    }
}
