<?php

namespace App\Models\Work;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class WorkTypeHardware extends Model
{
    use HasFactory;

    protected $fillable = [
        'work_type_id',
        'device_type_id'
    ];

}
