<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class Device extends Model
{
    use HasFactory;

    protected $fillable = [
        'device_type_id',
        'name',
        'created_by'
    ];

    /** @return BelongsTo  */
    public function deviceType()
    {
        return $this->belongsTo(DeviceType::class);
    }
}
