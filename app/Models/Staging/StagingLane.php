<?php

namespace App\Models\Staging;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class StagingLane extends Model
{
    use HasFactory;

    protected $fillable = [
        'name',
        'max_deliveries',
        'in_service',
        'tracked',
        'tracked_by',
        'maxqty',
        'default_label_printer',
        'default_laser_printer'
    ];
}
