<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\DB;

class Location extends Model
{
    use HasFactory;


    protected $fillable = [
        'location_name',
        'aisle',
        'level',
        'position',
        'height',
        'Width',
        'length',
        'maxweight',
        'tracked_by',
        'maxqty',
        'travel_sequence',
        'storage_sequence',
        'pick_sequence',
        'location_type',
        'storage_zone',
        'pick_zone',
        'count_zone',
        'is_pickable',
        'is_pickface',
        'is_usable',
        'is_storable',
        'pending_count',
        'has_problem',
        'is_temporary',
        'created_by',
        'updated_by',
        'location_type_id',
        'count_zone_id',
    ];

    public function scopeSearch($query, $term)
    {
        $term = "%$term%";
        $query->where(function ($query) use ($term) {
            $query->where('location_name', 'like', $term);
        });
    }

    public function locationType()
    {
        return $this->belongsTo(LocationType::class);
    }

    public function storageZone()
    {
        return $this->belongsTo(StorageZone::class);
    }

    public function countZone()
    {
        return $this->belongsTo(CountZone::class);
    }

    public function PickZone()
    {
        return $this->belongsTo(PickZone::class);
    }

    public function setPickable(Bool $value)
    {
        $this->update(['is_pickable' => $value]);
    }

    public function setUsable(Bool $value)
    {
        $this->update(['is_usable' => $value]);
    }

    public function setStorable(Bool $value)
    {
        $this->update(['is_storable' => $value]);
    }
}
