<?php

namespace App\Models\Doors;

use App\Models\Doors\DoorType;
use App\Models\Inbound\InboundType;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Door extends Model
{
    use HasFactory;

    protected $fillable =[
        'name',
        'door_type_id',
        'in_service',
        'max_trucks',
        'cur_truck_count',
        'is_yard',
        'def_stage_lane',
        'default_label_printer',
        'default_laser_printer'
    ];
    

    public function doorType()
    {
        return $this->belongsTo(DoorType::class);
    }


    public function inboundTypes()
    {
        return $this->belongsToMany(InboundType::class);
    }


    public function toggleInService(Bool $value)
    {
        $this->update(['in_service' => $value]);
    }
}
