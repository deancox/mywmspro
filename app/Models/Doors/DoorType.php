<?php

namespace App\Models\Doors;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class DoorType extends Model
{
    use HasFactory;

    public function doors()
    {
        return $this->hasMany(Door::class);
    }


}
