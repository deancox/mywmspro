<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Uom extends Model
{
    use HasFactory;

    public function uoms()
    {
        return $this->belongsToMany(ProductDetail::class);
    }

}
