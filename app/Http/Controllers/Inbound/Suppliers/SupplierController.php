<?php

namespace App\Http\Controllers\Inbound\Suppliers;

use App\Http\Controllers\Controller;
use App\Models\Inbound\Supplier;
use Illuminate\Http\Request;

class SupplierController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }


    public function create()
    {
        //
    }


    public function store(Request $request)
    {
        //
    }

 
    public function show(Supplier $supplier)
    {
        //
    }

    public function edit(Supplier $supplier)
    {
        //
    }

  
    public function update(Request $request, Supplier $supplier)
    {
        //
    }


    public function destroy(Supplier $supplier)
    {
        //
    }
}
