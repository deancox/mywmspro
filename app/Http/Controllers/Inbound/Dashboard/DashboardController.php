<?php

namespace App\Http\Controllers\Inbound\Dashboard;

use App\Http\Controllers\Controller;

class DashboardController extends Controller
{
    public function __invoke()
    {
        return view('inbound.dashboard');
    }
}
