<?php

namespace App\Http\Controllers\Inbound;

use App\Http\Controllers\Controller;
use App\Http\Requests\Inbound\StoreInboundCheckInRequest;
use App\Models\Delivery;
use App\Models\DeliveryDetail;
use App\Models\Inbound\Supplier;
use App\Services\Inbound\InboundCheckInService;


class CheckInController extends Controller
{

    private $checkInService;


    public function __construct()
    {
        $this->checkInService = new InboundCheckInService;
    }


    public function index(Delivery $delivery)
    {

        if (!$this->checkInService->validateCanCheckIn($delivery)) {

            session()->flash('error', ' This Delivery Requires a Trailer to be assigned prior to Check In.');

            return redirect()->back();
        }

        return view('inbound.check-in', [
            'delivery' => $delivery,
            'doors' => $this->checkInService->getDoorsForCheckin($delivery)
        ]);
    }


    public function store(StoreInboundCheckInRequest $request, Delivery $delivery)
    {

        $this->checkInService->checkInDelivery($request->validated(), $delivery);

        session()->flash('success', ' Delivery Checked In Succesfully.');

        return view('inbound.delivery-details', [
            'delivery' => $delivery,
            'supplier' => Supplier::where('id', $delivery->supplier_id),
            'deliveryDetails' => DeliveryDetail::where('delivery_id', $delivery->id)
        ]);
    }
}
