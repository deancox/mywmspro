<?php

namespace App\Http\Controllers\Inbound\Deliveries;

use App\Http\Controllers\Controller;
use App\Models\Delivery;
use App\Services\Inbound\InboundDeliveryService;
use App\Services\Inbound\InboundStagingService;
use Illuminate\Http\Request;

class DeliveryStagingController extends Controller
{

    private $stagingService;
    private $deliveryService;


    public function __construct()
    {
        $this->stagingService = new InboundStagingService;
        $this->deliveryService = new InboundDeliveryService;
    }


    public function store(Request $request)
    {

        $this->stagingService->assignDeliveryToLane($request->lane_id, $request->delivery_id);

        return view('inbound.receiving', [
            'delivery' => $this->deliveryService->getDeliveryDetails($request->delivery_id)
        ]);
    }


    public function show($delivery_id)
    {

        $lanes = $this->stagingService->getAvailableInboundLanes($delivery_id);
       
        return view('inbound.staging.inbound-lanes', [
            'lanes' => $lanes,
            'delivery' => Delivery::findOrFail($delivery_id)
        ]);
    }

}
