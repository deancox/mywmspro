<?php

namespace App\Http\Controllers\Inbound\Deliveries;

use App\Http\Controllers\Controller;
use App\Http\Requests\Inbound\CreateInboundDeliveryRequest;
use App\Models\Delivery;

use App\Services\Inbound\InboundDeliveryService;
use Illuminate\Http\Request;

use App\Models\DeliveryDetail;
use App\Models\Inbound\InboundType;
use App\Models\Inbound\Supplier;
use App\Models\Product;

class DeliveryController extends Controller
{

    private $deliveryService;

    public function __construct()
    {
        $this->deliveryService = new InboundDeliveryService;
    }


    public function index()
    {
        return view('inbound.deliveries-table', [
            'deliveries' => Delivery::withCount('deliveryDetails')
                ->orderBy('due_date', 'ASC')->paginate(10),
        ]);
    }


    public function create()
    {
        $suppliers = Supplier::all();
        $inboundtypes = InboundType::all();

        return view('inbound.create-delivery', [
            'suppliers' => $suppliers,
            'inboundtypes' => $inboundtypes,
        ]);
    }


    public function store(CreateInboundDeliveryRequest $request)
    {

        $delivery = $this->deliveryService->storeInboundDelivery($request->validated());

        return view('inbound.delivery-details', [
            'delivery' => $delivery,
            'supplier' => Supplier::where('id', $delivery->supplier_id),
            'deliveryDetails' => DeliveryDetail::where('dellivery_id', $delivery->id),
            'products' => Product::all()
        ]);
    }


    public function show(Delivery $delivery)
    {

        return view('inbound.delivery-details', [
            'delivery' => $delivery,
            'supplier' => Supplier::where('id', $delivery->supplier_id),
            'deliveryDetails' => DeliveryDetail::where('delivery_id', $delivery->id),
            'products' => Product::all()
        ]);
    }


    public function update(Request $request, Delivery $delivery)
    {
        //
    }


    public function destroy(Delivery $delivery)
    {
        $delivery->delete();

        return redirect()->route('inbound.deliveries.index')->with('success', ' Delivery deleted successfully!');
    }
}
