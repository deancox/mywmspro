<?php

namespace App\Http\Controllers\Inbound\Deliveries;

use App\Http\Controllers\Controller;
use App\Models\DeliveryDetail;
use App\Services\Inbound\InboundDeliveryService;
use Illuminate\Http\Request;


class DeliveryDetailController extends Controller
{

    private $inboundDeliveryService;


    public function __construct()
    {

        $this->inboundDeliveryService = new InboundDeliveryService;
    }


    public function store(Request $request)
    {

        $this->inboundDeliveryService->storeInboundDeliveryLine($request);

        return redirect()->route('inbound.deliveries.show', $request->delivery_id);
    }


    public function update(Request $request, DeliveryDetail $deliveryDetail)
    {
        //
    }


    public function destroy(DeliveryDetail $deliveryDetail)
    {
        //
    }
}
