<?php

namespace App\Http\Controllers\Inbound\Deliveries;

use App\Http\Controllers\Controller;
use App\Services\Inbound\InboundDeliveryService;
use App\Services\Inbound\InboundStagingService;
use App\Services\Inbound\ReceivingService;
use Illuminate\Http\Request;

class DeliveryReceiptController extends Controller
{

    private $receivingService;
    private $inboundStagingService;
    private $deliveryService;


    public function __construct()
    {
        $this->receivingService = new ReceivingService;
        $this->inboundStagingService = new InboundStagingService;
        $this->deliveryService = new InboundDeliveryService;
    }


    public function edit(Request $request)
    {

        return view('inbound.receive-line', [
            'deliveryDetail' => $this->receivingService->getLineDetailsForReceipt($request),
            'stagingLanes' => $this->inboundStagingService->getAvailableInboundLanes(),
        ]);
    }


    public function show($delivery_id)
    {

        return view('inbound.receiving', [
            'delivery' => $this->deliveryService->getDeliveryDetails($delivery_id)
        ]);
    }


    public function update(Request $request)
    {

        $deliveryDetail = $this->receivingService->receiveInventoryLine($request);

        return redirect()->route('inbound.receipt.show', $deliveryDetail->delivery_id);
    }
}
