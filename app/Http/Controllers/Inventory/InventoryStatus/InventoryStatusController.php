<?php

namespace App\Http\Controllers\Inventory\InventoryStatus;

use App\Http\Controllers\Controller;
use App\Http\Requests\InventoryStatus\CreateInventoryStatusRequest;
use App\Models\Inventory\InventoryStatus;
use App\Services\Inventory\InventoryStatusService;
use Illuminate\Http\Request;

class InventoryStatusController extends Controller
{

    private $inventoryStatusService;


    public function __construct()
    {
        $this->inventoryStatusService = new InventoryStatusService;
    }


    public function index()
    {
        return view('inventory.inventory-status-index',[
            'statuses' => InventoryStatus::paginate(10),
        ]);
    }
    

    public function store(CreateInventoryStatusRequest $request)
    {
        $this->inventoryStatusService->store($request->validated());

        return redirect()->route('inventory.inventoryStatus.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Inventory\InventoryStatus  $inventoryStatus
     * @return \Illuminate\Http\Response
     */
    public function show(InventoryStatus $inventoryStatus)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Inventory\InventoryStatus  $inventoryStatus
     * @return \Illuminate\Http\Response
     */
    public function edit(InventoryStatus $inventoryStatus)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Inventory\InventoryStatus  $inventoryStatus
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, InventoryStatus $inventoryStatus)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Inventory\InventoryStatus  $inventoryStatus
     * @return \Illuminate\Http\Response
     */
    public function destroy(InventoryStatus $inventoryStatus)
    {
        //
    }
}
