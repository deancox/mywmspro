<?php

namespace App\Http\Controllers\Inventory\Dashboard;

use App\Http\Controllers\Controller;

class DashboardController extends Controller
{

    public function __invoke()
    {
        return view('inventory.dashboard');
    }
}
