<?php

namespace App\Http\Controllers\Inventory\Holds;

use App\Http\Controllers\Controller;
use App\Http\Requests\Holds\CreateHoldTypeRequest;
use App\Models\Holds\HoldType;
use App\Services\Holds\HoldTypeService;
use Illuminate\Http\Request;

class HoldTypeController extends Controller
{

    private $holdTypeService;

    public function __construct()
    {
        $this->holdTypeService = new HoldTypeService;
    }

    public function index()
    {
        return view('inventory.holds.hold-types', [
            'holdTypes' => HoldType::withCount('holds')->paginate(15),
        ]);
    }

    public function store(CreateHoldTypeRequest $request)
    {
        $this->holdTypeService->store($request->validated());

        return redirect()->route('inventory.holdTypes.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Holds\HoldType  $holdType
     * @return \Illuminate\Http\Response
     */
    public function show(HoldType $holdType)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Holds\HoldType  $holdType
     * @return \Illuminate\Http\Response
     */
    public function edit(HoldType $holdType)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Holds\HoldType  $holdType
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, HoldType $holdType)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Holds\HoldType  $holdType
     * @return \Illuminate\Http\Response
     */
    public function destroy(HoldType $holdType)
    {
        //
    }
}
