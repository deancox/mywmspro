<?php

namespace App\Http\Controllers\Inventory\Holds;

use App\Http\Controllers\Controller;
use App\Models\Hold;
use Illuminate\Http\Request;

class HoldController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Hold  $hold
     * @return \Illuminate\Http\Response
     */
    public function show(Hold $hold)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Hold  $hold
     * @return \Illuminate\Http\Response
     */
    public function edit(Hold $hold)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Hold  $hold
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Hold $hold)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Hold  $hold
     * @return \Illuminate\Http\Response
     */
    public function destroy(Hold $hold)
    {
        //
    }
}
