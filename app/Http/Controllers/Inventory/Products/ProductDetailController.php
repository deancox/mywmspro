<?php

namespace App\Http\Controllers\Inventory\Products;

use App\Http\Controllers\Controller;
use App\Models\Product;
use App\Models\ProductDetail;
use App\Services\Products\ProductService;
use Illuminate\Http\Request;

class ProductDetailController extends Controller
{

    private $productService;


    public function __construct()
    {

        $this->productService = new ProductService;
    }


    public function index()
    {

        $productDetails = ProductDetail::with('product', 'uom')
            ->orderBy('product_id')
            ->paginate(20);

        return view('inventory.product-details-index', [
            'productDetails' => $productDetails
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    public function show(ProductDetail $productDetail)
    {
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\ProductDetails  $productDetails
     * @return \Illuminate\Http\Response
     */
    public function edit(ProductDetail $productDetails)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\ProductDetails  $productDetails
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, ProductDetail $productDetails)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\ProductDetails  $productDetails
     * @return \Illuminate\Http\Response
     */
    public function destroy(ProductDetail $productDetails)
    {
        //
    }

    public function ajax(Product $product)
    {

        return $this->productService->getProductUoms($product->id);
    }
}
