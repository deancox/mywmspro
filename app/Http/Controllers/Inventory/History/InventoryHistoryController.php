<?php

namespace App\Http\Controllers\Inventory\History;

use App\Http\Controllers\Controller;
use App\Models\Inventory\InventoryHistory;
use Illuminate\Http\Request;

class InventoryHistoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Inventory\InventoryHistory  $inventoryHistory
     * @return \Illuminate\Http\Response
     */
    public function show(InventoryHistory $inventoryHistory)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Inventory\InventoryHistory  $inventoryHistory
     * @return \Illuminate\Http\Response
     */
    public function edit(InventoryHistory $inventoryHistory)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Inventory\InventoryHistory  $inventoryHistory
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, InventoryHistory $inventoryHistory)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Inventory\InventoryHistory  $inventoryHistory
     * @return \Illuminate\Http\Response
     */
    public function destroy(InventoryHistory $inventoryHistory)
    {
        //
    }
}
