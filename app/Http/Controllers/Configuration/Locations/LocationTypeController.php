<?php

namespace App\Http\Controllers\Configuration\Locations;

use App\Http\Controllers\Controller;
use App\Http\Requests\CreateLocationTypeRequest;
use App\Models\LocationType;
use App\Services\Locations\LocationService;
use App\Services\Locations\LocationTypeService;
use Illuminate\Http\RedirectResponse;
use Illuminate\Contracts\Container\BindingResolutionException;
use Illuminate\Contracts\View\View;
use Illuminate\Contracts\View\Factory;
use Illuminate\Http\Request;
use Illuminate\Validation\ValidationException;
use Symfony\Component\Routing\Exception\RouteNotFoundException;

class LocationTypeController extends Controller
{

    /** @return void  */
    public function __construct()
    {
        $this->locationTypeService = new LocationTypeService;
        $this->locationService = new LocationService;
    }

    /**
     * @return View|Factory 
     * @throws BindingResolutionException 
     */
    public function index()
    {

        return view('configuration.locations.location-types', [
            'locationTypes' => LocationType::withCount('locations')->paginate(20),
        ]);
    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }


    /**
     * @param CreateLocationTypeRequest $request 
     * @return RedirectResponse 
     * @throws ValidationException 
     * @throws BindingResolutionException 
     * @throws RouteNotFoundException 
     */
    public function store(CreateLocationTypeRequest $request)
    {
        $this->locationTypeService->store($request->validated());

        return redirect()->route('config.locationTypes.index');
    }


    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }


    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }


    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
    

    public function showLocations(LocationType $locationType)
    {
        return view('inventory.location-table', [
            'locations' => $this->locationService->findByLocationType($locationType),
        ]);
    }
}
