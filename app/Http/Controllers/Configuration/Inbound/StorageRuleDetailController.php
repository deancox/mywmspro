<?php

namespace app\Http\Controllers;

use App\Models\Inbound\StorageRuleDetail;
use Illuminate\Http\Request;

class StorageRuleDetailController extends Controller
{
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\StorageRuleDetail  $storageRuleDetail
     * @return \Illuminate\Http\Response
     */
    public function show(StorageRuleDetail $storageRuleDetail)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\StorageRuleDetail  $storageRuleDetail
     * @return \Illuminate\Http\Response
     */
    public function edit(StorageRuleDetail $storageRuleDetail)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\StorageRuleDetail  $storageRuleDetail
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, StorageRuleDetail $storageRuleDetail)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\StorageRuleDetail  $storageRuleDetail
     * @return \Illuminate\Http\Response
     */
    public function destroy(StorageRuleDetail $storageRuleDetail)
    {
        //
    }
}
