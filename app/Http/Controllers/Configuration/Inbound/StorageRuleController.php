<?php

namespace App\Http\Controllers\Configuration\Inbound;

use App\Http\Controllers\Controller;
use App\Models\Inbound\StorageRule;
use App\Models\Products\ProductType;
use App\Models\Uom;
use App\Models\Zones\StorageZone;
use App\Services\StorageRules\StorageRuleDetailService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class StorageRuleController extends Controller
{

    private $ruleDetailService;
    

    public function __construct()
    {
        $this->ruleDetailService = new StorageRuleDetailService;
    }


    public function index()
    {
        $storageRules = StorageRule::all();
        return view('configuration.inbound.storage-rules', [
            'storageRules' => $storageRules
        ]);
    }


    public function create()
    {
        return view('configuration.inbound.create-storage-rule', [
            'storageZones' => StorageZone::all(),
            'productTypes' => ProductType::all(),
            'uoms' => Uom::all()
        ]);
    }


    public function store(Request $request)
    {
      
        if(!$request->has('storageZones')){
            session()->flash('error', ' At least one Storage Zone is required');
            return back()->withInput();
        };

        DB::transaction(function () use ($request) {
            $storageRule = StorageRule::create([
                'name' => $request->name,
                'description' => $request->description,
                'sequence' => $request->sequence
            ]);

            $this->ruleDetailService->createStorageRuleDetail($request, $storageRule->id);
        });


        return redirect()->route('config.storageRules.index');
    }

 
    public function show(StorageRule $storageRule)
    {
        //
    }


    public function edit(StorageRule $storageRule)
    {
        //
    }


    public function update(Request $request, StorageRule $storageRule)
    {
        //
    }


    public function destroy(StorageRule $storageRule)
    {
        //
    }
}
