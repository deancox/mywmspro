<?php

namespace App\Http\Controllers\Configuration\Inbound;

use App\Http\Controllers\Controller;
use App\Models\Inbound\InboundConfig;
use Illuminate\Http\Request;

class InboundConfigController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
     
        return view('configuration.inbound.inbound-rules', [
            'rules' => InboundConfig::first()
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Inbound\InboundConfig  $inboundConfig
     * @return \Illuminate\Http\Response
     */
    public function show(InboundConfig $inboundConfig)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Inbound\InboundConfig  $inboundConfig
     * @return \Illuminate\Http\Response
     */
    public function edit(InboundConfig $inboundConfig)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Inbound\InboundConfig  $inboundConfig
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, InboundConfig $inboundConfig)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Inbound\InboundConfig  $inboundConfig
     * @return \Illuminate\Http\Response
     */
    public function destroy(InboundConfig $inboundConfig)
    {
        //
    }
}
