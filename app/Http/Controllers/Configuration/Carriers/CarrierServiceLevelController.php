<?php

namespace App\Http\Controllers\Configuration\Carriers;

use App\Http\Controllers\Controller;

use App\Http\Requests\Carriers\StoreCarrierServiceLevelRequest;
use App\Http\Requests\Carriers\UpdateCarrierServiceLevelRequest;

use App\Models\Carriers\CarrierServiceLevel;


class CarrierServiceLevelController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\Requests\StoreCarrierServiceLevelRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreCarrierServiceLevelRequest $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Carriers\CarrierServiceLevel  $carrierServiceLevel
     * @return \Illuminate\Http\Response
     */
    public function show(CarrierServiceLevel $carrierServiceLevel)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Carriers\CarrierServiceLevel  $carrierServiceLevel
     * @return \Illuminate\Http\Response
     */
    public function edit(CarrierServiceLevel $carrierServiceLevel)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Http\Requests\UpdateCarrierServiceLevelRequest  $request
     * @param  \App\Models\Carriers\CarrierServiceLevel  $carrierServiceLevel
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateCarrierServiceLevelRequest $request, CarrierServiceLevel $carrierServiceLevel)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Carriers\CarrierServiceLevel  $carrierServiceLevel
     * @return \Illuminate\Http\Response
     */
    public function destroy(CarrierServiceLevel $carrierServiceLevel)
    {
        //
    }
}
