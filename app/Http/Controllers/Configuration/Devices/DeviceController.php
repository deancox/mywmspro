<?php

namespace App\Http\Controllers\Configuration\Devices;

use App\Http\Controllers\Controller;
use App\Http\Requests\CreateDeviceRequest;
use App\Models\Device;
use App\Models\DeviceType;
use App\Services\MobileDevices\MobileDeviceService;
use Illuminate\Contracts\View\View;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\Container\BindingResolutionException;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use InvalidArgumentException;
use Symfony\Component\Routing\Exception\RouteNotFoundException;

class DeviceController extends Controller
{

    /** @return void  */
    public function __construct()
    {
        
        $this->deviceService = new MobileDeviceService;
    }


    /**
     * @return View|Factory 
     * @throws InvalidArgumentException 
     * @throws BindingResolutionException 
     */
    public function index()
    {
        return view('configuration.devices.devices', [
            'devices' => Device::with('deviceType')->paginate(20),
            'deviceTypes' => DeviceType::all()
        ]);
    }

    /**
     * @param CreateDeviceRequest $request 
     * @return RedirectResponse 
     * @throws RouteNotFoundException 
     */
    public function store(CreateDeviceRequest $request)
    {

        $this->deviceService->store($request->validated());

        return redirect()->route('config.devices.index');

    }

    public function show($id)
    {
        //
    }

    public function edit($id)
    {
        //
    }

    public function update(Request $request, $id)
    {
        //
    }

    public function destroy($id)
    {
        //
    }
}
