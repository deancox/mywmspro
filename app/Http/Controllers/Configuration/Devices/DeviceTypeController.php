<?php

namespace App\Http\Controllers\Configuration\Devices;

use App\Http\Controllers\Controller;
use App\Http\Requests\CreateDeviceTypeRequest;
use App\Services\MobileDevices\DeviceTypeService;

class DeviceTypeController extends Controller
{

    private $deviceTypeService;

    public function __construct()
    {

        $this->deviceTypeService = new DeviceTypeService;
    }
    

    public function index()
    {

        return view('configuration.devices.device-types', [
            'deviceTypes' => $this->deviceTypeService->getDeviceTypesWithDeviceCount(),
        ]);
    }


    public function store(CreateDeviceTypeRequest $request)
    {

        $this->deviceTypeService->store($request->validated());

        return redirect()->route('config.deviceTypes.index');
    }

    public function destroy($id)
    {
        //
    }
}
