<?php

namespace App\Http\Controllers\Configuration\Uploads;

use App\Http\Controllers\Controller;
use App\Services\Configuration\LocationUploadService;
use App\Services\Locations\SyncLocationIds;
use Illuminate\Contracts\View\View;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\Container\BindingResolutionException;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;

class ConfigUploadController extends Controller
{

    /**
     * @return View|Factory 
     * @throws BindingResolutionException 
     */
    public function index()
    {

        return view('configuration.config-upload');
    }


    /**
     * @param Request $request 
     * @return RedirectResponse 
     */
    public function store(Request $request)
    {
        
        if ($request->hasFile('location_upload')) {
            $count = (new LocationUploadService())->upload($request);
        };

        (new SyncLocationIds())->syncNow();

        return redirect()->back()->with('success', $count . ' Locations uploaded successfully!');
    }

}
