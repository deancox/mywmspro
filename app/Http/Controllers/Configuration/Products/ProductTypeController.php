<?php

namespace App\Http\Controllers\Configuration\Products;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\Products\CreateProductTypeRequest;
use App\Services\Products\ProductTypeService;
use Illuminate\Support\Facades\DB;

class ProductTypeController extends Controller
{

    private $productTypeService;

    public function __construct()
    {
        $this->productTypeService = new ProductTypeService;
    }


    public function index()
    {
        return view('configuration.products.product-types', [
            'productTypes' => DB::table('product_types')->paginate(15),
        ]);
    }
    

    public function store(CreateProductTypeRequest $request)
    {
        $this->productTypeService->store($request->validated());

        return redirect()->route('config.productTypes.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
