<?php

namespace App\Http\Controllers\Configuration\Products;

use App\Http\Controllers\Controller;
use App\Models\ProductDetail;
use App\Models\Uom;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class UOMController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //put this in a service! 
        $uoms = DB::table('product_details')
            ->join('uoms', 'product_details.uom', 'uoms.name')
            ->select(DB::raw('count(*) as productsUsing,
         uom,
         uoms.name,
         uoms.description,
         uoms.created_at,
         uoms.updated_at,
         uoms.created_by'))
            ->groupBy('product_details.uom')
            ->groupBy('uoms.created_at')
            ->groupBy('uoms.updated_at')
            ->groupBy('uoms.created_by')
            ->groupBy('uoms.name')
            ->groupBy('uoms.description')
            ->get();

        return view('configuration.inventory.uom-list', [
            'uoms' => $uoms,
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\UOM  $uOM
     * @return \Illuminate\Http\Response
     */
    public function show(UOM $uOM)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\UOM  $uOM
     * @return \Illuminate\Http\Response
     */
    public function edit(UOM $uOM)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\UOM  $uOM
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, UOM $uOM)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\UOM  $uOM
     * @return \Illuminate\Http\Response
     */
    public function destroy(UOM $uOM)
    {
        //
    }
}
