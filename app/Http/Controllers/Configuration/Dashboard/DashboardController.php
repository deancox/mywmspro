<?php

namespace App\Http\Controllers\Configuration\Dashboard;

use App\Http\Controllers\Controller;

use App\Models\Zones\CountZone;
use App\Models\Device;
use App\Models\Location;
use App\Models\LocationType;
use App\Models\Zones\PickZone;
use App\Models\Zones\StorageZone;
use App\Models\User;

class DashboardController extends Controller
{

    public function __invoke()
    {

        $users = User::count();
        $storageZones = StorageZone::count();
        $countZones = CountZone::count();
        $pickZones = PickZone::count();
        $locationTypes = LocationType::count();
        $locations = Location::count();
        $devices = Device::count();

        
        return view('configuration.dashboard', [
            'users' => $users,
            'storageZones' => $storageZones,
            'countZones' => $countZones,
            'pickZones' => $pickZones,
            'locationTypes' => $locationTypes,
            'locations' => $locations,
            'devices' => $devices,
        ]);

    }
}
