<?php

namespace App\Http\Controllers\Configuration\Hardware;

use App\Http\Controllers\Controller;
use App\Models\Printer;
use App\Models\PrinterType;
use Illuminate\Http\Request;

class PrinterController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        return view('configuration.hardware.printers', [
            'printers' => Printer::with('printerType')->paginate(20),
            'printerTypes' => PrinterType::all()
        ]);

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
       
        Printer::create([
            'name' => $request->name,
            'printer_type_id' => $request->printer_type_id,
            'ip' => $request->ip
        ]);

        return redirect()->route('config.printers.index');

    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Printer  $printer
     * @return \Illuminate\Http\Response
     */
    public function show(Printer $printer)
    {

        dd($printer, 'show');
        
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Printer  $printer
     * @return \Illuminate\Http\Response
     */
    public function edit(Printer $printer)
    {
        dd($printer, 'edit');
    }


    public function update(Request $request)
    {

        $printer = Printer::select('id', 'enabled')
        ->where('id', $request->printer)
        ->first();

        $printer->enabled ? $printer->toggleEnabled(0) : $printer->toggleEnabled(1);

    }


    public function destroy(Printer $printer)
    {
        //
    }
}
