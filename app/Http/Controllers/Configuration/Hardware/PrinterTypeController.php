<?php

namespace App\Http\Controllers\Configuration\Hardware;

use App\Http\Controllers\Controller;
use App\Models\PrinterType;
use Illuminate\Http\Request;

class PrinterTypeController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        return view('configuration.hardware.printer-types', [
            'printerTypes' => PrinterType::withCount('printers')->get()
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
       //TODO expand on this later maybe remove altogether..

       //dd(auth()->user()->name);
        PrinterType::create([
            'type' => $request->type,
            'created_by' => auth()->user()->name
        ]);

        return redirect()->route('config.printerTypes.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\PrinterType  $printerType
     * @return \Illuminate\Http\Response
     */
    public function show(PrinterType $printerType)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\PrinterType  $printerType
     * @return \Illuminate\Http\Response
     */
    public function edit(PrinterType $printerType)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\PrinterType  $printerType
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, PrinterType $printerType)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\PrinterType  $printerType
     * @return \Illuminate\Http\Response
     */
    public function destroy(PrinterType $printerType)
    {
        //
    }
}
