<?php

namespace App\Http\Controllers\Configuration\Work;

use App\Http\Controllers\Controller;
use App\Models\Work\WorkType;
use App\Http\Requests\Work\StoreWorkTypeRequest;
use App\Http\Requests\Work\UpdateWorkTypeRequest;
use App\Models\Work\WorkStatus;
use App\Services\Work\WorkTypeService;

class WorkTypeController extends Controller
{
 
    private $workTypeService;
    

    public function __construct()
    {
        $this->workTypeService = new WorkTypeService;
    }


    public function index()
    {
        return view('configuration.work.work-types-index', [
            'workTypes' => WorkType::all(),
            'workStatuses' => WorkStatus::all()
        ]);
    }


    public function store(StoreWorkTypeRequest $request)
    {
        
        $this->workTypeService->storeNewWorkType($request->validated() + [
            'auto_increment' => $request->auto_increment ? true : false,
        ]);

        return redirect()->route('config.workTypes.index');
    }

    public function show(WorkType $workType)
    {
        //
    }

    public function edit(WorkType $workType)
    {
        //
    }

    public function update(UpdateWorkTypeRequest $request, WorkType $workType)
    {
        //
    }

    public function destroy(WorkType $workType)
    {
        //
    }
}
