<?php

namespace app\Http\Controllers\Work;

use App\Models\WorkStatus;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class WorkStatusController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\WorkStatus  $workStatus
     * @return \Illuminate\Http\Response
     */
    public function show(WorkStatus $workStatus)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\WorkStatus  $workStatus
     * @return \Illuminate\Http\Response
     */
    public function edit(WorkStatus $workStatus)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\WorkStatus  $workStatus
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, WorkStatus $workStatus)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\WorkStatus  $workStatus
     * @return \Illuminate\Http\Response
     */
    public function destroy(WorkStatus $workStatus)
    {
        //
    }
}
