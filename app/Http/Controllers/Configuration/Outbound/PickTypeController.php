<?php

namespace App\Http\Controllers\Configuration\Outbound;

use App\Http\Controllers\Controller;
use App\Models\PickType;
use Illuminate\Http\Request;


class PickTypeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('configuration.outbound.pick-types-index', [
            'pickTypes' => PickType::all()->sortBy('category')
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\PickType  $pickType
     * @return \Illuminate\Http\Response
     */
    public function show(PickType $pickType)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\PickType  $pickType
     * @return \Illuminate\Http\Response
     */
    public function edit(PickType $pickType)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\PickType  $pickType
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, PickType $pickType)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\PickType  $pickType
     * @return \Illuminate\Http\Response
     */
    public function destroy(PickType $pickType)
    {
        //
    }
}
