<?php

namespace App\Http\Controllers\Configuration\Zones;

use App\Http\Controllers\Controller;
use App\Http\Requests\Zones\CreateCountZoneRequest;
use App\Services\Zones\CountZoneService;
use Illuminate\Contracts\View\View;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\Container\BindingResolutionException;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Validation\ValidationException;
use Symfony\Component\Routing\Exception\RouteNotFoundException;

class CountZoneController extends Controller
{

    /** @return void  */
    public function __construct()
    {
        $this->countZoneService = new CountZoneService;
    }

    /**
     * @return View|Factory 
     * @throws BindingResolutionException 
     */
    public function index()
    {
        return view('configuration.zones.count-zones', [
            'countZones' => $this->countZoneService->getCountZonesWithLocationCount(),
        ]);
    }

    /**
     * @param CreateCountZoneRequest $request 
     * @return RedirectResponse 
     * @throws ValidationException 
     * @throws BindingResolutionException 
     * @throws RouteNotFoundException 
     */
    public function store(CreateCountZoneRequest $request)
    {
        $this->countZoneService->store($request->validated());

        return redirect()->route('config.countZones.index');
    }

    public function show($id)
    {
        //
    }

    public function edit($id)
    {
        //
    }

    public function update(Request $request, $id)
    {
        //
    }

    public function destroy($id)
    {
        //
    }
}
