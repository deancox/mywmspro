<?php

namespace App\Http\Controllers\Configuration\Zones;

use App\Http\Controllers\Controller;
use App\Http\Requests\Zones\CreatePickZoneRequest;
use App\Services\Zones\PickZoneService;
use Illuminate\Contracts\View\View;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\Container\BindingResolutionException;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Validation\ValidationException;
use Symfony\Component\Routing\Exception\RouteNotFoundException;

class PickZoneController extends Controller
{

    /** @return void  */
    public function __construct()
    {
        $this->pickZoneService = new PickZoneService;
    }
 
    /**
     * @return View|Factory 
     * @throws BindingResolutionException 
     */
    public function index()
    {
        return view('configuration.zones.pick-zones', [
            'pickZones' => $this->pickZoneService->getPickZonesWithLocationCount(),
        ]);
    }

    /**
     * @param CreatePickZoneRequest $request 
     * @return RedirectResponse 
     * @throws ValidationException 
     * @throws BindingResolutionException 
     * @throws RouteNotFoundException 
     */
    public function store(CreatePickZoneRequest $request)
    {
        $this->pickZoneService->store($request->validated());

        return redirect()->route('config.pickZones.index');
    }

    public function show($id)
    {
        //
    }

     public function edit($id)
    {
        //
    }

    public function update(Request $request, $id)
    {
        //
    }

    public function destroy($id)
    {
        //
    }
}
