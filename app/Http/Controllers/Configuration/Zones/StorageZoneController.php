<?php

namespace App\Http\Controllers\Configuration\Zones;

use App\Http\Controllers\Controller;
use App\Http\Requests\Zones\CreateStorageZoneRequest;
use App\Services\Zones\StorageZoneService;
use Illuminate\Contracts\View\View;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\Container\BindingResolutionException;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Validation\ValidationException;
use Symfony\Component\Routing\Exception\RouteNotFoundException;

class StorageZoneController extends Controller
{

    /** @return void  */
    public function __construct()
    {
        $this->storageZoneService = new StorageZoneService;
    }
    

    /**
     * @return View|Factory 
     * @throws BindingResolutionException 
     */
    public function index()
    {
        return view('configuration.zones.storage-zones', [
            'storageZones' => $this->storageZoneService->getStorageZonesWithLocationCount(),
        ]);
    }


    /**
     * @param CreateStorageZoneRequest $request 
     * @return RedirectResponse 
     * @throws ValidationException 
     * @throws BindingResolutionException 
     * @throws RouteNotFoundException 
     */
    public function store(CreateStorageZoneRequest $request)
    {
        $this->storageZoneService->store($request->validated());

        return redirect()->route('config.storageZones.index');
    }

    public function show($id)
    {
        //
    }

    public function edit($id)
    {
        
    }

    public function update(Request $request, $id)
    {
        dd($request, 'update');
    }

    public function destroy($id)
    {
        //
    }
}
