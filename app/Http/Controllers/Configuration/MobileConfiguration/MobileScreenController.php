<?php

namespace app\Http\Controllers\ConfigurationMobileConfiguration;

use App\Http\Controllers\Controller;
use App\Models\MobileScreen;
use Illuminate\Http\Request;

class MobileScreenController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\MobileScreen  $mobileScreen
     * @return \Illuminate\Http\Response
     */
    public function show(MobileScreen $mobileScreen)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\MobileScreen  $mobileScreen
     * @return \Illuminate\Http\Response
     */
    public function edit(MobileScreen $mobileScreen)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\MobileScreen  $mobileScreen
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, MobileScreen $mobileScreen)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\MobileScreen  $mobileScreen
     * @return \Illuminate\Http\Response
     */
    public function destroy(MobileScreen $mobileScreen)
    {
        //
    }
}
