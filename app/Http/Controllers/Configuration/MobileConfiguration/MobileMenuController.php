<?php

namespace App\Http\Controllers\Configuration\MobileConfiguration;

use App\Http\Controllers\Controller;
use App\Models\MobileMenu;
use App\Services\MobileMenu\MobileMenuService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class MobileMenuController extends Controller
{

    private $mobileMenuService;

    public function __construct()
    {

        $this->mobileMenuService = new MobileMenuService;
    }


    public function index()
    {

        return view('MobileConfiguration.mobile-display-options', [
            'menus' => $this->mobileMenuService->getMainMenu()
        ]);
    }


    public function update(Request $request)
    {

        $menu = MobileMenu::select('id', 'visible')
            ->where('id', $request->mobileMenu)
            ->where('screen', $request->screen)
            ->first();

        $menu->visible ? $menu->toggleEnabled(0) : $menu->toggleEnabled(1);
    }


    public function updateSortOrder(Request $request)
    {

        $this->mobileMenuService->updateSortOrder($request);
    }
}
