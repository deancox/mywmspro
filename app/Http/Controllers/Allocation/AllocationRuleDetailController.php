<?php

namespace App\Http\Controllers\Allocation;

use App\Http\Controllers\Controller;
use App\Models\Allocation\AllocationRuleDetail;
use Illuminate\Http\Request;

class AllocationRuleDetailController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\AllocationRuleDetail  $allocationRuleDetail
     * @return \Illuminate\Http\Response
     */
    public function show(AllocationRuleDetail $allocationRuleDetail)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\AllocationRuleDetail  $allocationRuleDetail
     * @return \Illuminate\Http\Response
     */
    public function edit(AllocationRuleDetail $allocationRuleDetail)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\AllocationRuleDetail  $allocationRuleDetail
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, AllocationRuleDetail $allocationRuleDetail)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\AllocationRuleDetail  $allocationRuleDetail
     * @return \Illuminate\Http\Response
     */
    public function destroy(AllocationRuleDetail $allocationRuleDetail)
    {
        //
    }
}
