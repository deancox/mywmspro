<?php

namespace App\Http\Controllers\Allocation;

use App\Http\Controllers\Controller;
use App\Http\Requests\Allocation\StoreAllocationRulesRequest;
use App\Http\Requests\Allocation\UpdateAllocationRulesRequest;
use App\Models\Allocation\AllocationRules;

class AllocationRulesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\Requests\StoreAllocationRulesRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreAllocationRulesRequest $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\AllocationRules  $allocationRules
     * @return \Illuminate\Http\Response
     */
    public function show(AllocationRules $allocationRules)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\AllocationRules  $allocationRules
     * @return \Illuminate\Http\Response
     */
    public function edit(AllocationRules $allocationRules)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Http\Requests\UpdateAllocationRulesRequest  $request
     * @param  \App\Models\AllocationRules  $allocationRules
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateAllocationRulesRequest $request, AllocationRules $allocationRules)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\AllocationRules  $allocationRules
     * @return \Illuminate\Http\Response
     */
    public function destroy(AllocationRules $allocationRules)
    {
        //
    }
}
