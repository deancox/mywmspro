<?php

namespace App\Http\Controllers\StagingLanes;

use App\Http\Controllers\Controller;
use App\Models\Staging\StagingLane;
use App\Services\Inbound\InboundStagingService;
use Illuminate\Http\Request;

class InboundStagingLaneController extends Controller
{

    private $inboundStagingService;

    public function __construct()
    {
        $this->inboundStagingService = new InboundStagingService;
    }

    
    public function index()
    {
        return view('staging.staging-lanes', [
            'lanes' => StagingLane::paginate(10),

        ]);
    }


    public function store(Request $request)
    {
        $this->inboundStagingService->StoreNewStagingLane($request);

        return redirect()->route('stagingLanes.index');
    }
}
