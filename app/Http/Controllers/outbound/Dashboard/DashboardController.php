<?php

namespace App\Http\Controllers\Outbound\Dashboard;

use App\Http\Controllers\Controller;


class DashboardController extends Controller
{
    public function __invoke()
    {
        return view('outbound.dashboard');
    }
}
