<?php

namespace App\Http\Controllers\Outbound\Picking;

use App\Http\Controllers\Controller;
use App\Models\PickTypeDetail;
use Illuminate\Http\Request;


class PickTypeDetailController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\PickTypeDetail  $pickTypeDetail
     * @return \Illuminate\Http\Response
     */
    public function show(PickTypeDetail $pickTypeDetail)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\PickTypeDetail  $pickTypeDetail
     * @return \Illuminate\Http\Response
     */
    public function edit(PickTypeDetail $pickTypeDetail)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\PickTypeDetail  $pickTypeDetail
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, PickTypeDetail $pickTypeDetail)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\PickTypeDetail  $pickTypeDetail
     * @return \Illuminate\Http\Response
     */
    public function destroy(PickTypeDetail $pickTypeDetail)
    {
        //
    }
}
