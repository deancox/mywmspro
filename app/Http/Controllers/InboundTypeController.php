<?php

namespace App\Http\Controllers;

use App\Models\Inbound\InboundType;
use App\Http\Requests\StoreInboundTypeRequest;
use App\Http\Requests\UpdateInboundTypeRequest;

class InboundTypeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\Requests\StoreInboundTypeRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreInboundTypeRequest $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Inbound\InboundType  $inboundType
     * @return \Illuminate\Http\Response
     */
    public function show(InboundType $inboundType)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Inbound\InboundType  $inboundType
     * @return \Illuminate\Http\Response
     */
    public function edit(InboundType $inboundType)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Http\Requests\UpdateInboundTypeRequest  $request
     * @param  \App\Models\Inbound\InboundType  $inboundType
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateInboundTypeRequest $request, InboundType $inboundType)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Inbound\InboundType  $inboundType
     * @return \Illuminate\Http\Response
     */
    public function destroy(InboundType $inboundType)
    {
        //
    }
}
