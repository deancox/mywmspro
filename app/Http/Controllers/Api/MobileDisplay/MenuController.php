<?php

namespace App\Http\Controllers\Api\MobileDisplay;

use App\Http\Controllers\Controller;
use App\Http\Resources\MenuResource;
use App\Models\MobileMenu;
use Illuminate\Http\Request;

class MenuController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return MenuResource::collection(MobileMenu::select('description')
            ->where('visible', true)
            ->orderBy('displayorder')
            ->get());
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\MobileMenu  $mobileMenu
     * @return \Illuminate\Http\Response
     */
    public function show(MobileMenu $mobileMenu)
    {
        //
    }
}
