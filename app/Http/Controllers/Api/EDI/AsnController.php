<?php

namespace App\Http\Controllers\Api\EDI;

use App\Http\Controllers\Controller;

class AsnController extends Controller
{
    public function index()
    {

        //---for SAP EDI_DC40 is always the idoc type for external comms---

        //on file receipt headers/channel?
        $file = 'ASN_OPEN.xml';

        $xml = simplexml_load_file(
            storage_path('app\edi_in\\' . $file)
        );

        foreach ($xml->IDOC->EDI_DC40 as $key => $value) {
            $EDI_DC40 = [$key => $value];
        }

    
        //on file receipt in headers/channel driven?
        $message_type = 'E1SHP_IBDLV_SAVE_REPLICA';

        foreach ($xml->IDOC->$message_type as $key => $value) {
            $message_detail = [$key => $value];
        }

        $new = [
            'docnum' => strval($EDI_DC40['EDI_DC40']->DOCNUM),
            'direct' => strval($EDI_DC40['EDI_DC40']->DIRECT),
            'idoctyp' => strval($EDI_DC40['EDI_DC40']->IDOCTYP),
            'mestyp' => strval($EDI_DC40['EDI_DC40']->MESTYP),
            'sndpor' => strval($EDI_DC40['EDI_DC40']->SNDPOR),
            'sndprt' => strval($EDI_DC40['EDI_DC40']->SNDPRT),
            'sndprn' => strval($EDI_DC40['EDI_DC40']->SNDPRN),
            'rcvpor' => strval($EDI_DC40['EDI_DC40']->RCVPOR),
            'rcvprt' => strval($EDI_DC40['EDI_DC40']->RCVPRT),
            'rcvprn' => strval($EDI_DC40['EDI_DC40']->RCVPRN),
            'credat' => strval($EDI_DC40['EDI_DC40']->CREDAT),
            'cretim' => strval($EDI_DC40['EDI_DC40']->CRETIM),
        ];

        dd($EDI_DC40, $message_detail,$new, $EDI_DC40['EDI_DC40']->MESTYP, $xml, $xml->IDOC->EDI_DC40->DOCNUM);
        //print_r(json_encode($xml));
        //print_r($xml->MATERIAL);
        //dd($xml);
    }

    public function show($request)
    {
    }
}
