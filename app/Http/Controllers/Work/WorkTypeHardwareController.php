<?php

namespace App\Http\Controllers\Work;

use App\Http\Controllers\Controller;
use App\Models\Work\WorkTypeHardware;
use App\Http\Requests\Work\StoreWorkTypeHardwareRequest;
use App\Http\Requests\Work\UpdateWorkTypeHardwareRequest;

class WorkTypeHardwareController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\Requests\StoreWorkTypeHardwareRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreWorkTypeHardwareRequest $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Work\WorkTypeHardware  $workTypeHardware
     * @return \Illuminate\Http\Response
     */
    public function show(WorkTypeHardware $workTypeHardware)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Work\WorkTypeHardware  $workTypeHardware
     * @return \Illuminate\Http\Response
     */
    public function edit(WorkTypeHardware $workTypeHardware)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Http\Requests\UpdateWorkTypeHardwareRequest  $request
     * @param  \App\Models\Work\WorkTypeHardware  $workTypeHardware
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateWorkTypeHardwareRequest $request, WorkTypeHardware $workTypeHardware)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Work\WorkTypeHardware  $workTypeHardware
     * @return \Illuminate\Http\Response
     */
    public function destroy(WorkTypeHardware $workTypeHardware)
    {
        //
    }
}
