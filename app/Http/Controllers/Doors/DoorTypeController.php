<?php

namespace App\Http\Controllers\Doors;

use App\Http\Controllers\Controller;
use App\Models\Doors\DoorType;
use App\Http\Requests\StoreDoorTypeRequest;
use App\Http\Requests\UpdateDoorTypeRequest;

class DoorTypeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\Requests\StoreDoorTypeRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreDoorTypeRequest $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Doors\DoorType  $doorType
     * @return \Illuminate\Http\Response
     */
    public function show(DoorType $doorType)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Doors\DoorType  $doorType
     * @return \Illuminate\Http\Response
     */
    public function edit(DoorType $doorType)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Http\Requests\UpdateDoorTypeRequest  $request
     * @param  \App\Models\Doors\DoorType  $doorType
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateDoorTypeRequest $request, DoorType $doorType)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Doors\DoorType  $doorType
     * @return \Illuminate\Http\Response
     */
    public function destroy(DoorType $doorType)
    {
        //
    }
}
