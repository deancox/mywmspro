<?php

namespace App\Http\Controllers\Doors;

use App\Http\Controllers\Controller;
use App\Models\Doors\Door;
use App\Http\Requests\StoreDoorRequest;
use App\Http\Requests\UpdateDoorRequest;
use App\Models\Doors\DoorType;
use App\Models\Inbound\InboundType;
use Illuminate\Support\Facades\DB;

class DoorController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        return view('doors.doors-index', [
            'doors' => Door::all(),
            'doorTypes' => DoorType::all()
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\Requests\StoreDoorRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreDoorRequest $request)
    {
        Door::create(
            $request->validated() + [
                'in_service' => $request->in_service ? true : false,
                'max_trucks' => $request->max_trucks ?? 1
            ]
        );
        return redirect()->route('doors.index');
        //dd($request);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Door  $door
     * @return \Illuminate\Http\Response
     */
    public function show(Door $door)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Door  $door
     * @return \Illuminate\Http\Response
     */
    public function edit(Door $door)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Http\Requests\UpdateDoorRequest  $request
     * @param  \App\Models\Door  $door
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateDoorRequest $request, Door $door)
    {
        $door = Door::select('id', 'in_service')
        ->where('id', $request->door)
        ->first();

        $door->in_service ? $door->toggleInService(0) : $door->toggleInService(1);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Door  $door
     * @return \Illuminate\Http\Response
     */
    public function destroy(Door $door)
    {
        //
    }
}
