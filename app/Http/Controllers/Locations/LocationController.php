<?php

namespace App\Http\Controllers\Locations;

use App\Http\Controllers\Controller;
use App\Http\Requests\UpdateLocationRequest;
use App\Models\Zones\CountZone;
use App\Models\Location;
use App\Models\LocationType;
use App\Models\Zones\PickZone;
use App\Models\Zones\StorageZone;
use App\Services\Locations\SyncLocationIds;
use App\Services\Locations\LocationService;
use Illuminate\Http\Request;

class LocationController extends Controller
{

    private $locationService;


    public function __construct(LocationService $locationService)
    {
        $this->locationService = $locationService;
    }


    public function index()
    {

        return view('inventory.location-table', [
            'locations' => Location::paginate(100),
            'countZones' => CountZone::all(),
            'pickZones' => PickZone::all(),
            'storageZones' => StorageZone::all(),
            'locationTypes' => LocationType::all(),
            'location' => '',

        ]);
    }


    public function store(Request $request)
    {
        //
    }


    public function show(location $location)
    {

        return view('inventory.edit-location', [

            'location' => $location,
            'countZones' => CountZone::where('name', '!=', $location->count_zone)->get(),
            'pickZones' => PickZone::where('name', '!=', $location->pick_zone)->get(),
            'storageZones' => StorageZone::where('name', '!=', $location->storage_zone)->get(),
            'locationTypes' => LocationType::where('name', '!=', $location->location_type)->get(),
            'locationType' => LocationType::where('name', $location->location_type)->get(),
        ]);
    }


    public function update(UpdateLocationRequest $request, Location $location)
    {

        $this->locationService->userUpdateLocation($request, $location);

        (new SyncLocationIds())->syncNow();

        return redirect()->route('location.index');
    }

    
    public function destroy($id)
    {
        //
    }
}
