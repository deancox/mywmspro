<?php

namespace App\Http\Requests\Work;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class StoreWorkTypeRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [

            'name' => 'required',
            'description' => 'required',
            'default_creation_status' => 'required',
            'base_priority' => 'required',
            //'auto_increment' => 'required',
            'increment_by' => Rule::requiredIf($this->request->has('auto_increment')),
            'increment_interval' => Rule::requiredIf($this->request->has('auto_increment')),
        ];
    }

    /*protected function prepareForValidation()
    {
        $this->merge([
            'auto_increment' => $this->request->has('auto_increment') ? true : false
        ]);
    }*/
}
