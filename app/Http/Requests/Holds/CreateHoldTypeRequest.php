<?php

namespace App\Http\Requests\Holds;

use Illuminate\Foundation\Http\FormRequest;

class CreateHoldTypeRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|unique:hold_types',
            'description' => 'required',
        ];
    }
}
