<?php

namespace App\Http\Requests\Inbound;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class StoreInboundCheckInRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        
        $delivery = $this->delivery;

        return [
            'door' => 'required',
            'lane' => Rule::requiredIf(function () use($delivery) {
                return $delivery->checkin_lane_required;
            })
        ];
    }
}
