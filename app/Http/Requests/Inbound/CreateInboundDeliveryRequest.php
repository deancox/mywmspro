<?php

namespace App\Http\Requests\Inbound;

use App\Actions\GetNextNumberAction;
use App\Models\Inbound\Supplier;
use Illuminate\Contracts\Container\BindingResolutionException;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\ValidationException;

class CreateInboundDeliveryRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'supplier' => 'required',
            'reference' => 'unique:App\Models\Delivery,delivery_ref',
            'dueDate' => 'after_or_equal:today',
            'time' => 'required',
            'shorts_allowed_flg' => 'sometimes|required',
            'delivery_type' => 'required',
        ];
    }

    /**
     * @return array 
     * @throws ValidationException 
     * @throws BindingResolutionException 
     */
    public function validated()
    {
        //set reference if user didn't choose one.
        if ($this->reference === null) {
            $this->reference = (new GetNextNumberAction())->getNextNumber('inbound_delivery');
        }

        //get supplier ID because we only pass the name from the front end.
        $this->supplier_id = Supplier::where('name', $this->supplier)->pluck('id');

        return array_merge(parent::validated(), [

            'created_by' => auth()->user()->name,
            'reference' => $this->reference,
            'supplier_id' => $this->supplier_id->get(0)

        ]);
    }
}
