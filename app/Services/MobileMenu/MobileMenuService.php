<?php

namespace App\Services\MobileMenu;

use App\Models\MobileMenu;

class MobileMenuService
{

    public function getMainMenu()
    {
        return MobileMenu::all()->sortby('displayorder');
    }


    public function updateSortOrder($request)
    {
        $newOrder = 1;

        foreach ($request->displayOrder as $menu_id) {
            MobileMenu::where('id', $menu_id)->update(['displayorder' => $newOrder]);
            $newOrder++;
        }
    }
}