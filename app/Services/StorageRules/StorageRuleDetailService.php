<?php

namespace App\Services\StorageRules;

use App\Models\Inbound\ProductTypeStorage;
use App\Models\Inbound\UomStorage;


class StorageRuleDetailService
{

    public function createStorageRuleDetail($request, $storageRule_id)
    {

        foreach ($request->storageZones as $zone) {
            $this->createZoneProductType($request, $zone, $storageRule_id);
            $this->createZoneUom($request, $zone, $storageRule_id);
        }
    }


    private function createZoneProductType($request, $zone, $storageRule_id)
    {

        foreach ($request->productTypes as $type) {
            ProductTypeStorage::create([
                'storage_rule_id' => $storageRule_id,
                'product_type_id' => $type,
                'storage_zone_id' => $zone
            ]);
        };
    }


    private function createZoneUom($request, $zone, $storageRule_id)
    {

        foreach ($request->uoms as $uom) {
            UomStorage::create([
                'storage_rule_id' => $storageRule_id,
                'uom_id' => $uom,
                'storage_zone_id' => $zone
            ]);
        };
    }
}
