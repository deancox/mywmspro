<?php

namespace App\Services\Inventory;

use App\Services\Products\ProductService;
use App\Services\Utils\IdGeneratorService;

class InventoryStructureService
{

    private $productService;
    private $idService;

    public function __construct()
    {
        $this->productService = new ProductService;
        $this->idService = new IdGeneratorService;
    }

    public function createNewInventoryStructure($product_id, $lpn_level, $quantity, $lpn = null)
    {

        if ($lpn_level === 'PA') {

            return $this->createLpnStructureByPallet($product_id, $quantity, $lpn);
        }
        if ($lpn_level === 'CS') {

            return $this->createLpnStructureByCase($product_id, $quantity, $lpn);
        }

        return $this->createLpnStructureByEach($product_id, $quantity, $lpn);
    }


    public function createLpnStructureByEach($product_id, $quantity, $lpn = null)
    {

        $inventoryStructure = [
            'qty' => $quantity,
            'pallet_qty' => $this->productService->getLpnLevelQuantity($product_id, 'PA'),
            'case_qty' => $this->productService->getLpnLevelQuantity($product_id, 'CS'),
            'pallet_id' => $lpn ? $lpn : $this->idService->getNextPalletID(),
            'case_id' => $this->idService->getNextCaseID(),
            'each_id' => $this->idService->getNextEachID(),
        ];

        return $inventoryStructure;
    }


    public function createLpnStructureByCase($product_id, $quantity, $lpn = null)
    {

        $case_qty = $this->productService->getLpnLevelQuantity($product_id, 'CS');

        for ($i = 0; $i < $quantity; $i++) {
            $inventoryStructure = [
                'qty' => $case_qty,
                'pallet_qty' => $this->productService->getLpnLevelQuantity($product_id, 'PA'),
                'case_qty' => $case_qty,
                'pallet_id' => $this->idService->getNextPalletID(),
                'case_id' => $lpn ? $lpn : $this->idService->getNextCaseID(),
                'each_id' => $this->idService->getNextEachID(),
            ];
        }

        return $inventoryStructure;
    }


    public function createLpnStructureByPallet($product_id, $quantity, $lpn = null)
    {

        $pallet_qty = $this->productService->getLpnLevelQuantity($product_id, 'PA');

        for ($i = 0; $i < $quantity; $i++) {
            $inventoryStructure = [
                'qty' => $pallet_qty,
                'pallet_qty' => $pallet_qty,
                'case_qty' => $this->productService->getLpnLevelQuantity($product_id, 'CS'),
                'pallet_id' => $lpn ? $lpn : $this->idService->getNextPalletID(),
                'case_id' => $this->idService->getNextCaseID(),
                'each_id' => $this->idService->getNextEachID(),
            ];
        }

        return $inventoryStructure;
    }
}
