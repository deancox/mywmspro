<?php

namespace App\Services\Inventory;

use App\Models\Inventory\Inventory;
use App\Models\Product;


class InventoryService
{

    private $inventoryStructureService;


    public function __construct()
    {
        
        $this->inventoryStructureService = new InventoryStructureService;
    }


    public function createInventoryForReceipt($request, $deliveryDetail)
    {

        $product = Product::where('product', $deliveryDetail->product)->first();

        if ($request->rcvlpnlevel != 'EA') {

            foreach ($request->rcvlpns as $lpn) {
                $inventoryStructure[] =
                    $this->inventoryStructureService->createNewInventoryStructure($product->id, $request->rcvlpnlevel, 1, $lpn);
            }
        } else {

            $inventoryStructure[] =
                $this->inventoryStructureService->createNewInventoryStructure($product->id, $request->rcvlpnlevel, $request->quantity, $request->rcvlpns[0]);
        }

        foreach ($inventoryStructure as $structure) {

            $inventory = Inventory::create([
                'location' => $request->staging_lane,
                'product' => $deliveryDetail->product,
                'qty' => $structure['qty'],
                'pallet_qty' => $structure['pallet_qty'],
                'case_qty' => $structure['case_qty'],
                'inventory_status' => $product->def_rcv_status,
                'pallet_id' => $structure['pallet_id'],
                'case_id' => $structure['case_id'],
                'each_id' => $structure['each_id'],
                'supplier_id' => $deliveryDetail->supplier_id,
                'delivery_ref' => $deliveryDetail->delivery->delivery_ref,
                'product_type' => $product->product_type,
                'created_by' => auth()->user()->name
            ]);
        }

        return $inventory;
    }
}
