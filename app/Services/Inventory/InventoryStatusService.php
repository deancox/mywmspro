<?php

namespace App\Services\Inventory;

use App\Models\Inventory\InventoryStatus;


class InventoryStatusService
{
    
    public function store($request)
    {
        InventoryStatus::create([
            'name' => $request['name'],
            'description' => $request['description'],
            'created_by' => auth()->user()->name
        ]);
    }

}