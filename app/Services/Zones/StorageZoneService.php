<?php

namespace App\Services\Zones;

use App\Models\Zones\StorageZone;
use Illuminate\Contracts\Container\BindingResolutionException;

class StorageZoneService
{
    
    /**
     * @param mixed $request 
     * @return void 
     * @throws BindingResolutionException 
     */
    public function store($request)
    {
        StorageZone::create([
            'name' => $request['name'],
            'description' => $request['description'],
            'created_by' => auth()->user()->name
        ]);
    }


    /** @return mixed  */
    public function getStorageZonesWithLocationCount()
    {
        return StorageZone::withCount('locations')->paginate(20);
    }

}