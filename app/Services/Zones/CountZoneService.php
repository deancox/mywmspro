<?php

namespace App\Services\Zones;

use App\Models\Zones\CountZone;
use Illuminate\Contracts\Container\BindingResolutionException;

class CountZoneService
{
    /**
     * @param mixed $request 
     * @return void 
     * @throws BindingResolutionException 
     */
    public function store($request)
    {
        CountZone::create([
            'name' => $request['name'],
            'description' => $request['description'],
            'created_by' => auth()->user()->name
        ]);
    }

    /** @return mixed  */
    public function getCountZonesWithLocationCount()
    {
        return CountZone::withCount('locations')->paginate(20);
    }

}