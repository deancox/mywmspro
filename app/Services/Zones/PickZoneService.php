<?php

namespace App\Services\Zones;

use App\Models\Zones\PickZone;
use Illuminate\Contracts\Container\BindingResolutionException;

class PickZoneService
{
    /**
     * @param mixed $request 
     * @return void 
     * @throws BindingResolutionException 
     */
    public function store($request)
    {
        PickZone::create([
            'name' => $request['name'],
            'description' => $request['description'],
            'created_by' => auth()->user()->name
        ]);
    }

    /** @return mixed  */
    public function getPickZonesWithLocationCount()
    {
        return PickZone::withCount('locations')->paginate(20);
    }

}