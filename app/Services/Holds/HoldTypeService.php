<?php

namespace App\Services\Holds;

use App\Models\Holds\HoldType;

class HoldTypeService
{
    
    public function store($request)
    {
        HoldType::create([
            'name' => $request['name'],
            'description' => $request['description'],
            'created_by' => auth()->user()->name
        ]);
    }

}