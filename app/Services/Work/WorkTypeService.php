<?php

namespace App\Services\Work;

use App\Models\Work\WorkType;

class WorkTypeService 
{

    public function storeNewWorkType($request)
    {
        WorkType::create($request);
    }

}