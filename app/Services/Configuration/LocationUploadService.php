<?php

namespace App\Services\Configuration;

use App\Events\LocationFileUploaded;
use App\Models\Location;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\LazyCollection;

class LocationUploadService
{

    public function upload($request)
    {

        $file = $request->location_upload->storeAs('uploads', $request->location_upload->getClientOriginalName());

        //to prevent fk errors on truncate
        Schema::disableForeignKeyConstraints();

        Location::truncate();

        LazyCollection::make(function () use ($file) {

            $handle = fopen(storage_path('app/' . $file), 'r');
            fgetcsv($handle); //consumes first row (headings) 

            while ($line = fgetcsv($handle)) {
                yield $line;
            }
        })->chunk(10000)
            ->each(function ($lines) {
                $list = [];
                foreach ($lines as $line) {
                    if (isset($line[1])) {
                        $list[] = [
                            'location_name'     => $line[0],
                            'aisle'             => $line[1],
                            'level'             => $line[2],
                            'position'          => $line[3],
                            'height'            => $line[4],
                            'width'             => $line[5],
                            'length'            => $line[6],
                            'maxweight'         => $line[7],
                            'tracked_by'        => $line[8],
                            'maxqty'            => $line[9],
                            'travel_sequence'   => $line[10],
                            'storage_sequence'  => $line[11],
                            'pick_sequence'     => $line[12],
                            'location_type'     => $line[13],
                            'storage_zone'      => $line[14],
                            'pick_zone'         => $line[15],
                            'count_zone'        => $line[16],
                            'is_pickable'       => $line[17],
                            'is_pickface'       => $line[18],
                            'is_usable'         => $line[19],
                            'is_storable'       => $line[20],
                            'created_by'        => 'Mass Upload ' . auth()->user()->name,
                            'created_at'        => now()
                        ];
                    }
                }
                foreach (array_chunk($list, 100) as $record) {
                    Location::insert($record);
                };
            });

        event(new LocationFileUploaded());

        //disabled at start of upload 
        Schema::enableForeignKeyConstraints();

        return Location::count();
    }
}
