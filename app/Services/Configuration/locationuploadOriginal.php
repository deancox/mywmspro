<?php

namespace App\Services\Configuration;

use App\Events\LocationFileUploaded;
use App\Models\Location;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\LazyCollection;

class LocationUploadService
{
    public function upload($request)
    {
        $file = $request->location_upload->storeAs('uploads', $request->location_upload->getClientOriginalName());
        $file = File::get(storage_path('app/' . $file));

        //to prevent fk errors on truncate
        Schema::disableForeignKeyConstraints();

        Location::truncate();

        $contents = explode(PHP_EOL, $file);

        $count = 0;
        $import_count = 0;

        foreach ($contents as $line) {

            //first line contains headings
            if ($count > 0) {

                $fields = explode(',', $line);

                $field_count = count($fields);

                if ($field_count == 21) {

                    $location_name     = $fields[0];
                    $aisle             = $fields[1];
                    $level             = $fields[2];
                    $position          = $fields[3];
                    $height            = $fields[4];
                    $width             = $fields[5];
                    $length            = $fields[6];
                    $maxweight         = $fields[7];
                    $tracked_by        = $fields[8];
                    $maxqty            = $fields[9];
                    $travel_sequence   = $fields[10];
                    $storage_sequence  = $fields[11];
                    $pick_sequence     = $fields[12];
                    $location_type     = $fields[13];
                    $storage_zone      = $fields[14];
                    $pick_zone         = $fields[15];
                    $count_zone        = $fields[16];
                    $is_pickable       = $fields[17];
                    $is_pickface       = $fields[18];
                    $is_usable         = $fields[19];
                    $is_storable       = $fields[20];

                    $location_name    = str_replace([',', ';'], '', $location_name);
                    $aisle            = str_replace([',', ';'], '', $aisle);
                    $level            = str_replace([',', ';'], '', $level);
                    $position         = str_replace([',', ';'], '', $position);
                    $height           = str_replace([',', ';'], '', $height);
                    $width            = str_replace([',', ';'], '', $width);
                    $length           = str_replace([',', ';'], '', $length);
                    $maxweight        = str_replace([',', ';'], '', $maxweight);
                    $tracked_by       = str_replace([',', ';'], '', $tracked_by);
                    $maxqty           = str_replace([',', ';'], '', $maxqty);
                    $travel_sequence  = str_replace([',', ';'], '', $travel_sequence);
                    $storage_sequence = str_replace([',', ';'], '', $storage_sequence);
                    $pick_sequence    = str_replace([',', ';'], '', $pick_sequence);
                    $location_type    = str_replace([',', ';'], '', $location_type);
                    $storage_zone     = str_replace([',', ';'], '', $storage_zone);
                    $pick_zone        = str_replace([',', ';'], '', $pick_zone);
                    $count_zone       = str_replace([',', ';'], '', $count_zone);
                    $is_pickable      = str_replace([',', ';'], '', $is_pickable);
                    $is_pickface      = str_replace([',', ';'], '', $is_pickface);
                    $is_usable        = str_replace([',', ';'], '', $is_usable);
                    $is_storable      = str_replace([',', ';'], '', $is_storable);
                    $count_zone       = str_replace([',', ';'], '', $count_zone);
                    $is_pickable      = str_replace([',', ';'], '', $is_pickable);
                    $is_pickface      = str_replace([',', ';'], '', $is_pickface);
                    $is_usable        = str_replace([',', ';'], '', $is_usable);
                    $is_storable      = str_replace([',', ';'], '', $is_storable);

                    // Insert
                    $location = new Location;

                    $location->location_name    = $location_name;
                    $location->aisle            = $aisle;
                    $location->level            = $level;
                    $location->position         = $position;
                    $location->height           = $height;
                    $location->width            = $width;
                    $location->length           = $length;
                    $location->maxweight        = $maxweight;
                    $location->tracked_by       = $tracked_by;
                    $location->maxqty           = $maxqty;
                    $location->travel_sequence  = $travel_sequence;
                    $location->storage_sequence = $storage_sequence;
                    $location->pick_sequence    = $pick_sequence;
                    $location->location_type    = $location_type;
                    $location->storage_zone     = $storage_zone;
                    $location->pick_zone        = $pick_zone;
                    $location->count_zone       = $count_zone;
                    $location->is_pickable      = $is_pickable;
                    $location->is_pickface      = $is_pickface;
                    $location->is_usable        = $is_usable;
                    $location->is_storable      = $is_storable;
                    $location->created_by       = 'Mass Upload ' . auth()->user()->name;

                    $location->save();

                    $import_count++;
                }
            }

            $count++;
        }

        event(new LocationFileUploaded());

        //disabled at start of upload 
        Schema::enableForeignKeyConstraints();

        return $import_count;
    }

    //---------------------------------------------------------------------------------------------------------------------------

    public function uploadTest($request)
    {

        $file = $request->location_upload->storeAs('uploads', $request->location_upload->getClientOriginalName());

        //to prevent fk errors on truncate
        Schema::disableForeignKeyConstraints();

        Location::truncate();

        LazyCollection::make(function () use ($file) {

            $handle = fopen(storage_path('app/' . $file), 'r');

            fgetcsv($handle); //Skip header row

            while ($line = fgetcsv($handle)) {
                yield $line;
            }
            
        })->chunk(10000)
            ->each(function ($lines) {

                $list = [];

                foreach ($lines as $line) {

                    if (isset($line[1])) {

                        $list[] = [
                            'location_name'  => $line[0],
                            'aisle'  => $line[1],
                            'level'  => $line[2],
                            'position'  => $line[3],
                            'height'  => $line[4],
                            'width'  => $line[5],
                            'length'  => $line[6],
                            'maxweight'  => $line[7],
                            'tracked_by'  => $line[8],
                            'maxqty'  => $line[9],
                            'travel_sequence'  => $line[10],
                            'storage_sequence'  => $line[11],
                            'pick_sequence'  => $line[12],
                            'location_type'  => $line[13],
                            'storage_zone'  => $line[14],
                            'pick_zone'  => $line[15],
                            'count_zone'  => $line[16],
                            'is_pickable'  => $line[17],
                            'is_pickface'  => $line[18],
                            'is_usable'  => $line[19],
                            'is_storable'  => $line[20],
                            'created_by' => 'Mass Upload ' . auth()->user()->name,
                            'created_at' => now()
                        ];
                    }
                }
                foreach (array_chunk($list, 100) as $record) {
                    Location::insert($record);
                };
            });


        event(new LocationFileUploaded());

        //disabled at start of upload 
        Schema::enableForeignKeyConstraints();

        return Location::count();
    }
}
