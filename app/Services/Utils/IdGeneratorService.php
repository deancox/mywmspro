<?php

namespace App\Services\Utils;

use App\Models\SystemSettings;

class IdGeneratorService
{

    public function getNextPalletID()
    {
        $id = SystemSettings::first();
        $id->increment('pallet_id');

        return $id->pallet_id_prefix . $id->pallet_id;
    }

    public function getNextCaseID()
    {
        $id = SystemSettings::first();
        $id->increment('case_id');

        return $id->case_id_prefix . $id->case_id;
    }

    public function getNextEachID()
    {
        $id = SystemSettings::first();
        $id->increment('each_id');

        return $id->each_id_prefix . $id->each_id;
    }

    public function getNextSubUomID()
    {
        $id = SystemSettings::first();
        $id->increment('subuom_id');

        return $id->subuom_id_prefix . $id->subuom_id;
    }
}
