<?php

namespace App\Services\Locations;

use App\Models\LocationType;

class LocationTypeService
{
    public function store($request)
    {
        LocationType::create([
            'name' => $request['name'],
            'description' => $request['description'],
            'created_by' => auth()->user()->name
        ]);
    }
}