<?php

namespace App\Services\Locations;

use App\Events\LocationFileUploaded;
use App\Models\Zones\CountZone;
use App\Models\Location;
use App\Models\LocationType;
use App\Models\Zones\PickZone;
use App\Models\Zones\StorageZone;

class SyncLocationIds
{

    /**
     * Handle the event.
     *
     * @param  LocationFileUploaded  $event
     * @return void
     */
    public function syncNow()
    {

        $location_types = LocationType::all();

        foreach ($location_types as $type) {
            Location::where('location_type', $type->name)
                ->update(['location_type_id' => $type->id]);
        }

        $storage_zones = StorageZone::all();

        foreach ($storage_zones as $zone) {
            Location::where('storage_zone', $zone->name)
                ->update(['storage_zone_id' => $zone->id]);
        }

        $count_zones = CountZone::all();

        foreach ($count_zones as $zone) {
            Location::where('count_zone', $zone->name)
                ->update(['count_zone_id' => $zone->id]);
        }

        $pick_zones = PickZone::all();

        foreach ($pick_zones as $zone) {
            Location::where('pick_zone', $zone->name)
                ->update(['pick_zone_id' => $zone->id]);
        }
    }
}
