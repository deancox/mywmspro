<?php

namespace App\Services\Locations;

use App\Http\Requests\UpdateLocationRequest;
use App\Models\Location;
use App\Models\LocationType;
use App\Models\Zones\StorageZone;

class LocationService
{

    public function userUpdateLocation(UpdateLocationRequest $request, Location $location)
    {

        $request->has('is_pickable') ? $location->setPickable(1) : $location->setPickable(0);
        $request->has('is_usable') ? $location->setUsable(1) : $location->setUsable(0);
        $request->has('is_storable') ? $location->setStorable(1) : $location->setStorable(0);

        $location->update($request->validated());

    }

    public function findByLocationType(LocationType $locationType)
    {

        $locations = Location::where('location_type_id', $locationType->id)->paginate(10);

        return $locations;
    }


    public function findByStorageZone(StorageZone $storageZone)
    {

        $locations = Location::where('storage_zone_id', $storageZone->id)->paginate(15);

        return $locations;
    }


    public function getDoors(Location $Location)
    {
        $doors = Location::where('location_type', 'DOOR')->get();

        return $doors;
    }
}
