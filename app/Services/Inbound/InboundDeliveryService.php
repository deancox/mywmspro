<?php

namespace App\Services\Inbound;

use App\Models\Delivery;
use App\Models\DeliveryDetail;
use App\Services\Products\ProductService;
use App\Services\Products\UomService;

class InboundDeliveryService
{

    private $productService;
    private $uomService;


    public function __construct()
    {

        $this->productService = new ProductService;
        $this->uomService = new UomService;
    }


    public function getDeliveryDetails($delivery_id)
    {

        return Delivery::with('deliveryDetails')
            ->where('id', $delivery_id)
            ->first();
    }


    public function getDeliverySupplierID($delivery_id)
    {
        return Delivery::find($delivery_id)
            ->pluck('supplier_id')
            ->get(0);
    }


    public function storeInboundDelivery($request)
    {

        $delivery = Delivery::create([
            'delivery_ref' => $request['reference'],
            'supplier_id' => $request['supplier_id'],
            'delivery_type' => $request['delivery_type'],
            'due_date' => $request['dueDate'] . ' ' . $request['time'] . ':00',
            'short_receipt_allowed' => isset($request['short_receipt_allowed']) ? true : false,
            'created_by' => $request['created_by']
        ]);

        return $delivery;
    }


    public function storeInboundDeliveryLine($request)
    {

        $deliveryDetail = DeliveryDetail::create([
            'delivery_id' => $request->delivery_id,
            'supplier_id' => $this->getDeliverySupplierID($request->delivery_id),
            'rcv_line' => $this->getNextReceiveLine($request->delivery_id),
            'sub_rcv_line' => $this->getNextSubReceiveLine($request->delivery_id),
            'product' => $this->productService->getProductName($request->product_id),
            'expected_qty' => $request->qty,
            'status' => 'EXPECTED',
            'uom' => $this->uomService->getUomDescription($request->uom_id),
            'uom_id' => $request->uom_id,
            'uom_qty' => $this->productService->getUomQuantity($request->product_id, $request->uom_id),
            'lpnlevel' => $this->productService->getLpnLevel($request->product_id, $request->uom_id),
            'created_by' => auth()->user()->name,
            'batch' => $request->batch,
            'cust_batch' => $request->cust_batch,
        ]);

        return $deliveryDetail;
    }


    public function deliveryLineReceived($request, $inventory, $deliveryDetail)
    {

        if ($deliveryDetail->delivery->status != 'IN_PROGRESS') {
            $deliveryDetail->delivery->update(['status' => 'IN_PROGRESS']);
        };

        $deliveryDetail->update([
            'expected_qty' => $deliveryDetail->expected_qty - $request->quantity,
            'received_qty' => $deliveryDetail->received_qty + $request->quantity,
            'pallet_id' => $inventory->pallet_id,
            'case_id' => $inventory->case_id,
            'each_id' => $inventory->each_id,
            'rcv_to_lane' => $inventory->location,
            'inprogress_flg' => true,

        ]);

        //TODO: 
        // if received = expected and deliverydetail over_receipt_allowed == false then 
        // update completed flag and set status to COMPLETE

        return $deliveryDetail;
    }


    private function getNextReceiveLine($delivery)
    {
        //later new function to check if line consolidation is allowed
        //and add extra logic for that. 
        $rcv_line = DeliveryDetail::where('delivery_id', $delivery)->max('rcv_line');

        return $rcv_line + 1;
    }


    private function getNextSubReceiveLine($delivery)
    {
        //later new function to check if line consolidation is allowed
        //and add extra logic for that. 
        $sub_rcv_line = DeliveryDetail::where('delivery_id', $delivery)->max('sub_rcv_line');

        return $sub_rcv_line + 1;
    }

}
