<?php

namespace App\Services\Inbound;

use App\Models\Delivery;
use App\Models\Staging\StagingLane;

class InboundStagingService
{

    public function StoreNewStagingLane($request)
    {

        StagingLane::create([
            'name' => $request->name,
            'max_deliveries' => $request->max_deliveries,
            'in_service' => $request->in_service ? true : false,
            'tracked' => $request->tracked ? true : false,
            'tracked_by' => $request->tracked_by,
            'maxqty' => $request->maxqty
        ]);
    }


    public function getAvailableInboundLanes()
    {
        
        $lanes = StagingLane::where('in_service', true)
            ->whereRaw('(cur_delivery_count < max_deliveries)')
            ->where('full', false)
            ->get();

        return $lanes;
    }


    public function assignDeliveryToLane($lane_id, $delivery_id)
    {

        $lane = StagingLane::find($lane_id);

        $lane->increment('cur_delivery_count');

        Delivery::find($delivery_id)->update(['lane' => $lane->name]);

    }


    public function assignDeliveryLineToLane($lane)
    {
        $lane = StagingLane::find($lane);
    }
}
