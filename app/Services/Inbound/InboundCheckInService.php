<?php

namespace App\Services\Inbound;

use App\Models\Doors\Door;
use Illuminate\Support\Carbon;

class InboundCheckInService
{

    public function __construct()
    {
        $this->stagingService = new InboundStagingService;
    }


    public function validateCanCheckin($delivery)
    {
        if ($delivery->truck_required && !$delivery->truck) {
            return false;
        }
        return true;
    }


    public function getDoorsForCheckin($delivery)
    {

        $doors = Door::whereHas('inboundTypes', function ($query) use ($delivery) {
            $query->where('inbound_type_id', $delivery->delivery_type)
                ->where('in_service', true)
                ->whereRaw('(cur_truck_count < max_trucks)');
        })->get();

        return $doors;
    }


    public function checkInDelivery($request, $delivery)
    {
        //if lane then lane check in
        in_array($request, ['lane']) ?? $this->stagingService->assignDeliveryToLane($request['lane'], $delivery);

        $door = Door::find($request['door']);

        $delivery->update([
            'door' => $door->name,
            'checked_in_flg' => true,
            'status' => 'CHECKED_IN',
            'check_in_date' => Carbon::now()
        ]);

        $door->increment('cur_delivery_count');

        $this->checkInDeliveryLines($delivery);

        //log event delivery checked in ($delivery)?

        //event listener if delivery has truck then checkin truck

        //event listener process exitpoint actions (future) 

    }

    
    public function checkInDeliveryLines($delivery)
    {
        foreach ($delivery->deliveryDetails as $detail) {

            $detail->update(['status' => 'CHECKED_IN']);
        }
    }

    public function checkInTruckToDoor($door, $delivery)
    {
        //increment door current truck count and truck location & status
    }
}
