<?php

namespace App\Services\Inbound;

use App\Models\DeliveryDetail;
use App\Services\Inventory\InventoryService;
use Illuminate\Support\Facades\DB;

class ReceivingService
{

    private $inventoryService;
    private $deliveryService;


    public function __construct()
    {
        $this->inventoryService = new InventoryService;
        $this->deliveryService = new InboundDeliveryService;
    }


    public function receiveInventoryLine($request)
    {

        $deliveryDetail = DeliveryDetail::with('delivery')->find($request->delivery_detail_id);

        DB::transaction(function () use ($request, $deliveryDetail) {

            $inventory = $this->inventoryService->createInventoryForReceipt($request, $deliveryDetail);

            $this->deliveryService->deliveryLineReceived($request, $inventory, $deliveryDetail);

        });

       return $deliveryDetail;
    }


    public function getLineDetailsForReceipt($request)
    {

        $deliveryDetail = DeliveryDetail::with('delivery')
            ->where('id', $request->deliveryDetail)
            ->first();

        return $deliveryDetail;
    }

    
}
