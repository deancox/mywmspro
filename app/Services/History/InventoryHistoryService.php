<?php

namespace App\Services\History;

use App\Models\History\InventoryHistory;
use App\Models\Inventory\Inventory;

class InventoryHistoryService
{

    public function LogInventoryCreated(Inventory $inventory)
    {

        InventoryHistory::create([
            'product' => $inventory->product,
            'transaction_type' => 'ADD',
            'action' => null,
            'transaction_comment' => null,
            'fr_location' => $inventory->delivery_ref ?? null,
            'to_location' => $inventory->location,
            'fr_qty' => 0,
            'to_qty' => $inventory->qty,
            'fr_pallet_qty' => 0,
            'to_pallet_qty' => $inventory->pallet_qty,
            'fr_case_qty' => 0,
            'to_case_qty' => $inventory->case_qty,
            'fr_subuom_qty' => 0,
            'to_subuom_qty' => $inventory->sub_uom_qty ?? null,
            'uom' => $inventory->uom ?? null,
            'sub_uom' => $inventory->sub_uom ?? null,
            'fr_inventory_status' => $inventory->inventory_status,
            'to_inventory_status' => $inventory->inventory_status,
            'hold_id' => $inventory->hold_id ?? null,
            'hold_type_id' => $inventory->hold_type_id ?? null,
            'fr_pallet_id' => null,
            'fr_case_id' => null,
            'fr_each_id' => null,
            'fr_sub_uom_id' => null,
            'to_pallet_id' => $inventory->pallet_id,
            'to_case_id' => $inventory->case_id,
            'to_each_id' => $inventory->each_id,
            'to_sub_uom_id' => $inventory->sub_uom_id ?? null,
            'supplier_id' => $inventory->supplier_id ?? null,
            'delivery_ref' => $inventory->delivery_ref ?? null,
            'fr_product_type' => null,
            'to_product_type' => $inventory->product_type,
            'fr_serial_no' => null,
            'to_serial_no' => null,
            'user_id' => auth()->id(),
        ]);
    }
}
