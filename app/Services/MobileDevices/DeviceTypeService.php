<?php

namespace App\Services\MobileDevices;

use App\Models\DeviceType;

class DeviceTypeService
{
    
    public function store($request)
    {
        DeviceType::create([
            'name' => $request['name'],
            'description' => $request['description'],
            'created_by' => auth()->user()->name
        ]);
    }

    public function getDeviceTypesWithDeviceCount()
    {
        return DeviceType::withCount('devices')->paginate(20);
    }

}