<?php

namespace App\Services\MobileDevices;

use App\Models\Device;

class MobileDeviceService
{

    public function store($request)
    {
        Device::create([
            'name' => $request['name'],
            'device_type_id' => $request['device_type_id'],
            'created_by' => auth()->user()->name
        ]);
    }

}