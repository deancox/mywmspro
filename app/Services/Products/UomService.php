<?php

namespace App\Services\Products;

use App\Models\Uom;

class UomService
{

    public function getUomDescription($uom_id)
    {
        return Uom::select('description')
            ->where('id', $uom_id)
            ->pluck('description')
            ->first();
    }
}
