<?php

namespace App\Services\Products;

use App\Models\Product;
use App\Models\ProductDetail;

class ProductService
{

    public function findProductByName($product_name)
    {

        return Product::where('product', $product_name)->first();
    }


    public function getProductName($product_id)
    {

        return Product::select('product')
            ->where('id', $product_id)
            ->pluck('product')
            ->first();
    }


    public function getReceivableProducts()
    {

        return Product::where('rcv_flg', true)->get();
    }


    public function getProductUoms($product_id)
    {

        $productDetail = ProductDetail::with('uom')->where('product_id', $product_id)->get();

        $productUoms = [];

        foreach ($productDetail as $detail) {
            $productUoms[] = [
                'uom_id' => $detail->uom_id,
                'uom' => $detail->uom,
                'description' => $detail->Uom->description,
            ];
        }

        return $productUoms;
    }


    public function getLpnLevel($product_id, $uom_id)
    {

        $lpn_level = ProductDetail::where('product_id', $product_id)
            ->where('uom_id', $uom_id)
            ->pluck('lpn_level')
            ->get(0);

        return $lpn_level;
    }


    public function getUomQuantity($product_id, $uom_id)
    {

        $uom_quantity = ProductDetail::select('uom_qty')
            ->where('product_id', $product_id)
            ->where('uom_id', $uom_id)
            ->first();

        return $uom_quantity->uom_qty;
    }

    public function getLpnLevelQuantity($product_id, $lpn_level)
    {

        $quantity = ProductDetail::select('uom_qty')
            ->where('product_id', $product_id)
            ->where('lpn_level', $lpn_level)
            ->first();

        return $quantity->uom_qty;
    }
}
