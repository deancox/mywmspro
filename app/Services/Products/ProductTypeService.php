<?php

namespace App\Services\Products;

use App\Models\Products\ProductType;

class ProductTypeService
{
    
    public function store($request)
    {
        ProductType::create([
            'name' => $request['name'],
            'description' => $request['description'],
            'created_by' => auth()->user()->name
        ]);
    }

}