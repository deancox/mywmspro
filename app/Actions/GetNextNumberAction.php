<?php

namespace App\Actions;

use App\Models\SystemSettings;

class GetNextNumberAction
{

    public function getNextNumber($type)
    {
        $number = SystemSettings::select($type, $type . '_prefix');
        SystemSettings::where($type, $number->pluck($type)->get(0))->increment($type);

        return $number->pluck($type . '_prefix')->get(0) . $number->pluck($type)->get(0);
    }
}
