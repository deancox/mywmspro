<?php

namespace App\View\Components;

use App\Models\Zones\CountZone;
use App\Models\Location;
use App\Models\LocationType;
use App\Models\Zones\PickZone;
use App\Models\Zones\StorageZone;
use Illuminate\View\Component;

class EditLocation extends Component
{
    /**
     * Create a new component instance.
     *
     * @return void
     */
    public function __construct(Location $location)
    {
        $this->location = $location;
    }

    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\Contracts\View\View|\Closure|string
     */
    public function render()
    {
        //dd($this->location);
        return view('components.edit-location',[
            'location' => $this->location,
            'countZones' => CountZone::where('name', '!=', $this->location->count_zone)->get(),
            'pickZones' => PickZone::where('name', '!=', $this->pick_zone)->get(),
            'storageZones' => StorageZone::where('name', '!=', $this->location->storage_zone)->get(),
            'locationTypes' => LocationType::where('name', '!=', $this->location->location_type)->get(),
            'locationType' => LocationType::where('name', $this->location->location_type)->get(),
        ]);
    }
}
