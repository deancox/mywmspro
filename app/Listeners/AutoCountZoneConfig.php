<?php

namespace App\Listeners;

use App\Events\LocationFileUploaded;
use App\Models\Zones\CountZone;
use App\Models\Location;

class AutoCountZoneConfig
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  LocationFileUploaded  $event
     * @return void
     */
    public function handle(LocationFileUploaded $event)
    {
       
        $countZones = Location::whereNotNull('count_zone')
            ->where('count_zone', '!=', '')
            ->get()
            ->unique('count_zone')
            ->pluck('count_zone');

        foreach ($countZones as $zone) {
            CountZone::firstOrCreate([
                'name' => strtoupper($zone),
                'description' => 'Enter a zone description',
                'created_by' => 'Mass Upload '.auth()->user()->name,
            ]);
        }
    }
}
