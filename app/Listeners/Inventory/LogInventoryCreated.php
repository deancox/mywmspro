<?php

namespace App\Listeners\Inventory;

use App\Events\Inventory\InventoryCreated;
use App\Services\History\InventoryHistoryService;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;

class LogInventoryCreated
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        $this->inventoryHistoryService = new InventoryHistoryService;
    }

    /**
     * Handle the event.
     *
     * @param  InventoryCreated  $event
     * @return void
     */
    public function handle(InventoryCreated $event)
    {
        $this->inventoryHistoryService->logInventoryCreated($event->inventory);
        //dd($event,'loginventorycreated',$event->inventory->location);
    }
}
