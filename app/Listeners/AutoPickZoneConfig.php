<?php

namespace App\Listeners;

use App\Events\LocationFileUploaded;
use App\Models\Location;
use App\Models\Zones\PickZone;

class AutoPickZoneConfig
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  LocationFileUploaded  $event
     * @return void
     */
    public function handle(LocationFileUploaded $event)
    {

        $pickZones = Location::whereNotNull('pick_zone')
            ->where('pick_zone', '!=', '')
            ->get()
            ->unique('pick_zone')
            ->pluck('pick_zone');

        foreach ($pickZones as $zone) {
            PickZone::firstOrCreate([
                'name' => strtoupper($zone),
                'description' => 'Enter a zone description',
                'created_by' => 'Mass Upload ' . auth()->user()->name,
            ]);
        }
    }
}
