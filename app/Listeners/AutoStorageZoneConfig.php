<?php

namespace App\Listeners;

use App\Events\LocationFileUploaded;
use App\Models\Location;
use App\Models\Zones\StorageZone;

class AutoStorageZoneConfig
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  LocationFileUploaded  $event
     * @return void
     */
    public function handle(LocationFileUploaded $event)
    {

        $storageZones = Location::whereNotNull('storage_zone')
            ->where('storage_zone', '!=', '')
            ->get()
            ->unique('storage_zone')
            ->pluck('storage_zone');

        foreach ($storageZones as $zone) {
            StorageZone::firstOrCreate([
                'name' => strtoupper($zone),
                'description' => 'Enter a zone description',
                'created_by' => 'Mass Upload ' . auth()->user()->name,
            ]);
        }
    }
}
