<?php

namespace App\Providers;

use App\Events\Inventory\InventoryCreated;
use Illuminate\Auth\Events\Registered;
use Illuminate\Auth\Listeners\SendEmailVerificationNotification;
use Illuminate\Foundation\Support\Providers\EventServiceProvider as ServiceProvider;
use App\Listeners\AutoStorageZoneConfig;
use App\Listeners\AutoCountZoneConfig;
use App\Events\LocationFileUploaded;
use App\Listeners\AutoPickZoneConfig;
use App\Listeners\Inventory\LogInventoryCreated;
use App\Models\Inventory\Inventory;
use App\Observers\InventoryObserver;

class EventServiceProvider extends ServiceProvider
{
    /**
     * The event listener mappings for the application.
     *
     * @var array
     */
    protected $listen = [

        Registered::class => [
            SendEmailVerificationNotification::class,
        ],
        LocationFileUploaded::class => [
            AutoStorageZoneConfig::class,
            AutoCountZoneConfig::class,
            AutoPickZoneConfig::class,
        ],
         InventoryCreated::class => [
         LogInventoryCreated::class,
        ]
    ];

    /**
     * Register any events for your application.
     *
     * @return void
     */
    public function boot()
    {
        //Observers
        Inventory::observe(InventoryObserver::class);
    }
}
