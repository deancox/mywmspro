<?php

namespace Database\Seeders;

use App\Models\Carriers\Carrier;
use Illuminate\Database\Seeder;

class CarrierSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Carrier::create([
            'name' => 'DHL',
            'created_by' => 'DEFAULT'
        ]);
        Carrier::create([
            'name' => 'FEDEX',
            'created_by' => 'DEFAULT'
        ]);
        Carrier::create([
            'name' => 'DPD',
            'created_by' => 'DEFAULT'
        ]);
        Carrier::create([
            'name' => 'HERMES',
            'created_by' => 'DEFAULT'
        ]);
        Carrier::create([
            'name' => 'UPS',
            'created_by' => 'DEFAULT'
        ]);
    }
}
