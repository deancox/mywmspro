<?php

namespace Database\Seeders;

use App\Models\Uom;
use Illuminate\Database\Seeder;

class UOMSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        Uom::create([
            'name' => 'EA',
            'description' => 'Each',
            'created_by' => 'DEFAULT',
        ]);
        Uom::create([
            'name' => 'CS',
            'description' => 'Case',
            'created_by' => 'DEFAULT',
        ]);
        Uom::create([
            'name' => 'PA',
            'description' => 'Pallet',
            'created_by' => 'DEFAULT',
        ]);
        Uom::create([
            'name' => 'RO',
            'description' => 'Roll',
            'created_by' => 'DEFAULT',
        ]);
        Uom::create([
            'name' => 'M',
            'description' => 'Meter',
            'created_by' => 'DEFAULT',
        ]);
        Uom::create([
            'name' => 'MD',
            'description' => 'Medkit',
            'created_by' => 'DEFAULT',
        ]);
        Uom::create([
            'name' => 'CM',
            'description' => 'Centimeter',
            'created_by' => 'DEFAULT',
        ]);
        Uom::create([
            'name' => 'BA',
            'description' => 'Barrel',
            'created_by' => 'DEFAULT',
        ]);
        Uom::create([
            'name' => 'L',
            'description' => 'Litre',
            'created_by' => 'DEFAULT',
        ]);
        Uom::create([
            'name' => 'ML',
            'description' => 'Millilitre',
            'created_by' => 'DEFAULT',
        ]);
        Uom::create([
            'name' => 'B',
            'description' => 'Bag',
            'created_by' => 'DEFAULT',
        ]);
        Uom::create([
            'name' => 'T',
            'description' => 'Ton',
            'created_by' => 'DEFAULT',
        ]);
        Uom::create([
            'name' => 'KG',
            'description' => 'Kilogram',
            'created_by' => 'DEFAULT',
        ]);
        Uom::create([
            'name' => 'G',
            'description' => 'Gram',
            'created_by' => 'DEFAULT',
        ]);
        Uom::create([
            'name' => 'MG',
            'description' => 'Milligram',
            'created_by' => 'DEFAULT',
        ]);
    }
}
