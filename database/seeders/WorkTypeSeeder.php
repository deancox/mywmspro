<?php

namespace Database\Seeders;

use App\Models\Work\WorkType;
use Illuminate\Database\Seeder;

class WorkTypeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        WorkType::create([
            'name' => 'PUT',
            'description' => 'Putaway',
        ]);
        WorkType::create([
            'name' => 'LIPCK',
            'description' => 'List Pick',
        ]);
        WorkType::create([
            'name' => 'PAPCK',
            'description' => 'Pallet Pick',
        ]);
        WorkType::create([
            'name' => 'CSPCK',
            'description' => 'Case Pick',
        ]);
        WorkType::create([
            'name' => 'EAPCK',
            'description' => 'Each Pick',
        ]);
        WorkType::create([
            'name' => 'PARPL',
            'description' => 'Pallet Replenishment',
        ]);
        WorkType::create([
            'name' => 'CSRPL',
            'description' => 'Case Replenishment',
        ]);
        WorkType::create([
            'name' => 'EARPL',
            'description' => 'Each Replenishment',
        ]);
        WorkType::create([
            'name' => 'CYCNT',
            'description' => 'Cycle Count',
        ]);
        WorkType::create([
            'name' => 'AUCNT',
            'description' => 'Audit Count',
        ]);
        WorkType::create([
            'name' => 'CNTAU',
            'description' => 'Count Audit',
        ]);
        WorkType::create([
            'name' => 'STKTK',
            'description' => 'Stock Take Count',
        ]);
        WorkType::create([
            'name' => 'STKTKAU',
            'description' => 'Stock Take Audt Count',
        ]);
        WorkType::create([
            'name' => 'INVTR',
            'description' => 'Inventory Transfer',
        ]);

    }
}
