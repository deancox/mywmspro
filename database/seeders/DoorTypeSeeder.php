<?php

namespace Database\Seeders;

use App\Models\Doors\DoorType;
use Illuminate\Database\Seeder;

class DoorTypeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        DoorType::create([
            'name' => 'ANY',
            'description' => 'Any',
            'created_by' => 'DEFAULT'
        ]);
        DoorType::create([
            'name' => 'INB',
            'description' => 'Inbound Only',
            'created_by' => 'DEFAULT'
        ]);
        DoorType::create([
            'name' => 'OUT',
            'description' => 'Outbound Only',
            'created_by' => 'DEFAULT'
        ]);
        DoorType::create([
            'name' => 'PAR',
            'description' => 'Parcel Only',
            'created_by' => 'DEFAULT'
        ]);
        DoorType::create([
            'name' => 'RET',
            'description' => 'Returns Only',
            'created_by' => 'DEFAULT'
        ]);
        DoorType::create([
            'name' => 'INOUTNP',
            'description' => 'Inbound and Outbound (no Parcel)',
            'created_by' => 'DEFAULT'
        ]);
        DoorType::create([
            'name' => 'INOUTP',
            'description' => 'Inbound and Outbound (incl Parcel)',
            'created_by' => 'DEFAULT'
        ]);
        DoorType::create([
            'name' => 'INOUTNR',
            'description' => 'Inbound and Outbound (no Returns)',
            'created_by' => 'DEFAULT'
        ]);
        DoorType::create([
            'name' => 'INOUTR',
            'description' => 'Inbound and Outbound (incl Returns)',
            'created_by' => 'DEFAULT'
        ]);
        DoorType::create([
            'name' => 'PARINB',
            'description' => 'All Inbound and only Parcel Outbound',
            'created_by' => 'DEFAULT'
        ]);

    }
}
