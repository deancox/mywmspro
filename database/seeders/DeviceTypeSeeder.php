<?php

namespace Database\Seeders;

use App\Models\DeviceType;
use Illuminate\Database\Seeder;

class DeviceTypeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        DeviceType::create([
            'name' => 'HANDHELD',
            'description' => 'Handheld Scanner',
            'created_by' => 'DEFAULT',
        ]);
        DeviceType::create([
            'name' => 'REACH',
            'description' => 'Reach Truck Scanner',
            'created_by' => 'DEFAULT',
        ]);
        DeviceType::create([
            'name' => 'CB',
            'description' => 'Counterbalance Scanner',
            'created_by' => 'DEFAULT',
        ]);
        DeviceType::create([
            'name' => 'LLOP',
            'description' => 'Low Level Order Picker',
            'created_by' => 'DEFAULT',
        ]);
        DeviceType::create([
            'name' => 'BENDI',
            'description' => 'Bendi Truck scanner',
            'created_by' => 'DEFAULT',
        ]);

    }
}
