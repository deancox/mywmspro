<?php

namespace Database\Seeders;

use App\Models\Holds\HoldType;
use Illuminate\Database\Seeder;

class HoldTypeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        HoldType::create([
            'name' => 'INB',
            'description' => 'Inbound Inspection Hold',
            'created_by' => 'SYSTEM',
        ]);

        HoldType::create([
            'name' => 'QA',
            'description' => 'Quality Inspection Hold',
            'created_by' => 'SYSTEM',
        ]);

        HoldType::create([
            'name' => 'OUT',
            'description' => 'Outbound Inspection Hold',
            'created_by' => 'SYSTEM',
        ]);

        HoldType::create([
            'name' => 'CUST',
            'description' => 'Reserved for Customer Hold',
            'created_by' => 'SYSTEM',
        ]);

        HoldType::create([
            'name' => 'REC',
            'description' => 'Recall Inspection Hold',
            'created_by' => 'SYSTEM',
        ]);

        HoldType::create([
            'name' => 'DEST',
            'description' => 'Destruction Hold',
            'created_by' => 'SYSTEM',
        ]);
    }
}
