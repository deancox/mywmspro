<?php

namespace Database\Seeders;

use App\Models\Printer;
use Illuminate\Database\Seeder;

class PrinterSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        
        Printer::create([
            'name' => 'Example Laser Printer',
            'name_on_server' => 'example laser',
            'ip' => '192.0.0.1',
            'enabled' => true,
            'printer_type_id' => 1
        ]);
        Printer::create([
            'name' => 'Example Label Printer',
            'name_on_server' => 'example label',
            'ip' => '192.0.0.1',
            'enabled' => true,
            'printer_type_id' => 2
        ]);
    }
}
