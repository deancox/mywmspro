<?php

namespace Database\Seeders;

use App\Models\LocationType;
use Illuminate\Database\Seeder;

class LocationTypeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        
        LocationType::create([
            'name' => 'STOR',
            'description' => 'Storage Location',
            'created_by' => 'DEFAULT',
            'default' => 1
        ]);
        LocationType::create([
            'name' => 'STAGE',
            'description' => 'Staging Lane',
            'created_by' => 'DEFAULT',
            'default' => 1
        ]);
        LocationType::create([
            'name' => 'WHR',
            'description' => 'Routing Location',
            'created_by' => 'DEFAULT',
            'default' => 1
        ]);
        LocationType::create([
            'name' => 'DOOR',
            'description' => 'Dock door',
            'created_by' => 'DEFAULT',
            'default' => 1
        ]);
        LocationType::create([
            'name' => 'MOBILE',
            'description' => 'Mobile Device',
            'created_by' => 'DEFAULT',
            'default' => 1
        ]);
    }
}
