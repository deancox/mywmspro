<?php

namespace Database\Seeders;

use App\Models\Device;
use Illuminate\Database\Seeder;

class DeviceSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        
        Device::create([
            'name' => 'DEFAULTLLOP',
            'device_type_id' => '4',
            'created_by' => 'DEFAULT',
        ]);
        Device::create([
            'name' => 'DEFAULTHANDHELD',
            'device_type_id' => '1',
            'created_by' => 'DEFAULT',
        ]);
        Device::create([
            'name' => 'DEFAULTREACH',
            'device_type_id' => '2',
            'created_by' => 'DEFAULT',
        ]);
        Device::create([
            'name' => 'DEFAULTBENDI',
            'device_type_id' => '5',
            'created_by' => 'DEFAULT',
        ]);
        Device::create([
            'name' => 'DEFAULTCB',
            'device_type_id' => '3',
            'created_by' => 'DEFAULT',
        ]);
    }
}
