<?php

namespace Database\Seeders;

use App\Models\Products\ProductType;
use Illuminate\Database\Seeder;

class ProductTypeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        
        ProductType::create([
            'name' => 'AMB',
            'description' => 'Ambient Product',
            'created_by' => 'DEFAULT',
        ]);
        ProductType::create([
            'name' => 'CHILL',
            'description' => 'Chilled Product',
            'created_by' => 'DEFAULT',
        ]);
        ProductType::create([
            'name' => 'HAZ',
            'description' => 'Hazardous Product',
            'created_by' => 'DEFAULT',
        ]);
        ProductType::create([
            'name' => 'GM',
            'description' => 'General Product',
            'created_by' => 'DEFAULT',
        ]);
    }
}
