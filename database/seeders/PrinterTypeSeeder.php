<?php

namespace Database\Seeders;

use App\Models\PrinterType;
use Illuminate\Database\Seeder;

class PrinterTypeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        
        PrinterType::create([
            'type' => 'LASER',
            'created_by' => 'DEFAULT'
        ]);
        PrinterType::create([
            'type' => 'LABEL',
            'created_by' => 'DEFAULT'

        ]);
        PrinterType::create([
            'type' => 'MOBILE',
            'created_by' => 'DEFAULT'
        ]);
    }
}
