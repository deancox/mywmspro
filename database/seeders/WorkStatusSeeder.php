<?php

namespace Database\Seeders;

use App\Models\Work\WorkStatus;
use Illuminate\Database\Seeder;

class WorkStatusSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        
        WorkStatus::create([
            'status' => 'Created',
        ]);

        WorkStatus::create([
            'status' => 'Hold',
        ]);

        WorkStatus::create([
            'status' => 'Suspended',
        ]);

        WorkStatus::create([
            'status' => 'Ready',
        ]);

        WorkStatus::create([
            'status' => 'In Progress',
        ]);

        WorkStatus::create([
            'status' => 'Completed',
        ]);
    }
}
