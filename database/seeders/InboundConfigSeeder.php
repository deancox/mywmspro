<?php

namespace Database\Seeders;

use App\Models\Inbound\InboundConfig;
use Illuminate\Database\Seeder;

class InboundConfigSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        InboundConfig::create([
            'truck_required' => '0',
            'truck_required_by_supplier' => '0',
            'short_receive' => '0',
            'shorts_by_supplier' => '0',
            'over_receive' => '0',
            'over_receipt_by_supplier' => '0',
            'mobile_can_close' => '0',
            'mobile_can_dispatch' => '0'
        ]);
    }
}
