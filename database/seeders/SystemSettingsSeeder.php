<?php

namespace Database\Seeders;

use App\Models\SystemSettings;
use Illuminate\Database\Seeder;

class SystemSettingsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        SystemSettings::create([
        'warehouse_id' => 'DEFAULT',
        'inbound_delivery_prefix' => 'IBD',
        'outound_delivery_prefix' => 'OBD',
        'pallet_id_prefix' => 'PA',
        'case_id_prefix' => 'CS',
        'each_id_prefix' => 'EA',
        'subuom_id_prefix' => 'SUU',
        'inbound_trailer_prefix' => 'ITRL',
        'outbound_trailer_prefix' => 'OTRL',
        'load_id_prefix' => 'LD',
        'wave_id_prefix' => 'WA',
        'pcklst_id_prefix' => 'PL',
        'work_id_prefix' => 'WRK',
        'inbound_delivery' => '1000001',
        'outound_delivery' => '1000001',
        'pallet_id' => '10000001',
        'case_id' => '10000001',
        'each_id' => '10000001',
        'subuom_id' => '1000001',
        'inbound_trailer' => '10000001',
        'outbound_trailer' => '10000001',
        'load_id' => '10000001',
        'wave_id' => '10000001',
        'pcklst_id' => '10000001',
        'work_id' => '1000001',
        ]);
    }
}
