<?php

namespace Database\Seeders;

use App\Models\Doors\Door;
use App\Models\Inbound\InboundType;
use Illuminate\Database\Seeder;

class DoorSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Door::create([
            'name' => 'DOOR01'
        ]);
        Door::create([
            'name' => 'DOOR02'
        ]);
        Door::create([
            'name' => 'DOOR03'
        ]);
        Door::create([
            'name' => 'DOOR04'
        ]);
        Door::create([
            'name' => 'DOOR05'
        ]);
        Door::create([
            'name' => 'DOOR06'
        ]);
        Door::create([
            'name' => 'DOOR07'
        ]);
        Door::create([
            'name' => 'DOOR08'
        ]);
        Door::create([
            'name' => 'DOOR09'
        ]);
        Door::create([
            'name' => 'DOOR10'
        ]);

        foreach (Door::all() as $door){
            $inboundTypes = InboundType::inRandomOrder()->take(rand(1,4))->pluck('id');
            $door->inboundTypes()->attach($inboundTypes);
        }
    }
}
