<?php

namespace Database\Seeders;

use App\Models\Inbound\Supplier;
use Illuminate\Database\Seeder;

class SupplierSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Supplier::create([
            'name' => 'INTERNAL',
            'description' => 'Internal',
            'created_by' => 'seed',
        ]);

        Supplier::create([
            'name' => 'RETURN',
            'description' => 'Return',
            'created_by' => 'seed',
        ]);
    }
}
