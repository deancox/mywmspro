<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // \App\Models\User::factory(10)->create();

        $this->call([

            DeviceTypeSeeder::class,
            LocationTypeSeeder::class,
            SuperUserSeeder::class,
            ProductTypeSeeder::class,
            ProductSeeder::class,
            UOMSeeder::class,
            ProductDetailSeeder::class,
            DeviceSeeder::class,
            InventoryStatusSeeder::class,
            SupplierSeeder::class,
            SystemSettingsSeeder::class,
            HoldTypeSeeder::class,
            WorkStatusSeeder::class,
            MobileMenuSeeder::class,
            ReceiptTypeSeeder::class,
            PrinterTypeSeeder::class,
            PrinterSeeder::class,
            PickTypeSeeder::class,
            DoorTypeSeeder::class,
            InboundTypeSeeder::class,
            DoorSeeder::class,
            CarrierSeeder::class,
            InboundConfigSeeder::class,
            StagingLaneSeeder::class,
            WorkTypeSeeder::class,
            WorkSeeder::class,
  
        ]);
    }
}
