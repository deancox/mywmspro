<?php

namespace Database\Seeders;

use App\Models\Product;
use App\Models\ProductDetail;
use App\Models\Uom;
use Illuminate\Database\Seeder;

class ProductDetailSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $products = Product::all();

        foreach ($products as $product) {

            $uom = Uom::inRandomOrder()->limit(1)->first();

            ProductDetail::create([
                'product_id' => $product->id,
                'name' => $product->product,
                'description' => 'some description',
                'lpn_level' => 'EA',
                'uom' => $uom->name,
                'uom_id' => $uom->id,
                'uom_qty' => 1,
                'default_uom_qty' => rand(1,0),
                'uom_pick_flg' => rand(1, 0),
                'uom_receive_flg' => rand(1, 0),
                'uom_sto_flg' => rand(1, 0),
                'uom_replen_flg' => rand(1, 0),
                'uom_partial_flg' => rand(1, 0),
                'uom_round_up_flg' => rand(1, 0),
                'uom_round_down_flg' => rand(1, 0),
                'uom_height' => rand(10, 50),
                'uom_width' => rand(10, 50),
                'uom_length' => rand(10, 50),
                'uom_netweight' => rand(10, 50),
                'uom_grossweight' => rand(10, 50),
                'created_by' => 'SEED'
            ]);

            $csuom = Uom::where('name','!=',$uom->name)->inRandomOrder()->limit(1)->first();

            ProductDetail::create([
                'product_id' => $product->id,
                'name' => $product->product,
                'description' => 'some description',
                'lpn_level' => 'CS',
                'uom' => $csuom->name,
                'uom_id' => $csuom->id,
                'uom_qty' => rand(10,20),
                'default_uom_qty' => rand(1,0),
                'uom_pick_flg' => rand(1, 0),
                'uom_receive_flg' => rand(1, 0),
                'uom_sto_flg' => rand(1, 0),
                'uom_replen_flg' => rand(1, 0),
                'uom_partial_flg' => rand(1, 0),
                'uom_round_up_flg' => rand(1, 0),
                'uom_round_down_flg' => rand(1, 0),
                'uom_height' => rand(10, 50),
                'uom_width' => rand(10, 50),
                'uom_length' => rand(10, 50),
                'uom_netweight' => rand(10, 50),
                'uom_grossweight' => rand(10, 50),
                'created_by' => 'SEED'
            ]);

            $pauom = Uom::whereNotIn('name' ,[$uom->name, $csuom->name])->inRandomOrder()->limit(1)->first();

            ProductDetail::create([
                'product_id' => $product->id,
                'name' => $product->product,
                'description' => 'some description',
                'lpn_level' => 'PA',
                'uom' => $pauom->name,
                'uom_id' => $pauom->id,
                'uom_qty' => rand(100,500),
                'default_uom_qty' => rand(1,0),
                'uom_pick_flg' => rand(1, 0),
                'uom_receive_flg' => rand(1, 0),
                'uom_sto_flg' => rand(1, 0),
                'uom_replen_flg' => rand(1, 0),
                'uom_partial_flg' => rand(1, 0),
                'uom_round_up_flg' => rand(1, 0),
                'uom_round_down_flg' => rand(1, 0),
                'uom_height' => rand(10, 50),
                'uom_width' => rand(10, 50),
                'uom_length' => rand(10, 50),
                'uom_netweight' => rand(10, 50),
                'uom_grossweight' => rand(10, 50),
                'created_by' => 'SEED'
            ]);
        };
    }
}
