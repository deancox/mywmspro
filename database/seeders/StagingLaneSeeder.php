<?php

namespace Database\Seeders;

use App\Models\Staging\StagingLane;
use Illuminate\Database\Seeder;

class StagingLaneSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        StagingLane::create([
            'name' => 'LANE01',
            'used_percent' => rand(1,100),
        ]);
        StagingLane::create([
            'name' => 'LANE02',
            'used_percent' => rand(1,100),
        ]);
        StagingLane::create([
            'name' => 'LANE03',
            'used_percent' => rand(1,100),
        ]);
        StagingLane::create([
            'name' => 'LANE04',
            'used_percent' => rand(1,100),
        ]);
        StagingLane::create([
            'name' => 'LANE05',
            'used_percent' => rand(1,100),
        ]);
        StagingLane::create([
            'name' => 'LANE06'
        ]);
        StagingLane::create([
            'name' => 'LANE07'
        ]);
        StagingLane::create([
            'name' => 'LANE08'
        ]);
        StagingLane::create([
            'name' => 'LANE09'
        ]);
        StagingLane::create([
            'name' => 'LANE10'
        ]);
    }
}
