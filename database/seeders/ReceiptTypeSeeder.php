<?php

namespace Database\Seeders;

use App\Models\ReceiptType;
use Illuminate\Database\Seeder;

class ReceiptTypeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        ReceiptType::create([
            'name' => 'ASN',
            'description' => 'ASN Receipt',
            'created_by' => 'DEFAULT'
        ]);
        ReceiptType::create([
            'name' => 'PO',
            'description' => 'PO Receipt',
            'created_by' => 'DEFAULT'
        ]);
        ReceiptType::create([
            'name' => 'RET',
            'description' => 'Return',
            'created_by' => 'DEFAULT'
        ]);
        ReceiptType::create([
            'name' => 'INT',
            'description' => 'Created Internally',
            'created_by' => 'DEFAULT'
        ]);
    }
}
