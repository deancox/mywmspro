<?php

namespace Database\Seeders;

use App\Models\PickType;
use App\Models\Uom;
use Illuminate\Database\Seeder;

class PickTypeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $uoms = Uom::all();

        foreach($uoms as $uom){

            PickType::create([
                'name' => $uom->name,
                'category' => 'Pick',
                'description' => $uom->description . ' Pick',
                'created_by' => 'DEFAULT'
            ]);
            
            PickType::create([
                'name' => $uom->name,
                'category' => 'Replenishment',
                'description' => $uom->description . ' Replenishment',
                'created_by' => 'DEFAULT'
            ]);
        }
    }
}
