<?php

namespace Database\Seeders;

use App\Models\Inbound\InboundType;
use Illuminate\Database\Seeder;

class InboundTypeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        InboundType::create([
            'name' => 'PO Receipt',
        ]);
        InboundType::create([
            'name' => 'ASN',
        ]);
        InboundType::create([
            'name' => 'Consumables',
        ]);
        InboundType::create([
            'name' => 'Returns',
        ]);
    }
}
