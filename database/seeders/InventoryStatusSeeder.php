<?php

namespace Database\Seeders;

use App\Models\Inventory\InventoryStatus;
use Illuminate\Database\Seeder;

class InventoryStatusSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        InventoryStatus::create([
            'name' => 'AV',
            'description' => 'Available',
            'created_by' => 'SYSTEM',
            'in_use' => 1,
        ]);
        InventoryStatus::create([
            'name' => 'DAM',
            'description' => 'Damaged',
            'created_by' => 'SYSTEM',
            'in_use' => 1,
        ]);
        InventoryStatus::create([
            'name' => 'DEST',
            'description' => 'For Destruction',
            'created_by' => 'SYSTEM',
        ]);
        InventoryStatus::create([
            'name' => 'QA',
            'description' => 'Quality',
            'created_by' => 'SYSTEM',
            'in_use' => 1,
        ]);
        InventoryStatus::create([
            'name' => 'HOLD',
            'description' => 'On Hold',
            'created_by' => 'SYSTEM',
            'in_use' => 1,
        ]);
        InventoryStatus::create([
            'name' => 'PCK',
            'description' => 'Picked',
            'created_by' => 'SYSTEM',
            'in_use' => 1,
        ]);
        InventoryStatus::create([
            'name' => 'PACK',
            'description' => 'Packed',
            'created_by' => 'SYSTEM',
            'in_use' => 1,
        ]);
        InventoryStatus::create([
            'name' => 'LOD',
            'description' => 'Loaded',
            'created_by' => 'SYSTEM',
            'in_use' => 1,
        ]);
        InventoryStatus::create([
            'name' => 'SHP',
            'description' => 'Shipped',
            'created_by' => 'SYSTEM',
            'in_use' => 1,
        ]);
        InventoryStatus::create([
            'name' => 'EX',
            'description' => 'Expired',
            'created_by' => 'SYSTEM',
            'in_use' => 1,
        ]);
        InventoryStatus::create([
            'name' => 'HV',
            'description' => 'High Value',
            'created_by' => 'SYSTEM',
        ]);
        InventoryStatus::create([
            'name' => 'SEC',
            'description' => 'Secure',
            'created_by' => 'SYSTEM',
        ]);
    }
}
