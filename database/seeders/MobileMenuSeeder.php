<?php

namespace Database\Seeders;

use App\Models\MobileMenu;
use Illuminate\Database\Seeder;

class MobileMenuSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        MobileMenu::create([
            'name' => 'inventory',
            'screen' => 'home',
            'displayorder' => '1',
            'description' => 'Inventory'
        ]);
        MobileMenu::create([
            'name' => 'receipt',
            'screen' => 'home',
            'displayorder' => '2',
            'description' => 'Receiving'
        ]);
        MobileMenu::create([
            'name' => 'pick',
            'screen' => 'home',
            'displayorder' => '3',
            'description' => 'Picking'
        ]);
        MobileMenu::create([
            'name' => 'load',
            'screen' => 'home',
            'displayorder' => '4',
            'description' => 'Loading'
        ]);
        MobileMenu::create([
            'name' => 'work',
            'screen' => 'home',
            'displayorder' => '5',
            'description' => 'Find Work'
        ]);
    }
}
