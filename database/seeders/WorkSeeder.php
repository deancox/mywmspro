<?php

namespace Database\Seeders;

use App\Models\Work\Work;
use Illuminate\Database\Seeder;

class WorkSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Work::factory(100)->create();
    }
}
