<?php

namespace Database\Factories;

use App\Models\Product;
use App\Models\Products\ProductType;
use Illuminate\Database\Eloquent\Factories\Factory;

class ProductFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Product::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'product' => random_int(500000,599999),
            'description' => $this->faker->name,
            'product_type' => ProductType::inRandomOrder()->pluck('name')->first(),
            'rcv_flg' => rand(1,0),
            'haz_flg' => rand(1,0),
            'serial_flg' => rand(1,0),
            'created_by' =>'seed',
        ];
    }
}
