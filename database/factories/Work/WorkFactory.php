<?php

namespace Database\Factories\Work;

use App\Models\Work\Work;
use App\Models\Work\WorkStatus;
use App\Models\Work\WorkType;
use Illuminate\Database\Eloquent\Factories\Factory;

class WorkFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Work::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'status' => WorkStatus::inRandomOrder()->pluck('status')->first(),
            'type' => WorkType::inRandomOrder()->pluck('description')->first(),
            'priority' => rand(1,100),
            'base_priority' => rand(1,100),
            'estimate_mins' => rand(1,180),
            'created_by' => 'SEED'
        ];
    }
}
