<?php

namespace Database\Factories\Work;

use Illuminate\Database\Eloquent\Factories\Factory;

class WorkTypeFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            //
        ];
    }
}
