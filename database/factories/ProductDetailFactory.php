<?php

namespace Database\Factories;

use App\Models\Product;
use App\Models\ProductDetail;
use App\Models\Uom;
use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Validation\Rules\Unique;

class ProductDetailFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = ProductDetail::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
    
        $uom = Uom::inRandomOrder()->limit(1)->first();

            return [
                'description' => 'some description',
                'lpn_level' => rand(1,0) ? 'PA' : 'CS',
                'uom' => $uom->name,
                'uom_id' => $uom->id,
                'uom_qty' => rand(1, 10),
                'default_uom_qty' => rand(1,0),
                'uom_pick_flg' => rand(1, 0),
                'uom_receive_flg' => rand(1, 0),
                'uom_sto_flg' => rand(1, 0),
                'uom_replen_flg' => rand(1, 0),
                'uom_partial_flg' => rand(1, 0),
                'uom_round_up_flg' => rand(1, 0),
                'uom_round_down_flg' => rand(1, 0),
                'uom_height' => rand(10, 50),
                'uom_width' => rand(10, 50),
                'uom_length' => rand(10, 50),
                'uom_netweight' => rand(10, 50),
                'uom_grossweight' => rand(10, 50),
                'created_by' => 'SEED'
            
            ];
    }
}
