<?php

namespace Database\Factories;

use App\Models\DeliveryDetail;
use Illuminate\Database\Eloquent\Factories\Factory;

class DeliveryDetailFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = DeliveryDetail::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            //
        ];
    }
}
