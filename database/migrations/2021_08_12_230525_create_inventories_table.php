<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateInventoriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('inventories', function (Blueprint $table) {
            $table->id();
            $table->string('product');
            $table->string('location');
            $table->string('qty');
            $table->string('allocated_qty')->default(0);
            $table->string('pallet_qty');
            $table->string('case_qty');
            $table->string('subuom_qty')->default(0);
            $table->string('subuom_allocated_qty')->default(0);
            $table->string('uom')->nullable();
            $table->string('sub_uom')->nullable();
            $table->string('inventory_status');
            $table->string('hold_id')->nullable()->constrained();
            $table->string('hold_type_id')->nullable()->constrained();
            $table->string('pallet_id');
            $table->string('case_id')->nullable();
            $table->string('each_id')->nullable();
            $table->string('sub_uom_id')->nullable();
            $table->string('supplier_id');
            $table->string('delivery_ref')->nullable();
            $table->foreignId('storage_zone_id')->nullable()->constrained();
            $table->foreignId('pick_zone_id')->nullable()->constrained();
            $table->foreignId('count_zone_id')->nullable()->constrained();
            $table->string('product_type')->nullable();
            $table->boolean('is_picked')->default(0);
            $table->boolean('is_packed')->default(0);
            $table->boolean('is_serialized')->default(0);
            $table->string('created_by');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('inventories');
    }
}
