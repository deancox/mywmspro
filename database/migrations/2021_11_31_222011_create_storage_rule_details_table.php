<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateStorageRuleDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('storage_rule_details', function (Blueprint $table) {
            $table->id();
            $table->foreignId('storage_rule_id')->constrained('storage_rules')->onDelete('cascade');
            $table->foreignId('storage_zone_id')->nullable()->constrained('storage_zones')->onDelete('cascade');
            $table->foreignId('product_type_id')->nullable()->constrained('product_types')->onDelete('cascade');
            $table->foreignId('uom_id')->nullable()->constrained('uoms')->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('storage_rule_details');
    }
}
