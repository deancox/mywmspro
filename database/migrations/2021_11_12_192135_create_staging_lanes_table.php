<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateStagingLanesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('staging_lanes', function (Blueprint $table) {
            $table->id();
            $table->string('name');
            $table->string('area')->nullable();
            $table->boolean('in_service')->default(true);
            $table->integer('max_deliveries')->default(1);
            $table->integer('cur_delivery_count')->default(0);
            $table->string('def_door_id')->nullable();
            $table->string('default_label_printer')->nullable();
            $table->string('default_laser_printer')->nullable();
            $table->boolean('tracked')->default(false);
            $table->string('tracked_by')->nullable();
            $table->integer('maxqty')->nullable();
            $table->integer('current_qty')->default(0);
            $table->integer('used_percent')->default(0);
            $table->boolean('overflow_allowed')->default(false);
            $table->integer('overflow_percent')->default(0);
            $table->boolean('full')->default(false);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('staging_lanes');
    }
}
