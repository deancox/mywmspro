<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateWorksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('works', function (Blueprint $table) {
            $table->id();
            $table->string('status');
            $table->string('type');
            $table->integer('priority')->default(50);
            $table->integer('base_priority')->default(50);
            $table->string('source_location')->nullable();
            $table->string('destination_location')->nullable();
            $table->string('wave_ref')->nullable();
            $table->string('delivery_ref')->nullable();
            $table->string('count_ref')->nullable();
            $table->string('list_id')->nullable();
            $table->string('assigned_user_id')->nullable();
            $table->string('assigned_user_group')->nullable();
            $table->string('assigned_equiment_group')->nullable();
            $table->string('estimate_mins')->nullable();
            $table->string('created_by');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('works');
    }
}
