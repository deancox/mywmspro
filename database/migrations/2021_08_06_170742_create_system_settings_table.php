<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSystemSettingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('system_settings', function (Blueprint $table) {
            $table->id();
            $table->String('warehouse_id');
            $table->String('inbound_delivery_prefix');
            $table->String('outound_delivery_prefix');
            $table->String('pallet_id_prefix');
            $table->String('case_id_prefix');
            $table->String('each_id_prefix');
            $table->String('subuom_id_prefix');
            $table->String('inbound_trailer_prefix');
            $table->String('outbound_trailer_prefix');
            $table->String('load_id_prefix');
            $table->String('wave_id_prefix');
            $table->String('pcklst_id_prefix');
            $table->String('work_id_prefix');
            $table->String('inbound_delivery');
            $table->String('outound_delivery');
            $table->String('pallet_id');
            $table->String('case_id');
            $table->String('each_id');
            $table->String('subuom_id');
            $table->String('inbound_trailer');
            $table->String('outbound_trailer');
            $table->String('load_id');
            $table->String('wave_id');
            $table->String('pcklst_id');
            $table->String('work_id');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('system_settings');
    }
}
