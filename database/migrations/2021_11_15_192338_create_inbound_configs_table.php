<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateInboundConfigsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('inbound_configs', function (Blueprint $table) {
            $table->id();
            $table->boolean('truck_required')->default(false);
            $table->boolean('truck_required_by_supplier')->default(false);
            $table->boolean('short_receive')->default(false);
            $table->boolean('shorts_by_supplier')->default(false);
            $table->boolean('over_receive')->default(false);
            $table->boolean('over_receipt_by_supplier')->default(false);
            $table->boolean('mobile_can_close')->default(false);
            $table->boolean('mobile_can_dispatch')->default(false);
            $table->boolean('checkin_lane_required_by_supplier')->default(false);
            $table->boolean('checkin_lane_required')->default(false);
            $table->boolean('can_override_default_door')->default(true);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('inbound_configs');
    }
}
