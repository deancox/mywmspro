<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDeliveryDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('delivery_details', function (Blueprint $table) {
            $table->id();
            $table->foreignId('delivery_id')->constrained('deliveries')->onDelete('cascade');
            $table->foreignId('supplier_id')->constrained();
            $table->foreignId('receipt_type_id')->nullable()->constrained();
            $table->integer('rcv_line');
            $table->integer('sub_rcv_line')->default(0);
            $table->string('product');
            $table->string('pallet_id')->nullable();
            $table->string('case_id')->nullable();
            $table->string('each_id')->nullable();
            $table->string('sub_uom_id')->nullable();
            $table->string('expected_qty');
            $table->string('received_qty')->default(0);
            $table->string('short_qty')->default(0);
            $table->boolean('short_receipt_allowed')->default(true);
            $table->boolean('over_receipt_allowed')->default(true);
            $table->string('batch')->nullable();
            $table->string('cust_batch')->nullable();
            $table->string('rcv_to_lane')->nullable();
            $table->string('status')->default('EXPECTED');
            $table->boolean('complete_flg')->default(false);
            $table->boolean('inprogress_flg')->default(false);
            $table->boolean('qa_required')->default(false);
            $table->string('uom');
            $table->integer('uom_id');
            $table->integer('uom_qty');
            $table->string('lpnlevel');
            $table->string('serial_num')->nullable();
            $table->string('serial_type')->nullable();
            $table->string('created_by');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('delivery_details');
    }
}
