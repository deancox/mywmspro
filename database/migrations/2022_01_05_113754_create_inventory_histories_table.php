<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateInventoryHistoriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('inventory_histories', function (Blueprint $table) {
            $table->id();
            $table->string('product');
            $table->string('transaction_type');
            $table->string('action')->nullable();
            $table->string('transaction_comment')->nullable();
            $table->string('fr_location')->nullable();
            $table->string('to_location')->nullable();
            $table->string('fr_qty')->nullable();
            $table->string('to_qty')->nullable();
            $table->string('fr_pallet_qty')->nullable();
            $table->string('to_pallet_qty')->nullable();
            $table->string('fr_case_qty')->nullable();
            $table->string('to_case_qty')->nullable();
            $table->string('fr_subuom_qty')->nullable();
            $table->string('to_subuom_qty')->nullable();
            $table->string('uom')->nullable();
            $table->string('sub_uom')->nullable();
            $table->string('fr_inventory_status')->nullable();
            $table->string('to_inventory_status')->nullable();
            $table->string('hold_id')->nullable();
            $table->string('hold_type_id')->nullable();
            $table->string('fr_pallet_id')->nullable();
            $table->string('fr_case_id')->nullable();
            $table->string('fr_each_id')->nullable();
            $table->string('fr_sub_uom_id')->nullable();
            $table->string('to_pallet_id')->nullable();
            $table->string('to_case_id')->nullable();
            $table->string('to_each_id')->nullable();
            $table->string('to_sub_uom_id')->nullable();
            $table->string('supplier_id')->nullable();
            $table->string('delivery_ref')->nullable();
            $table->string('fr_product_type')->nullable();
            $table->string('to_product_type')->nullable();
            $table->string('fr_serial_no')->nullable();
            $table->string('to_serial_no')->nullable();
            $table->string('user_id')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('inventory_histories');
    }
}
