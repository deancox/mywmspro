<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('products', function (Blueprint $table) {
            $table->id();
            $table->string('product');
            $table->string('customer_product_id')->nullable();
            $table->string('description')->nullable();
            $table->string('product_type')->nullable();
            $table->string('def_rcv_status')->default('AV');
            $table->boolean('rcv_flg')->default(true);
            $table->boolean('hot_flg')->default(false);
            $table->boolean('serial_flg')->default(false);
            $table->boolean('send_to_host_flg')->default(true);
            $table->boolean('returnable_flg')->default(true);
            $table->boolean('haz_flg')->default(false);
            $table->string('created_by');
            $table->string('updated_by')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('products');
    }
}
