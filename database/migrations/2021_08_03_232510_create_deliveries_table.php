<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDeliveriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('deliveries', function (Blueprint $table) {
            $table->id();
            $table->string('delivery_ref');
            $table->foreignId('supplier_id')->constrained();
            $table->integer('delivery_type');
            $table->dateTime('due_date')->nullable();
            $table->string('status')->default('EXPECTED');
            $table->boolean('short_flg')->default(false);
            $table->boolean('short_receipt_allowed')->default(true);
            $table->boolean('over_receipt_allowed')->default(true);
            $table->boolean('host_flg')->default(false);
            $table->boolean('truck_required')->default(false);
            $table->boolean('can_close')->default(false);
            $table->boolean('can_dispatch')->default(false);
            $table->string('truck')->nullable();
            $table->string('door')->nullable();
            $table->string('lane')->nullable();
            $table->boolean('can_check_in')->default(true);
            $table->boolean('checked_in_flg')->default(false);
            $table->boolean('qa_required')->default(false);
            $table->boolean('checkin_lane_required')->default(false);
            $table->dateTime('check_in_date')->nullable();
            $table->string('created_by');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('deliveries');
    }
}
