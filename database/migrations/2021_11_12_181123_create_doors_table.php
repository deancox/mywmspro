<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDoorsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('doors', function (Blueprint $table) {
            $table->id();
            $table->string('name');
            $table->string('door_type_id')->default(1);
            $table->string('area')->nullable();
            $table->boolean('in_service')->default(true);
            $table->integer('max_trucks')->default(1);
            $table->integer('cur_truck_count')->default(0);
            $table->integer('cur_delivery_count')->default(0);
            $table->boolean('is_yard')->default(false);
            $table->string('def_stage_lane')->nullable();
            $table->string('default_label_printer')->nullable();
            $table->string('default_laser_printer')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('doors');
    }
}
