<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProductDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        
        Schema::create('product_details', function (Blueprint $table) {
            $table->id();
            $table->foreignId('product_id')->constrained()->onDelete('cascade');
            $table->foreignId('uom_id')->constrained();
            $table->string('name');
            $table->string('description');
            $table->string('lpn_level');
            $table->string('uom');
            $table->integer('uom_qty');
            $table->boolean('short_receipt_override')->default(false);
            $table->boolean('over_receipt_override')->default(false);
            $table->boolean('short_receipt_allowed')->default(true);
            $table->boolean('over_receipt_allowed')->default(true);
            $table->boolean('default_uom_qty')->default(true);
            $table->boolean('uom_pick_flg')->default(0);
            $table->boolean('uom_receive_flg')->default(0);
            $table->boolean('uom_sto_flg')->default(0);
            $table->boolean('uom_replen_flg')->default(0);
            $table->boolean('uom_partial_flg')->default(0);
            $table->boolean('uom_round_up_flg')->default(0);
            $table->boolean('uom_round_down_flg')->default(0);
            $table->string('sub_uom')->nullable();
            $table->integer('sub_uom_qty')->nullable();
            $table->boolean('sub_uom_pick_flg')->default(0);
            $table->boolean('sub_uom_receive_flg')->default(0);
            $table->boolean('sub_uom_sto_flg')->default(0);
            $table->boolean('sub_uom_replen_flg')->default(0);
            $table->boolean('sub_uom_partial_flg')->default(0);
            $table->boolean('sub_uom_round_up_flg')->default(0);
            $table->boolean('sub_uom_round_down_flg')->default(0);
            $table->string('uom_height');
            $table->string('uom_width');
            $table->string('uom_length');
            $table->string('uom_netweight');
            $table->string('uom_grossweight');
            $table->string('created_by');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('product_details');
    }
}
