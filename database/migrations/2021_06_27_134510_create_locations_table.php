<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateLocationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('locations', function (Blueprint $table) {
            $table->id();
            $table->string('location_name')->unique();
            $table->string('aisle');
            $table->string('level');
            $table->string('position');
            $table->string('height');
            $table->string('width');
            $table->string('length');
            $table->string('maxweight');
            $table->string('tracked_by')->nullable();
            $table->string('maxqty')->nullable();
            $table->string('currentqty')->nullable();
            $table->integer('travel_sequence')->nullable();
            $table->integer('storage_sequence')->nullable();
            $table->integer('pick_sequence')->nullable();
            $table->string('location_type')->nullable();
            $table->string('storage_zone')->nullable();
            $table->string('pick_zone')->nullable();
            $table->string('count_zone')->nullable();
            $table->boolean('is_pickable')->default(true);
            $table->boolean('is_pickface')->default(false);
            $table->boolean('is_usable')->default(true);
            $table->boolean('is_storable')->default(true);
            $table->boolean('pending_count')->default(false);
            $table->boolean('has_problem')->default(false);
            $table->boolean('is_temporary')->default(false);
            $table->string('access_group_id')->nullable();
            $table->boolean('full')->default(false);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('locations');
    }
}
